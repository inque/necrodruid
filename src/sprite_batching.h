#include "sprites.h"
#include "gameapp.h"
#include "rlgl.h"

#define MAX_SPRITE_QUEUES 16
#define SPRITE_QUEUE_LEN 2048

typedef struct{
	Texture2D* tex;
	SpriteShape_t* shape;
	Vector3 pos;
	float scale;
	Color tint;
} SpriteQueueEntry_t;

typedef struct{
	int length;
	SpriteQueueEntry_t data[SPRITE_QUEUE_LEN];
	void* aux;
	void* auxPrev;
	int deletionTimer;
} SpriteQueue_t;

static SpriteQueue_t* startQueue = NULL;
static SpriteQueue_t* endQueue   = NULL;
static SpriteQueue_t* endQueueFrame = NULL;
static int spriteQueuesUsed = 0;
static int endQueueFrameNum = 0;

static void SpriteQueuePush(SpriteQueueEntry_t entry);
static void SpriteQueueClear(SpriteQueue_t* queue);
static void SpriteQueueRender(SpriteQueue_t* queue);

//------------------------

static void SpriteQueueDrawShape(SpriteQueueEntry_t* entry);

static void SpriteQueuePush(SpriteQueueEntry_t entry){
	SpriteQueue_t* queue = endQueueFrame;
	SpriteQueue_t* aux;

	if(queue->length < SPRITE_QUEUE_LEN){
		queue->data[queue->length++] = entry;
	}else{
		if(queue->aux){
			aux = (SpriteQueue_t*)queue->aux;
			aux->data[aux->length++] = entry;
			endQueueFrame = aux;
			endQueueFrameNum++;
		}else if(endQueueFrameNum < MAX_SPRITE_QUEUES){
			aux = (SpriteQueue_t*)malloc(sizeof(SpriteQueue_t));
			aux->length = 1;
			aux->data[0] = entry;
			aux->aux = NULL;
			aux->deletionTimer = 0;
			aux->auxPrev = endQueue;
			endQueue->aux = aux;
			endQueue = aux;
			spriteQueuesUsed++;
		}
	}
}

static void SpriteQueueClear(SpriteQueue_t* lastQueue){
	if(!lastQueue){
		startQueue = (SpriteQueue_t*)calloc(1, sizeof(SpriteQueue_t));
		endQueueFrame = startQueue;
		endQueue = startQueue;
		lastQueue = endQueue;
		spriteQueuesUsed = 1;
	}else if(endQueueFrame){
		endQueueFrame = startQueue;
		endQueueFrameNum = 0;
	}

	SpriteQueue_t* auxPrev = (SpriteQueue_t*)lastQueue->auxPrev;
	int lastLength = lastQueue->length;
	lastQueue->length = 0;
	if((lastQueue == endQueue) && (endQueue != startQueue)){
		if(!lastLength){
			if(++lastQueue->deletionTimer > 24){
				auxPrev->aux = NULL;
				endQueue = auxPrev;
				free(lastQueue);
				spriteQueuesUsed--;
			}
		}
	}
	if(auxPrev){
		SpriteQueueClear(auxPrev);
	}
}

static int SpriteQueueCompareEntries(const SpriteQueueEntry_t* a, const SpriteQueueEntry_t* b){
	//return (int)(a->pos.z - b->pos.z);
	return (
		(a->pos.z != b->pos.z) ?
		(int)(a->pos.z - b->pos.z) //order
		: (a->tex->id - b->tex->id) //texture
	);
}

static void SpriteQueueRender(SpriteQueue_t* queue){
	qsort(&queue->data, queue->length, sizeof(SpriteQueueEntry_t), SpriteQueueCompareEntries);
	for(int i=0; i < queue->length; i++){
		SpriteQueueDrawShape(&queue->data[i]);
	}
	if(queue->aux){
		SpriteQueue_t* aux = (SpriteQueue_t*)queue->aux;
		SpriteQueueRender(aux);
	}
}

static void SpriteQueueDrawShape(SpriteQueueEntry_t* entry){
	SpriteShape_t* shape = entry->shape;
	rlCheckRenderBatchLimit((shape->vertCount-1)*4);
	rlSetTexture(entry->tex->id);
	rlBegin(RL_QUADS);
		rlNormal3f(0, 0, 1);
		rlColor4ub(entry->tint.r, entry->tint.g, entry->tint.b, entry->tint.a);
		int blarp = 0;
		for(int i=shape->indicesUsed-1; i >= 0; i--){
			Vector2 vert = shape->verts[shape->indices[i]];
			rlTexCoord2f((vert.x+shape->uv.x) / entry->tex->width, (vert.y+shape->uv.y) / entry->tex->height);
			rlVertex2f(
				vert.x*entry->scale + entry->pos.x,
				vert.y*entry->scale + entry->pos.y
			);
			//note: this idiocy is here because Raylib literally
			// dropped support for textured triangles, and
			// I didn't get an answer on how to get around it.
			if((blarp++%4)==1) i++;
		}
	rlEnd();
}

