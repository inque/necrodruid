#ifndef GEOMETRY_H
#include "utils.h"
#include "raylib.h"
#include "raymath.h"
#include "stdbool.h"

int MatchAngle(float angleDeg, int frames); ///angle to frame number
int MatchDirection(Vector2 dir, int frames); ///direction to frame number

//## Triangulation

#define TRIANGULATE_MAX 24

typedef struct{
	int triCount;
	int indices[TRIANGULATE_MAX][3];
} Triangulation2dOutput_t;

void Triangulate2d(int vertCount, const Vector2 *verts, Triangulation2dOutput_t* out);
bool IsConvex2d(int vertCount, Vector2* verts);

//## Rect utils

// inspired by: https://halt.software/dead-simple-layouts/
Rectangle CutLeft(Rectangle* rect, float a);
Rectangle CutRight(Rectangle* rect, float a);
Rectangle CutTop(Rectangle* rect, float a);
Rectangle CutBottom(Rectangle* rect, float a);
void Extend(Rectangle* rect, float a);
Rectangle FsRect();
bool IsMouseInRect(Rectangle bounds);

#define GEOMETRY_H
#endif
#ifdef IMP_GEOMETRY_H
//==== implementation starts here ====

#include "gameapp.h"

int MatchAngle(float angleDeg, int frames){
	int frame = (int)(roundf(angleDeg / 360 * frames));
	return mod2(frame, frames);
}

int MatchDirection(Vector2 dir, int frames){
	return MatchAngle(90 - Vector2Angle(Vector2Zero(), dir), frames);
}

//## Triangulation

static bool IsTriangleClockWise2d(Vector2 p1, Vector2 p2, Vector2 p3){
	float a = p2.x * p1.y + p3.x * p2.y + p1.x * p3.y;
	float b = p1.x * p2.y + p2.x * p3.y + p3.x * p1.y;
	return (a > b);
}

static bool IsPointInTriangle2d(Vector2 p, Vector2* t){
	for(int i=0; i<3; i++)
		if(IsTriangleClockWise2d(t[i], t[(i + 1) % 3], p)) return false;
	return true;
}

static bool Ear_Q(int i, int j, int k, int vertCount, const Vector2* verts){
	Vector2 t[3];

	t[0].x = verts[i].x;
	t[0].y = verts[i].y;

	t[1].x = verts[j].x;
	t[1].y = verts[j].y;

	t[2].x = verts[k].x;
	t[2].y = verts[k].y;

	if(IsTriangleClockWise2d(t[0], t[1], t[2])) return false;

	for(int m=0; m < vertCount; m++){
		if((m != i) && (m != j) && (m != k))
			if(IsPointInTriangle2d(verts[m], (Vector2*) &t)) return false;
	}
	return true;
}

void Triangulate2d(int vertCount, const Vector2 *verts, Triangulation2dOutput_t* out){
	// left/right neighbor indices
	int l[TRIANGULATE_MAX], r[TRIANGULATE_MAX];
	for(int i=0; i < vertCount; i++){
		l[i] = ((i-1) + vertCount) % vertCount;
		r[i] = ((i+1) + vertCount) % vertCount;
	}
	out->triCount = 0;
	int i = vertCount-1;
	int iterationCounter = 0;
	while(out->triCount < (vertCount - 2)){
		i = r[i];
		if( (iterationCounter++) > vertCount ) return;
		if(Ear_Q(l[i], i, r[i], vertCount, verts)){
			int* newTriangle = &out->indices[out->triCount++][0];
			newTriangle[0] = l[i];
			newTriangle[1] = i;
			newTriangle[2] = r[i];
			if(out->triCount == TRIANGULATE_MAX) return;
			l[ r[i] ] = l[i];
			r[ l[i] ] = r[i];
		}
	}
}

bool IsConvex2d(int vertCount, Vector2* verts){
	for(int i=0; i < vertCount; i++) {
		if(!IsTriangleClockWise2d(verts[i], verts[(i + 1) % vertCount], verts[(i + 2) % vertCount])) return false;
	}
	return true;
}


//## Rect utils

Rectangle CutLeft(Rectangle* rect, float a){
	float x = rect->x;
	rect->x += a;
	rect->width -= a;
	return (Rectangle){x, rect->y, a, rect->height};
}

Rectangle CutRight(Rectangle* rect, float a){
	float width = rect->width;
	rect->width -= a;
	return (Rectangle){rect->x + width - a, rect->y, a, rect->height};
}

Rectangle CutTop(Rectangle* rect, float a){
	float y = rect->y;
	rect->y += a;
	rect->height -= a;
	return (Rectangle){rect->x, y, rect->width, a};
}

Rectangle CutBottom(Rectangle* rect, float a){
	float height = rect->height;
	rect->height -= a;
	return (Rectangle){rect->x, rect->y + height - a, rect->width, a};
}

void Extend(Rectangle* rect, float a){
	rect->x -= a;
	rect->y -= a;
	rect->width += a*2;
	rect->height += a*2;
}

Rectangle FsRect(){ return (Rectangle){0, 0, client.width, client.height}; }

bool IsMouseInRect(Rectangle bounds){
	Vector2 mousePos = GetMousePosition();
	if(mousePos.x < bounds.x) return false;
	if(mousePos.y < bounds.y) return false;
	if(mousePos.x > (bounds.x + bounds.width)) return false;
	if(mousePos.y > (bounds.y + bounds.height)) return false;
	return true;
}

#undef IMP_GEOMETRY_H
#endif
