#ifndef FOGOFWAR_H
#include "stdbool.h"
#include "raylib.h"

bool CanSeeObject(int boyId, Vector3 pos);
bool CanSeeToy(int boyId, int toyId);
void UpdateFog(int boyId);
void DrawFog();
Vector3 GetRandomUnexploredLocation(int boyId, int seed, float* outScore);

#define FOGOFWAR_H
#endif
#ifdef IMP_FOGOFWAR_H
//==== implementation starts here ====
#include "gameapp.h"

static const int CircleSize = 16;
static const char CircleMap[16*16];

static void TileCoordinates(Vector3 pos, int* out){
	out[0] = (int)(pos.x/LV_TILESIZE);
	out[1] = (int)(pos.y/LV_TILESIZE);
}

bool CanSeeObject(int boyId, Vector3 pos){
	Boy_t* boy = GetBoy(boyId);
	int tile[2];
	TileCoordinates(pos, (int*)&tile);
	if(boyId == -1) return false;
	if(CoordOutOfBounds(tile, &game.level)) return false;

	return !!(boy->fog.visible[tile[0]][tile[1]]);
}

bool CanSeeToy(int boyId, int toyId){
	Toy_t* toy = GetToy(toyId);
	return CanSeeObject(boyId, PeekPos(&toy->limbs[0].tform));
}

void UpdateFog(int boyId){
	Boy_t* boy = GetBoy(boyId);
	if(boyId == -1) return;

	memset(&boy->fog.visible, 0, sizeof(boy->fog.visible));
	forEachToy(toy, toyId){
		if((app.role != ROLE_LOCAL) || (toyId != boy->toyId)){
			if(toy->health <= 0) continue;
		}
		if(toy->boyId == boyId){
			Vector3 pos = PeekPos(&toy->limbs[0].tform);
			int toyTile[2];
			TileCoordinates(pos, (int*)&toyTile);
			for(int iy = 0; iy < CircleSize; iy++)
			for(int ix = 0; ix < CircleSize; ix++){
				int tile[] = {toyTile[0]-CircleSize/2+ix, toyTile[1]-CircleSize/2+iy};
				if(CoordOutOfBounds(tile, &game.level)) continue;
				if(CircleMap[ix+iy*CircleSize]){
					boy->fog.visible[tile[0]][tile[1]] = 1;
					boy->fog.visited[tile[0]][tile[1]] = 1;
				}
			}
		}
	}
}

void DrawFog(){
	Boy_t* boy = GetBoy(client.playerBoyId);
	if(client.playerBoyId == -1) return;

	for(int iy=client.tileViewBox[1]; iy < client.tileViewBox[3]; iy++)
	for(int ix=client.tileViewBox[0]; ix < client.tileViewBox[2]; ix++){
		Rectangle tileRec = {
			ix*LV_TILESIZE, iy*app.config.persp_y*LV_TILESIZE,
			LV_TILESIZE, LV_TILESIZE*app.config.persp_y
		};
		float brightness = 0.6;
		if(boy->fog.visible[ix][iy]){
			brightness = 1;
		}else if(boy->fog.visited[ix][iy]){
			brightness = 0.3;
		}
		if(brightness < 1){
			DrawRectangleRec(tileRec, Fade(BLACK, brightness));
		}
	}
}

Vector3 GetRandomUnexploredLocation(int boyId, int seed, float* outScore){
	Boy_t* boy = GetBoy(boyId);
	int lazyAttempts = 30;
	
	for(int i=0; i < lazyAttempts; i++){
		int ix = (789*seed+432) % game.level.mapSize[0];
		int iy = (267*seed+834) % game.level.mapSize[1];
		if(!boy->fog.visited[ix][iy]){
			*outScore = 1 - 0.5 * ((float)i / lazyAttempts);
			return (Vector3){ix*LV_TILESIZE, iy*LV_TILESIZE, 0};
		}
	}

	for(int iy=0; iy < game.level.mapSize[1]; iy++)
	for(int ix=0; ix < game.level.mapSize[0]; ix++){
		if(!boy->fog.visited[ix][iy]){
			*outScore = 0.25;
			return (Vector3){ix*LV_TILESIZE, iy*LV_TILESIZE, 0};
		}
	}

	*outScore = 0;
	return (Vector3){0.5*game.level.mapSize[0]*LV_TILESIZE, 0.5*game.level.mapSize[1]*LV_TILESIZE, 0};
}

static const char CircleMap[] = {
    0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,
    0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
    0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,
    0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0,
    0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,
};

/** ```js
const radius = 16;
const size = radius*2;
const center = radius-0.5;

let res = "{";
for(let iy=0; iy < size; iy++){
	res += "\n\t";
  for(let ix=0; ix < size; ix++){
		let inside = Math.sqrt(Math.pow(ix-center,2)+Math.pow(iy-center,2)) < radius;
    res += (inside?'1':'0')+",";
  }
}
res+="\n};";
console.log(res);
``` */

#undef IMP_FOGOFWAR_H
#endif
