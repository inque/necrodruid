#include "gameapp.h"
#include "effects.h"
#include "utils.h"
#include "stdio.h"

PlayCardClass_t cardClasses[_PLAYCARD_COUNT] = {
	[PLAYCARD_DUMMY] = {
		.name = "Errno",
		.type = PLAYCARD_CLASS_SPECIAL
	},
	[PLAYCARD_SUMMON_SKELETON] = {
		.name = "Summon skeletons",
		.type = PLAYCARD_CLASS_SUMMON,
		.summon = {
			.toyType = TOY_SKELLY,
			.amount = 25,
			.minDist = 100,
			.maxDist = 380
		}
	},
	[PLAYCARD_SUMMON_TIGRA] = {
		.name = "Summon tigers",
		.type = PLAYCARD_CLASS_SUMMON,
		.summon = {
			.toyType = TOY_TIGRA,
			.amount = 8,
			.minDist = 100,
			.maxDist = 250
		}
	},
	[PLAYCARD_GIVE_SHOTGUN] = {
		.name = "Get a boomstick",
		.type = PLAYCARD_CLASS_ITEM,
		.item = {.itemType = ITEM_SHOTGUN}
	}
};

const int cardDespawnDur = (TICKS_PER_SEC/4);

int GiveCard(int boyId, PlayCard_e type){
	Boy_t* boy = GetBoy(boyId);

	for(int i=0; i < BOY_MAX_CARDS; i++){
		PlayCard_t* dst = &boy->cards[i];
		if(dst->active) continue;

		*dst = (PlayCard_t){
			.active = true,
			.usable = true,
			.type = type
		};
		return i;
	}
	return -1;
}

bool BoyUseCard(int boyId, int slot){
	Boy_t* boy = GetBoy(boyId);
	Toy_t* toy = GetToy(boy->toyId);
	PlayCard_t* card = &boy->cards[slot];
	PlayCardClass_t* cardClass = &cardClasses[card->type];

	if(boy->toyId == -1) return false;

	Vector3 pos = PeekPos(&toy->limbs[0].tform);
	
	switch(cardClass->type){
		case PLAYCARD_CLASS_SUMMON:

			for(int i=0; i < cardClass->summon.amount; i++){
				float angle = fmodf(randf(), 4*3.14);
				float distance = cardClass->summon.minDist + randf() * (cardClass->summon.maxDist - cardClass->summon.minDist);
				Vector3 spawnPos = {pos.x+distance*sinf(angle), pos.y+distance*cosf(angle), pos.z};

				SpawnToy(cardClass->summon.toyType, true, boyId, spawnPos);
			}

			break;
		case PLAYCARD_CLASS_ITEM:
			SpawnItem(cardClass->item.itemType, boy->toyId, Vector3Zero());
			break;
		case PLAYCARD_CLASS_SPECIAL:
			break;
	}

	card->usable = false;
	card->despawnTimer = cardDespawnDur;
	ApplyEffect((EffectParams_t){
		.type = EFFECT_CARD_USE,
		.pos = Vector3Zero(),
		.amount = 13
	}, boyId);

	return true;
}

void DrawCard(PlayCard_t* card, Vector2 pos, Color tint){
	PlayCardClass_t* cardClass = &cardClasses[card->type];
	float alpha = card->despawnTimer ? ((float)card->despawnTimer/cardDespawnDur) : 1;
	Color color = Fade(tint, alpha);
	float scale = 1;
	int spriteId = 0;
	char text[512];
	text[0] = 0;

	BlitSprite(client.assets.sprite.card, 0, pos, color, 1);
	switch(cardClass->type){
		case PLAYCARD_CLASS_SUMMON:
			ToyClass_t* toyClass = GetToyClass(cardClass->summon.toyType);
			spriteId = toyClass->sprite.icon;
			sprintf(text, "x%i", cardClass->summon.amount);

			break;
		case PLAYCARD_CLASS_ITEM:
			ItemClass_t* itemClass = GetItemClass(cardClass->item.itemType);
			spriteId = itemClass->sprite.icon;
			scale = 0.75;
			if(itemClass->isGun){
				sprintf(text, "%i", itemClass->gun.ammo);
			}

			break;
		case PLAYCARD_CLASS_SPECIAL:
			break;
	}

	BlitSprite(spriteId, 0, (Vector2){pos.x - 26, pos.y - 65}, color, 0.6 * scale);
	DrawText(text, pos.x - 35, pos.y - 55, 15, Fade(BLACK, alpha));
}
