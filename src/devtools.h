#ifndef DEVTOOLS_H

void EditorScreen();
void DebugOverlay();
void DebugWorld();

#define DEVTOOLS_H
#endif
#ifdef IMP_DEVTOOLS_H
//==== implementation starts here ====
#include "gameapp.h"
#include "editor_level.h"
#include "editor_sprites.h"
#include "raylib.h"
#include "stdio.h"

static struct{
	int editorMode;
	bool debugEnabled;
} dev;

void EditorScreen(){
	Rectangle wnd = {80, client.height-60-10, 200, 60};

	if(!game.initialized) ResetGame();
	switch(dev.editorMode){
		case 0:
			LevelEditor();
			break;
		case 1:
			SpriteEditor();
			break;
	}

	if(GuiWindowBox(wnd, "devtools") || IsKeyPressed(KEY_ESCAPE)){
		CloseGame(true);
		client.screen = SCREEN_MAINMENU;
		return;
	}
	CutTop(&wnd, 20);
	Extend(&wnd, -5);
	if(IsKeyPressed(KEY_F2) || GuiToggle(CutLeft(&wnd, 80), "Map editor", dev.editorMode==0)) dev.editorMode = 0;
	CutLeft(&wnd, 10);
	if(IsKeyPressed(KEY_F3) || GuiToggle(CutLeft(&wnd, 80), "Spritesheets", dev.editorMode==1)) dev.editorMode = 1;
	// if(IsKeyPressed(KEY_F4)){
	// 	CloseGame(true);
	// 	StartGame(ROLE_LOCAL);
	// 	client.screen = SCREEN_GAME;
	// }
}

void DebugOverlay(){
	char text[1024];
	if(IsKeyPressed(KEY_F4)) dev.debugEnabled = !dev.debugEnabled;
	if(!dev.debugEnabled) return;

	if(IsKeyPressed(KEY_T)){
		Vector2 point = GetScreenToWorld2D(GetMousePosition(), client.cam);

		forEachBoy(boy, boyId){
			if(boy->isRobot){
				int toyId = SpawnToy(TOY_SKELLY, true, boyId, Unproject2d(point, 0));
				break;
			}
		}
	}

	if(IsKeyPressed(KEY_P)){
		Vector2 point = GetScreenToWorld2D(GetMousePosition(), client.cam);
		ApplyEffect((EffectParams_t){
			.type = EFFECT_FATALITY_KILL,
			.pos = {point.x, point.y, 0},
			.amount = 10
		}, -1);
	}

	{
		static bool editingTickrate;
		static int newTickRate = 30;
		Rectangle wnd = {client.width-270, client.height-330, 260, 280};
		GuiWindowBox(wnd, "settings");
		CutTop(&wnd, 25);
		Extend(&wnd, -5);
		CutLeft(&wnd, 80);

		app.config.persp_y = GuiSlider(CutTop(&wnd, 26), "Perspective:", "", app.config.persp_y, 0.5, 1.5);
		sprintf(text, "%f", app.config.persp_y);
		GuiLabel(CutTop(&wnd, 26), text);

		if(GuiValueBox(CutTop(&wnd, 26), "Tick rate:", &newTickRate, 1, 100, editingTickrate)){
			editingTickrate = !editingTickrate;
		}
		if(newTickRate > 120) newTickRate = 120;
		if(newTickRate > 0) app.config.tickrate = newTickRate;

		app.config.tmpMulForce = GuiSlider(CutTop(&wnd, 26), "Mult force:", "", app.config.tmpMulForce, 0.1, 3.0);
		sprintf(text, "%f", app.config.tmpMulForce);
		GuiLabel(CutTop(&wnd, 26), text);

		app.config.tmpMulFrames = GuiSlider(CutTop(&wnd, 26), "Mult frames:", "", app.config.tmpMulFrames, 0.1, 3.0);
		sprintf(text, "%f", app.config.tmpMulFrames);
		GuiLabel(CutTop(&wnd, 26), text);

		app.config.tmpDrag = GuiSlider(CutTop(&wnd, 26), "FRICTION:", "", app.config.tmpDrag, 0.05, 1.0);
		sprintf(text, "%f", app.config.tmpDrag);
		GuiLabel(CutTop(&wnd, 26), text);
		cpSpaceSetDamping(&game.space, 1 - app.config.tmpDrag);
		
		client.chat.inputOpen = false;
	}
	
	sprintf(text, "m:%i. s:%i", game.mode, game.state);
	DrawText(text, 20, client.height-30, 20, GREEN);

	DrawFPS(client.width-150, client.height-50);
}

static const char* ToyAutomataToText(Toy_t* toy, const char* text){
	switch(toy->automation.action.type){
		case ACTION_WHATEVER:
			return "action: none";
		case ACTION_WALK:
			return "action: walking";
		case ACTION_IDLE:
			return "action: idling";
		case ACTION_ATTACK_TOY:
			sprintf((char*)text, "action: attacking\n target: %i", toy->automation.action.attackToy.targetToyId);			
			return text;
		default:
			sprintf((char*)text, "action: %i", toy->automation.action.type);
			return text;
	}
}

void DebugWorld(){
	char text[4096];
	if(!dev.debugEnabled) return;

	forEachToy(toy, toyId){
		if(!IsVisible(&toy->limbs[0].tform)) continue;
		Vector2 pos = Project2d(GetPos(&toy->limbs[0].tform));
		
		sprintf(text, "anim: %i.\n\nnav: %i.\nteam:%i", toy->animation.state, toy->automation.navigation.active, GetBoy(toy->boyId)->team);
		DrawText(text, pos.x+40, pos.y-30, 10, GREEN);

		DrawCircle(toy->loco.point[0].x, toy->loco.point[0].y*app.config.persp_y, 5, GREEN);
		DrawCircle(toy->loco.point[1].x, toy->loco.point[1].y*app.config.persp_y, 5, BLUE);

		if(toy->automation.enabled){
			DrawText(ToyAutomataToText(toy, text), pos.x+40, pos.y-20, 10, PURPLE);
			sprintf(text, "%i %i %i %i %i %i %i %i", toy->automation.dbg[0], toy->automation.dbg[1], toy->automation.dbg[2], toy->automation.dbg[3], toy->automation.dbg[4], toy->automation.dbg[5], toy->automation.dbg[6], toy->automation.dbg[7]);
			DrawText(text, pos.x+40, pos.y+30, 10, BLACK);
			if(toy->automation.navigation.active){
				Vector2 target = Project2d(toy->automation.navigation.target);
				DrawLine(pos.x, pos.y, target.x, target.y, BLUE);
			}
		}
	}

}

#undef IMP_DEVTOOLS_H
#endif
