#ifndef UTILS_H

int mod2(int a, int b); /// modulo operator, but negative numbers wrap
int Millisecs();        /// current time in milliseconds
void Delay(int ms);     /// sleep
float SmoothOver(float cv, float st, float dt); /// fps-independent lerp
void RemoveElement(void* array, int index, int length, size_t elementSize); ///remove jenga block
void RemoveElements(void* array, int index, int count, int length, size_t elementSize); //same but multiple
void InsertElements(void* array, int index, int count, int length, size_t elementSize);
void SanitizeString(char* str, bool filesystem);
void ApplyPlatformSpecificHacks();

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define randf() ((float)rand() / RAND_MAX) /// random float
#define CHR4(d,c,b,a) ( ((a)<<24) | ((b)<<16) | ((c)<<8) | (d) ) /// int32 string

static inline void IncPtr(void** ptr){
	char **tmpPtr = (char**)ptr;
		++*tmpPtr;
}

static inline void AddPtr(void** ptr, int bytes){
	char **tmpPtr = (char**)ptr;
		  *tmpPtr += bytes;
}

static inline int DecCharId(unsigned char value){
	return (value == 255) ? -1 : value;
}

static inline unsigned char EncCharId(int value){
	return (value == -1) ? 255 : (unsigned char)value;
}

#define UTILS_H
#endif
#ifdef IMP_UTILS_H
//==== implementation starts here ====

#include "stdlib.h"
#include "math.h"
#include "time.h"
#ifdef _WIN32
    #include "safe_windows.h"
	#include "windows.h"
#else
	#include "sys/stat.h"
	#include "utime.h"
	#include "unistd.h"
#endif
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
	#include <emscripten/html5.h>
#endif

int mod2(int a, int b){
	int r = a % b;
	return r < 0 ? r + b : r;
}

int Millisecs(){
	#ifdef _WIN32
		return (int)clock();
	#else
		struct timespec ts;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		static unsigned long long int baseTime;
		unsigned long long int time = (unsigned long long int)ts.tv_sec*1000000000LLU + (unsigned long long int)ts.tv_nsec;
		if(!baseTime){
			baseTime = time;
			return 0;
		}else{
			return (int)((time - baseTime)*1e-9 * 1000);
		}
	#endif
}

void Delay(int ms){
	#ifdef _WIN32
		Sleep(ms);
	#elif defined(__EMSCRIPTEN__)
		return;
	#elif defined(__linux__)
		struct timespec req = { 0 };
		time_t sec = (int)(ms/1000.0f);
		ms -= (float)(sec*1000);
		req.tv_sec = sec;
		req.tv_nsec = ms*1000000L;

		while (nanosleep(&req, &req) == -1) continue;
	#elif defined(__APPLE__)
		usleep(ms*1000.0f);
	#endif
}

float SmoothOver(float cv, float st, float dt){
	return 1 - powf((1 - cv), (dt / st));
}

void RemoveElement(void* array, int index, int length, size_t elementSize){
	RemoveElements(array, index, 1, length, elementSize);
}

void RemoveElements(void* array, int index, int count, int length, size_t elementSize){
	char* arrayPtr = (char*)array;
	int toMove = length - index - count;
	if(toMove > 0){
		void* dst = (void*)(arrayPtr + index*elementSize);
		void* src = (void*)(arrayPtr + (index + count)*elementSize);
		memmove(dst, src, toMove * elementSize);
	}
}

void InsertElements(void* array, int index, int count, int length, size_t elementSize){
	char* arrayPtr = (char*)array;
	int toMove = length - index;
	if(toMove > 0){
		void* dst = (void*)(arrayPtr + (index + count)*elementSize);
		void* src = (void*)(arrayPtr + index*elementSize);
		memmove(dst, src, toMove * elementSize);
	}
}

void SanitizeString(char* str, bool filesystem){
	while(*str){
		if(*str < 32){
			*str = '?';
		}else if(filesystem){
			bool disableSlash = true;
			if( ((*str > 32) && (*str < '/')) || (disableSlash && (*str == '/')) || ((*str > '9') && (*str < 'A')) || ((*str > 'Z') && (*str < 'a') && (*str != '_')) || (*str > 'z')){
				*str = '_';
			}
		}
		str++;
	}
}

#ifdef __EMSCRIPTEN__

static EM_BOOL onEmscSizeChanged(int event_type, const EmscriptenUiEvent* ui_event, void* user_data){
	double w, h;
	float dpi_scale;
	emscripten_get_element_css_size("canvas", &w, &h);
	if(w < 1){
		w = ui_event->windowInnerWidth;
	}
	if(h < 1){
		h = ui_event->windowInnerHeight;
	}
	dpi_scale = emscripten_get_device_pixel_ratio();
	int width = (int)(w*dpi_scale);
	int height = (int)(h*dpi_scale);
	emscripten_set_canvas_element_size("canvas", width, height);

	EM_ASM_INT({
		GLFW.onCanvasResize($0, $1);
	}, width, height);
	return true;
}

#endif

void ApplyPlatformSpecificHacks(){
    #ifdef __EMSCRIPTEN__
        //fix canvas resizing
        emscripten_set_resize_callback(EMSCRIPTEN_EVENT_TARGET_WINDOW, 0, false, onEmscSizeChanged);
    #endif
}

#undef IMP_UTILS_H
#endif
