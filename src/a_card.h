#pragma once
#include "stdbool.h"
#include "raymath.h"

typedef enum{
	PLAYCARD_DUMMY,
	PLAYCARD_SUMMON_SKELETON,
	PLAYCARD_SUMMON_TIGRA,
	PLAYCARD_GIVE_SHOTGUN,
	_PLAYCARD_COUNT
} PlayCard_e;

typedef struct{
	int active;
	PlayCard_e type;
	bool usable;
	int despawnTimer;
} PlayCard_t;

typedef enum{
	PLAYCARD_CLASS_SPECIAL,
	PLAYCARD_CLASS_SUMMON,
	PLAYCARD_CLASS_ITEM
} PlayCardClassType_e;

typedef struct{
	char name[32];
	PlayCardClassType_e type;
	union{
		struct{
			int toyType;
			int amount;
			float minDist;
			float maxDist;
		} summon;
		struct{
			int itemType;
		} item;
	};
} PlayCardClass_t;

extern PlayCardClass_t cardClasses[_PLAYCARD_COUNT];

int GiveCard(int boyId, PlayCard_e type);
bool BoyUseCard(int boyId, int slot);
void DrawCard(PlayCard_t* card, Vector2 pos, Color tint);
