#include "gameapp.h"
#include "a_toy_class.h"
#include "a_boy.h"
#include "effects.h"
#include "fogofwar.h"
#include "stdbool.h"

static void Dematerialize(Toy_t* toy, bool toBeRemoved){
	cpSpace* space = &game.space;
	forEachLimb(toy, limb, limbId){
		if(limb->body)  cpSpaceRemoveBody(space, limb->body);
		if(limb->shape) cpSpaceRemoveShape(space, limb->shape);
		if(limb->joint) cpSpaceRemoveConstraint(space, limb->joint);
		if(limb->motor) cpSpaceRemoveConstraint(space, limb->motor);
		if(!toBeRemoved){
			limb->body  = NULL;
			limb->shape = NULL;
			limb->joint = NULL;
			limb->motor = NULL;
			PetrifyTransform(&limb->tform);
		}
	}
	if(!toBeRemoved){
		toy->dash.timer = 0;
		toy->melee.preparation = 0;
	}
}

static void Materialize(int toyId, Position_t pos){
	cpSpace* space = &game.space;
	Toy_t* toy = GetToy(toyId);
	ToyClass_t* toyClass = GetToyClass(toy->type);

	toy->loco.point[0] = (Vector3){pos.x-toyClass->radius*0.4, pos.y, pos.z};
	toy->loco.point[1] = (Vector3){pos.x, pos.y+toyClass->radius*0.4, pos.z};
	toy->loco.lastChangeDelta = 10;

	switch(toy->type){
		Limb_t* limb;
		cpBody* body;
		float radius;
		float mass, moment;

		default: {
			//the body circle
			mass = toyClasses->mass;
			radius = toyClasses->radius;
			moment = cpMomentForCircle(mass, 0, radius, cpvzero);
			body = cpSpaceAddBody(space, cpBodyNew(mass, moment));
			limb = &toy->limbs[0];
			*limb = (Limb_t){
				.active = 1,
				.body = body,
				.shape = cpSpaceAddShape(space, cpCircleShapeNew(body, radius, cpvzero)),
				.tform = {.size = {radius, radius}}
			};
		} break;
	}
	forEachLimb(toy, limb, limbId){
		if(limb->body){
			cpBodySetPosition(limb->body, cpvadd(cpv(pos.x, pos.y), cpBodyGetPosition(limb->body)));
		}
		if(limb->shape){
			limb->shapeData = (ShapeData_t){
				.type = SHAPE_TOY,
				.toyId = toyId,
				.limbId = limbId
			};
			cpShapeSetUserData(limb->shape, &limb->shapeData);
			cpShapeSetCollisionType(limb->shape, COLLISION_TYPE_TOY);
		}
	}
}

bool IsToyFriendly(int a_toyId, int b_toyId){
	Toy_t* toy_a = GetToy(a_toyId);
	Toy_t* toy_b = GetToy(b_toyId);
	if((toy_a->boyId != -1) && (toy_b->boyId != -1)){
		Boy_t* boy_a = GetBoy(toy_a->boyId);
		Boy_t* boy_b = GetBoy(toy_b->boyId);
		return (boy_a->team == boy_b->team);
	}
	return false;
}

bool TakeDamage(int toyId, int dmg){
	Toy_t* toy = GetToy(toyId);
	ToyClass_t* toyClass = GetToyClass(toy->type);
	toy->lastTakenDamageFrame = game.frame;
	toy->health -= dmg;
	if(toy->health <= 0){
		if(toy->immortal && (dmg < 9000)){
			toy->health = 1;
			toy->walk.fallenTimer = 15*TICKS_PER_SEC;
		}else{
			toy->health = 0;
			toy->dematerializeTimer = 1;
		}
		return true;
	}else{
		if(toy->boyId != -1){
			Boy_t* boy = GetBoy(toy->boyId);
			if(boy->toyId == toyId){
				ApplyEffect((EffectParams_t){
					.amount = dmg,
					.type = EFFECT_DAMAGE_LOCAL
				}, toy->boyId);
			}
		}
	}
	toy->fatality.timer = 0;
	return false;
}

typedef struct {
	int toyId;
	MeleeAttackType_t* attack;
	float bonus;
} MeleeAttackEvent_t;

static void MeleeAttackQueryCallback(cpShape *shape, cpVect point, cpVect normal, cpFloat alpha, void* data){
	MeleeAttackEvent_t* event = (MeleeAttackEvent_t*)data;
	ShapeData_t* shapeData = (ShapeData_t*)cpShapeGetUserData(shape);
	
	if(shapeData && (shapeData->type == SHAPE_TOY)){
		if(shapeData->toyId != event->toyId){
			Toy_t* attackerToy = GetToy(event->toyId);
			Toy_t* toy = GetToy(shapeData->toyId);
			ToyClass_t* toyClass = GetToyClass(toy->type);
			Vector2 direction = Vector2Normalize(Vector2Subtract(toy->limbs[0].lastPos, attackerToy->limbs[0].lastPos));
			int dmg = event->attack->damage * event->bonus;
			if(IsToyFriendly(shapeData->toyId, event->toyId)) return; //no friendly fire
			if(event->attack->selfKnockback != 0.0f){
				if(game.frame != toy->melee.selfKnockbackLastAppliedFrame){
					float force = MUL_FORCE * 25 * event->attack->selfKnockback * event->bonus;
					cpBodyApplyImpulseAtWorldPoint(attackerToy->limbs[0].body, toCpv(Vector2Scale(direction, -force)), cpvzero);
					toy->melee.selfKnockbackLastAppliedFrame = game.frame;
				}
			}
			if(event->attack->enemyKnockback != 0.0f){
				float force = MUL_FORCE * 25 * event->attack->enemyKnockback * event->bonus;
				cpBodyApplyImpulseAtWorldPoint(toy->limbs[0].body, toCpv(Vector2Scale(direction, force)), cpvzero);
			}
			TakeDamage(shapeData->toyId, dmg);
			ApplyEffect((EffectParams_t){
				.type = EFFECT_DAMAGE_TOY,
				.amount = 6 + (dmg / 4),
				.pos = (Vector3){point.x, point.y - toyClass->height/2, 0},
				.velocity = (Vector3){normal.x, normal.y, 0}
			}, -1);
		}
	}
}

static bool PerformMeleeAttack(int toyId, MeleeAttackType_t* attack, float bonus){
	Toy_t* toy = GetToy(toyId);
	Vector3 pos = PeekPos(&toy->limbs[0].tform);
	float distBonus = MIN(1.5, bonus);
	Vector2 endPoint = Vector2Add(Vec2(pos), Vector2Scale(toy->melee.direction, attack->rangeDist * distBonus));
	//fixme: filter for COLLISION_TYPE_TOY
	cpShapeFilter filter = {CP_NO_GROUP, CP_ALL_CATEGORIES, CP_ALL_CATEGORIES};
	MeleeAttackEvent_t event = {
		.toyId = toyId,
		.attack = attack,
		.bonus = bonus
	};
	cpSpaceSegmentQuery(&game.space, toCpv(pos), toCpv(endPoint), attack->rangeRadius * distBonus, filter, (cpSpaceSegmentQueryFunc)MeleeAttackQueryCallback, (void*)&event);
	return false;
}

static bool ToyUpdateFatality(int toyId){
	Toy_t* toy = GetToy(toyId);
	ToyClass_t* toyClass = GetToyClass(toy->type);
	Vector3 ourPos = PeekPos(&toy->limbs[0].tform);
	toy->fatality.canPerform = false;

	if(!toy->fatalizer) return false;
	if(toy->walk.fallenTimer) return false;

	if(toy->fatality.timer){
		if(toy->animation.state != ANIM_FATALITY){
			toy->animation.state = ANIM_FATALITY;
			toy->animation.lastChangeFrame = game.frame;
		}
		if(!--toy->fatality.timer){
			int victimId = toy->fatality.toyId;
			Toy_t* victim = GetToy(victimId);
			if(victim->boyId != -1){
				Boy_t* boy = GetBoy(victim->boyId);
				if(boy->toyId == victimId){
					boy->toyId = -1;
				}
			}
			forEachToy(enemyToy, enemyToyId){
				if(enemyToy->boyId != victim->boyId) continue;
				if(!enemyToy->health) continue;
				TakeDamage(enemyToyId, 9000);
			}
			ApplyEffect((EffectParams_t){
				.type = EFFECT_FATALITY_KILL,
				.pos = PeekPos(&victim->limbs[0].tform),
				.amount = 10
			}, -1);
		}
	}else{
		forEachToy(enemyToy, enemyToyId){
			ToyClass_t* enemyToyClass = GetToyClass(enemyToy->type);
			float dist;
			if(enemyToyId == toyId) continue;
			if(!enemyToy->immortal) continue;
			if(!APPROX_NEAR(toy->sector, enemyToy->sector, 2)) continue;
			if((!enemyToy->walk.fallenTimer) || (enemyToy->health > 20)) continue;
			if(IsToyFriendly(toyId, enemyToyId)) continue;
			if(enemyToy->health <= 0) continue;
			
			dist = Vector3Distance(ourPos, PeekPos(&enemyToy->limbs[0].tform));
			if(dist < 200){
				toy->fatality.canPerform = true;
				toy->fatality.toyId = enemyToyId;
				break;
			}
		}
	}

	if(toy->fatality.canPerform){
		if(ToyButtonPressed(toy, BTN_FIRE)){
			Toy_t* victim = GetToy(toy->fatality.toyId);
			ApplyEffect((EffectParams_t){.type = EFFECT_FATALITY_START}, -1);
			toy->fatality.timer = 4*TICKS_PER_SEC;
			victim->walk.fallenTimer += toy->fatality.timer;
			return true;
		}
	}
	return false;
}

static bool IsValidEnemy(Toy_t* enemyToy){
	ToyClass_t* enemyToyClass = GetToyClass(enemyToy->type);
	bool legitEntity = enemyToy->active && enemyToy->health && enemyToy->limbs[0].body;
	if(!legitEntity) return false;
	if(enemyToy->immortal){
		if(enemyToy->walk.fallenTimer && (enemyToy->health < 10)) return false;
	}
	return true;
}

static int FindBestEnemyNearby(int toyId, int* outScore){
	Toy_t* toy = GetToy(toyId);

	int bestToyId = -1;
	int bestScore = -1;
	forEachToy(enemyToy, enemyToyId){
		ToyClass_t* enemyToyClass = GetToyClass(enemyToy->type);
		float dist;
		bool friendly;
		if(enemyToyId == toyId) continue;
		if(!APPROX_NEAR(enemyToy->sector, toy->sector, 4)) continue;
		if((!enemyToy->health) || (!enemyToy->limbs[0].body)) continue;
		friendly = IsToyFriendly(toyId, enemyToyId);
		
		if(!friendly){
			if(!CanSeeToy(toy->boyId, enemyToyId)) continue;
			if(!IsValidEnemy(enemyToy)) continue;
			dist = Vector2Distance(enemyToy->limbs[0].lastPos, toy->limbs[0].lastPos);
			int score = 100 - dist/70 - enemyToy->health/12 + enemyToyClass->ai.cost/10;
			if(enemyToy->health < toy->health/2) score += enemyToyClass->ai.cost/2;
			if((bestToyId == -1) || (score > bestScore)){
				bestToyId = enemyToyId;
				bestScore = score;
			}
		}
	}
	*outScore = bestScore;
	return bestToyId;
}

static BoyInputButton_e ToyMeleeButton[] = { BTN_FIRE, BTN_ALTFIRE };

static void UpdateAutomation(int toyId){
	Toy_t* toy = GetToy(toyId);
	ToyClass_t* toyClass = GetToyClass(toy->type);
	ToyAction_t* action = &toy->automation.action;
	Vector3 ourPos = PeekPos(&toy->limbs[0].tform);

	if(game.state != STATE_PLAYING) return;
	
	if(toy->fatality.canPerform){
		toy->inputs.buttons = (game.frame&7) ? 0 : BTN_FIRE;
		return;
	}

	if(toy->automation.navigation.active){
		Vector3 target = toy->automation.navigation.target;
		Vector2 direction = Vector2Normalize(Vector2Subtract(Vec2(target), Vec2(ourPos)));
		toy->inputs.axes[0] = direction.x;
		toy->inputs.axes[1] = direction.y;
	}

	if((toy->item.lookingAtItemId != -1) && toyClass->canPickUpItems){
		toy->inputs.buttons |= BTN_USE;
	}

	if(toy->immortal && (toy->boyId != -1)){
		Boy_t* boy = GetBoy(toy->boyId);
		if(!toy->useCard.cooldown){
			if(!action->useCardRecheckTimer--){
				int threshold;
				int allyScore=0, enemyScore=0;
				forEachToy(otherToy, otherToyId){
					if(!CanSeeToy(toy->boyId, otherToyId)) continue;
					int score = (int)(toyClasses[otherToy->type].ai.cost * ((float)otherToy->health/otherToy->maxHealth));
					if(otherToy->boyId != toy->boyId){
						enemyScore += score;
					}else{
						allyScore += score;
					}
				}
				action->useCardAccum++;
				threshold = (allyScore - enemyScore)/7 + toy->health/3;
				//printf("%i / %i\n", action->useCardAccum, threshold);
				if(action->useCardAccum > MIN(40, threshold)){
					int cardNum=0;
					for(int i=0; i < BOY_MAX_CARDS; i++){
						PlayCard_t* card = &boy->cards[i];
						if(!card->active) continue;
						cardNum++;
						if(card->usable){
							toy->inputs.action.type = BOY_ACTION_USECARD;
							toy->inputs.action.useCard.num = cardNum;
							break;
						}
					}
					action->useCardAccum = 0;
				}
				action->useCardRecheckTimer = TICKS_PER_SEC/4;
			}
		}

		if(toy->health < 25){
			if(action->panicTimer){
				action->panicTimer--;
			}else{
				action->type = ACTION_RETREAT;
				action->panicTimer=60*TICKS_PER_SEC;
			}
		}
	}

	switch(action->type){
		case ACTION_WHATEVER: {
			// look for enemies nearby to attack
			int bestScore;
			int bestToyId = FindBestEnemyNearby(toyId, &bestScore);
			if(bestToyId != -1){
				// attack
				action->type = ACTION_ATTACK_TOY;
				action->attackToy.targetToyId = bestToyId;
				action->attackToy.recheckTimer = TICKS_PER_SEC;
				break;
			}else{
				// give up
				Boy_t* boy = GetBoy(toy->boyId);
				if((toy->boyId == -1) || (boy->heroId == -1) || (boy->heroId == toyId)){
					action->type = ACTION_IDLE;
					action->idle.timer = TICKS_PER_SEC/2;
				}else{
					Toy_t* heroToy = GetToy(boy->heroId);
					Vector3 heroPos = PeekPos(&heroToy->limbs[0].tform);
					float distanceToHero = Vector3Distance(ourPos, heroPos);
					if(distanceToHero < 2750){
						action->type = ACTION_SCOUT;
					}else{
						action->type = ACTION_WALK;
						action->walk.target = heroPos;
						action->walk.radius = 1000;
						action->walk.timer = 10*TICKS_PER_SEC;
						action->walk.away = false;
						action->walk.checkEnemiesNearbyTimer = TICKS_PER_SEC;
						action->walk.attackEnemyScoreThreshold = 95;
					}
				}
			}
		} break;
		case ACTION_IDLE:
			if(action->idle.timer){
				action->idle.timer--;
			}else{
				action->type = ACTION_WHATEVER;
			}
			break;
		case ACTION_SCOUT: {
			float score;
			Vector3 target = GetRandomUnexploredLocation(toy->boyId, (toy->boyId*322+toyId*843+game.frame), &score);
			if(score > 0){
				action->type = ACTION_WALK;
				action->walk.away = false;
				action->walk.radius = 350;
				action->walk.timer = 5*TICKS_PER_SEC;
				action->walk.target = target;
				action->walk.checkEnemiesNearbyTimer = TICKS_PER_SEC;
				action->walk.attackEnemyScoreThreshold = 60;
			}else{
				action->type = ACTION_IDLE;
				action->idle.timer = TICKS_PER_SEC;
			}
		} break;
		case ACTION_RETREAT: {
			int bestScore;
			int bestToyId = FindBestEnemyNearby(toyId, &bestScore);
			if((bestToyId == -1) || (bestScore < 100)){
				action->type = ACTION_WHATEVER;
			}else{
				Toy_t* enemyToy = GetToy(bestToyId);
				if(enemyToy->health < toy->health){
					action->type = ACTION_ATTACK_TOY;
					action->attackToy.targetToyId = bestToyId;
					action->attackToy.recheckTimer = TICKS_PER_SEC;
				}else{
					action->type = ACTION_WALK;
					action->walk.away = true;
					action->walk.target = PeekPos(&enemyToy->limbs[0].tform);
					action->walk.radius = 700;
					action->walk.timer = 20*TICKS_PER_SEC;
					action->walk.checkEnemiesNearbyTimer = -1;
				}
			}
		} break;
		case ACTION_WALK: {
			float distance = Vector3Distance(ourPos, action->walk.target);
			Vector3 direction = Vector3Normalize(Vector3Subtract(action->walk.target, ourPos));
			//float disparity = Vector2Distance(Vec2(direction), toy->walk.dampedDir);
			//bool lookingInTheRightDirection = (disparity < 0.34);

			if(action->panicTimer){
				if((!toy->dash.timer) && (!toy->dash.cooldown)){
					toy->inputs.buttons |= BTN_JUMP;
				}
			}
			if(action->walk.away){
				if(distance > action->walk.radius){
					action->type = ACTION_WHATEVER;
					toy->automation.navigation.active = false;
				}else{
					toy->automation.navigation.active = true;
					toy->automation.navigation.target = Vector3Add(ourPos, Vector3Scale(direction, -250));
				}
			}else{
				if(distance < action->walk.radius){
					action->type = ACTION_WHATEVER;
					toy->automation.navigation.active = false;
				}else{
					toy->automation.navigation.active = true;
					toy->automation.navigation.target = action->walk.target;
				}
			}
			if(action->walk.timer){
				action->walk.timer--;
			}else{
				action->type = ACTION_WHATEVER;
				toy->automation.navigation.active = false;
			}
			if(!action->walk.checkEnemiesNearbyTimer){
				action->walk.checkEnemiesNearbyTimer = TICKS_PER_SEC;
				int bestScore;
				int bestToyId = FindBestEnemyNearby(toyId, &bestScore);
				if(bestToyId != -1){
					if(bestScore >= action->walk.attackEnemyScoreThreshold){
						action->type = ACTION_ATTACK_TOY;
						action->attackToy.targetToyId = bestToyId;
						action->attackToy.recheckTimer = TICKS_PER_SEC;
					}
				}
			}
		} break;
		case ACTION_ATTACK_TOY: {
			int enemyToyId = action->attackToy.targetToyId;
			Toy_t* enemyToy = GetToy(enemyToyId);
			ToyClass_t* enemyToyClass = GetToyClass(enemyToy->type);
			bool canStillSee = CanSeeToy(toy->boyId, enemyToyId);
			bool justKilledImmortal = (enemyToy->immortal && enemyToy->walk.fallenTimer && (enemyToy->health < 3));

			toy->automation.dbg[0] = 10;

			if(justKilledImmortal){
				int anotherEnemyScore;
				int anotherEnemyId = FindBestEnemyNearby(toyId, &anotherEnemyScore);
				if(anotherEnemyId != -1){
					action->type = ACTION_ATTACK_TOY;
					action->attackToy.targetToyId = anotherEnemyId;
					action->attackToy.recheckTimer = 3*TICKS_PER_SEC;
					break;
				}else{
					action->type = ACTION_WALK;
					action->walk.away = true;
					action->walk.target = PeekPos(&enemyToy->limbs[0].tform);
					action->walk.radius = 300;
					action->walk.timer = TICKS_PER_SEC/2;
					action->walk.checkEnemiesNearbyTimer = TICKS_PER_SEC;
					action->walk.attackEnemyScoreThreshold = 60;
					break;
				}
			}

			if( (!canStillSee) || (!IsValidEnemy(enemyToy)) ){
				action->type = ACTION_WHATEVER;
				break;
			}

			toy->automation.dbg[0] = 11;
			toy->automation.dbg[1] = action->attackToy.recheckTimer;

			if(action->attackToy.recheckTimer){
				action->attackToy.recheckTimer--;
			}else{
				Vector3 enemyPos = PeekPos(&enemyToy->limbs[0].tform);
				float distance = Vector3Distance(ourPos, enemyPos);
				bool canAct = !(toy->melee.preparation || toy->melee.cooldown || toy->dash.timer);
				bool acted = false;
				Vector2 diff = Vec2(Vector3Normalize(Vector3Subtract(enemyPos, ourPos)));
				float disparity = Vector2Distance(diff, toy->walk.dampedDir);
				bool lookingInTheRightDirection = (disparity < 0.15);

				toy->automation.navigation.active = true;
				toy->automation.navigation.target = enemyPos;

				if(toy->item.holdingItemId != -1){
					Item_t* item = GetItem(toy->item.holdingItemId);
					if((!toy->item.preparation) && (!item->cooldown)){
						if(game.frame&1) toy->inputs.buttons |= BTN_FIRE;
						// toy->inputs.axes[2] = diff.x;
						// toy->inputs.axes[3] = diff.y;
						toy->inputs.axes[2] = toy->walk.dampedDir.x;
						toy->inputs.axes[3] = toy->walk.dampedDir.y;
					}
				}else if(canAct){
					int seed = (4343*game.frame+32487*toyId);
					//try melee
					for(int variant=0; variant < 2; variant++){
						MeleeAttackType_t* attack = &toyClass->melee[variant];
						if((variant == 0) && ((seed&15) == 0)) continue;
						if(attack->damage && (distance < (attack->rangeDist + attack->rangeRadius))){
							toy->inputs.buttons |= ToyMeleeButton[variant];
							toy->inputs.axes[2] = toy->walk.dampedDir.x;
							toy->inputs.axes[3] = toy->walk.dampedDir.y;
							acted = true;
							break;
						}
					}
					//try dashing
					if((!acted) && toyClass->dash.duration){
						if((distance > 120) && (distance < 400)){
							if(lookingInTheRightDirection && (seed & 7)){
								toy->inputs.buttons |= BTN_JUMP;
								acted = true;
							}
						}
					}
				}

				//check up on allies
				if((distance > 400) && (distance < 750)){
					Vector3 correction;
					Vector3 direction;
					struct{
						int count;
						int score;
						float bbox[4];
						Vector3 center;
					} allies = {.bbox = {ourPos.x, ourPos.y, ourPos.x, ourPos.y}};
					forEachToy(allyToy, allyToyId){
						if(toy->boyId != allyToy->boyId) continue;
						if(allyToy->health < 10) continue;
						ToyClass_t* allyToyClass = GetToyClass(allyToy->type);
						Vector2 theirPos = allyToy->limbs[0].lastPos;
						float dist = Vector2Distance(toy->limbs[0].lastPos, theirPos);
						if(dist < 450){
							allies.count++;
							allies.score += allyToyClass->ai.cost;
							if(theirPos.x < allies.bbox[0]) allies.bbox[0] = theirPos.x;
							if(theirPos.y < allies.bbox[1]) allies.bbox[1] = theirPos.y;
							if(theirPos.x > allies.bbox[2]) allies.bbox[2] = theirPos.x;
							if(theirPos.y > allies.bbox[3]) allies.bbox[3] = theirPos.y;
						}
					}
					allies.center = (Vector3){
						(allies.bbox[0]+allies.bbox[2])/2,
						(allies.bbox[1]+allies.bbox[3])/2,
						0
					};
					direction = Vector3Normalize(Vector3Subtract(enemyPos, allies.center));
					allies.center = Vector3Subtract(allies.center, Vector3Scale(direction, 250));
					//spread up (in theory)
					correction = Vector3Scale(Vector3Subtract(ourPos, allies.center), 6.2);
					toy->automation.navigation.target = Vector3Add(toy->automation.navigation.target, correction);

					if(allies.score > enemyToyClass->ai.cost*30){
						toy->automation.action.type = ACTION_RETREAT;
					}
				}

				action->attackToy.recheckTimer = (distance > 400) ? TICKS_PER_SEC/2 : 6;

				toy->automation.dbg[0] = 12;
			}
		} break;
	}
}
