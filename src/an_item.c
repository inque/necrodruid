#include "gameapp.h"
#include "effects.h"
#include "geometry.h"
#include "utils.h"
#include "fogofwar.h"
#include "string.h"
#include "math.h"

ItemClass_t itemClasses[] = {
	[ITEM_SHOTGUN] = {
		.name = "Boomstick",
		.isGun = true,
		.gun = {
			.ammo = 18,
			.bulletsPerShot = 10,
			.damage = 12,
			.distance = 1300,
			.spread = 60,
			.knockbackEnemy = 3.7,
			.knockbackSelf = 1.9
		},
		.cooldown = TICKS_PER_SEC/2,
		.preparation = 6
	}
};

static void MaterializeItemSensor(Item_t* item, cpBody* body, cpVect pos){
	item->sensor = cpSpaceAddShape(&game.space, cpCircleShapeNew(body, 85, pos));
	cpShapeSetCollisionType(item->sensor, COLLISION_TYPE_SENSOR);
	cpShapeSetSensor(item->sensor, cpTrue);
	cpShapeSetUserData(item->sensor, (cpDataPointer)&item->sensorData);
}

static void MaterializeItem(Item_t* item, int itemId, Vector3 pos){
	float radius = 10;
	float mass = 0.4;
	float moment = cpMomentForCircle(mass, 0, radius, cpvzero);
	item->body = cpSpaceAddBody(&game.space, cpBodyNew(mass, moment));
	item->shape = cpSpaceAddShape(&game.space, cpCircleShapeNew(item->body, 50, cpvzero));	
	cpShapeSetUserData(item->shape, (cpDataPointer)&item->shapeData);
	cpBodySetPosition(item->body, toCpv(pos));
	//MaterializeItemSensor(item, item->body, cpvzero);
	UpdateTransformPos(&item->tform, pos);
}

static void DematerializeItem(Item_t* item, bool toBeRemoved, bool keepSensor){
	if(item->body) cpSpaceRemoveBody(&game.space, item->body);
	if(item->shape) cpSpaceRemoveShape(&game.space, item->shape);
	if(item->sensor) cpSpaceRemoveShape(&game.space, item->sensor);
	if(keepSensor){
		MaterializeItemSensor(item, cpSpaceGetStaticBody(&game.space), toCpv(PeekPos(&item->tform)));
	}
	if(!toBeRemoved){
		item->body = NULL;
		item->shape = NULL;
		if(!keepSensor) item->sensor = NULL;
		PetrifyTransform(&item->tform);
	}
}

int SpawnItem(ItemType_e type, int toyId, Vector3 pos){
	ItemClass_t* itemClass = &itemClasses[type];

	for(int itemId=0; itemId < MAX_ITEMS; itemId++){
		Item_t* item = GetItem(itemId);
		if(item->active) continue;

		memset(item, 0, sizeof(Item_t));
		item->active = true;
		item->type = type;
		item->ammo = itemClass->gun.ammo;
		item->heldByToyId = -1;
		item->retainedOwnerId = -1;
		item->shapeData = (ShapeData_t){
			.type = SHAPE_ITEM,
			.itemId = itemId
		};
		item->sensorData = (ShapeData_t){
			.type = SHAPE_ITEM_SENSOR,
			.itemId = itemId
		};
		if(toyId != -1){
			SetItemOwner(itemId, toyId);
		}else{
			item->heldByToyId = -1;
			MaterializeItem(item, itemId, pos);
		}
		return itemId;
	}
	return -1;
}

void RemoveItem(int itemId){
	Item_t* item = GetItem(itemId);
	item->active = false;
	DematerializeItem(item, true, false);
	if(item->heldByToyId != -1){
		Toy_t* toy = GetToy(item->heldByToyId);
		toy->item.holdingItemId = -1;
	}
}

void SetItemOwner(int itemId, int toyId){
	Item_t* item = GetItem(itemId);
	
	if(toyId == -1){
		Vector2 lookDir;
		Vector3 spawnPos = PeekPos(&item->tform);
		if(item->heldByToyId != -1){
			Toy_t* toy = GetToy(item->heldByToyId);
			ToyClass_t* toyClass = GetToyClass(toy->type);
			toy->item.holdingItemId = -1;
			lookDir = toy->walk.dampedDir;
			spawnPos.x += 150 * lookDir.x;
			spawnPos.y += 150 * lookDir.y;
			spawnPos.z += toyClass->height;
		}
		MaterializeItem(item, itemId, spawnPos);
		if(item->heldByToyId != -1){
			float force = 6 * MUL_FORCE;
			cpBodyApplyImpulseAtWorldPoint(item->body, cpvmult(toCpv(lookDir), force), cpvzero);
		}
	}else{
		Toy_t* toy = GetToy(toyId);
		if(toy->item.holdingItemId != -1)
		DematerializeItem(item, false, false);
		toy->item.holdingItemId = itemId;
		toy->item.preparation = 0;
		toy->melee.preparation = 0;
		toy->melee.cooldown = 0;
		toy->useCard.timer = 0;
		toy->fatality.timer = 0;
		item->retainedOwnerId = toyId;
		if(toy->boyId != -1){
			ApplyEffect((EffectParams_t){.type = EFFECT_PICKUP_GUN}, toy->boyId);
		}
	}
	item->heldByToyId = toyId;

}

typedef struct{
	int dmg;
	float knockback;
	cpVect startPoint;
	float maxDist;
	int initiatorToyId;
} BulletDamageInfo_t;

static void BulletDamageQueryCallback(cpShape *shape, cpVect point, cpVect normal, cpFloat alpha, void* data){
	BulletDamageInfo_t* dmgInfo = (BulletDamageInfo_t*)data;
	ShapeData_t* shapeData = (ShapeData_t*)cpShapeGetUserData(shape);
	
	if(shapeData && (shapeData->type == SHAPE_TOY)){
		Toy_t* toy = GetToy(shapeData->toyId);
		ToyClass_t* toyClass = GetToyClass(toy->type);
		float dist = (1 - (cpvdist(point, dmgInfo->startPoint) / dmgInfo->maxDist));
		int dmg = (int)(dmgInfo->dmg * (dist*(2-dist)) * (1 - toyClass->bulletArmor));
		
		//disable friendly fire, but only when shooting human players
		if(toy->boyId != -1){
			Toy_t* attacker = GetToy(dmgInfo->initiatorToyId);
			Boy_t* boy = GetBoy(toy->boyId);
			if( (!boy->isRobot) && (attacker->boyId != toy->boyId) ){
				if(IsToyFriendly(shapeData->toyId, dmgInfo->initiatorToyId)) return;
			}
		}

		if(dmgInfo->knockback != 0.0f){
			float force = MUL_FORCE * 25 * dmgInfo->knockback;
			cpBodyApplyImpulseAtWorldPoint(toy->limbs[0].body, cpvmult(normal, -force), cpvzero);
		}
		TakeDamage(shapeData->toyId, dmg);
		ApplyEffect((EffectParams_t){
			.type = EFFECT_DAMAGE_TOY,
			.amount = 6 + (dmg / 4),
			.pos = (Vector3){point.x, point.y - toyClass->height/2, 0},
			.velocity = (Vector3){normal.x, normal.y, 0}
		}, -1);
	}
}

bool UseItem(int itemId, Vector3 direction){
	Item_t* item = GetItem(itemId);
	ItemClass_t* itemClass = GetItemClass(item->type);
	Toy_t* toy = GetToy(item->heldByToyId);
	if(item->heldByToyId == -1) return false; //maybe later
	if(item->cooldown) return false;

	if(itemClass->isGun){
		Vector3 pos;
		Vector3 gripPos = PeekPos(&toy->limbs[0].tform);
		gripPos.z += 300;
		pos = gripPos;
		pos.x += direction.x * 80;
		pos.y += direction.y * 80;
		if(!item->ammo) return false;
		item->ammo--;

		for(int i=0; i < itemClass->gun.bulletsPerShot; i++){
			cpShapeFilter filter = {CP_NO_GROUP, CP_ALL_CATEGORIES, CP_ALL_CATEGORIES};
			BulletDamageInfo_t dmgInfo = {
				.dmg = itemClass->gun.damage,
				.knockback = itemClass->gun.knockbackEnemy,
				.startPoint = toCpv(pos),
				.maxDist = itemClass->gun.distance,
				.initiatorToyId = item->heldByToyId
			};
			Vector2 dir2 = Vector2Rotate(Vec2(direction), itemClass->gun.spread * (randf() - 0.5));
			Vector3 endPoint = pos;
			endPoint.x += dir2.x * itemClass->gun.distance;
			endPoint.y += dir2.y * itemClass->gun.distance;

			//cpSpaceSegmentQuery(&game.space, toCpv(pos), toCpv(endPoint), 4, filter, (cpSpaceSegmentQueryFunc)BulletDamageQueryCallback, (void*)&dmgInfo);
			cpSegmentQueryInfo queryInfo;
			cpShape* shape = cpSpaceSegmentQueryFirst(&game.space, toCpv(pos), toCpv(endPoint), 4, filter, &queryInfo);
			if(shape){
				BulletDamageQueryCallback(shape, queryInfo.point, queryInfo.normal, queryInfo.alpha, (void*)&dmgInfo);
			}

			// ApplyEffect((EffectParams_t){
			// 	.type = EFFECT_DAMAGE_CRIT,
			// 	.velocity = direction,
			// 	.pos = endPoint,
			// 	.amount = 100
			// }, -1);
		}

		if(toy->limbs[0].body){
			float force = -itemClass->gun.knockbackSelf * 25 * MUL_FORCE;
			cpBodyApplyImpulseAtWorldPoint(toy->limbs[0].body, cpvmult(toCpv(direction), force), cpvzero);
		}

		{
			Vector2 displace4[] = { {0, 1.5}, {1, 0}, {0, -1}, {-1, 0} };
			int dir4 = MatchDirection(Vec2(direction), 4);
			ApplyEffect((EffectParams_t){
				.type = EFFECT_GUNFIRE_SHOTGUN,
				.velocity = direction,
				.pos = Vector3Add(gripPos, (Vector3){85 * displace4[dir4].x, 85 * displace4[dir4].y}),
				.amount = 1
			}, -1);
		}

		ApplyEffect((EffectParams_t){
			.type = EFFECT_PELLET_SHOTGUN,
			.velocity = direction,
			.pos = gripPos,
			.amount = 2
		}, -1);

		ApplyEffect((EffectParams_t){
			.type = EFFECT_DAMAGE_LOCAL,
			.amount = 10,
			.pos = pos
		}, item->heldByToyId);
	}
	item->cooldown = itemClass->cooldown;

	return true;
}

void UpdateItems(){

	forEachItem(item, itemId){
		ItemClass_t* itemClass = GetItemClass(item->type);
		Vector3 pos = Vector3Zero();
		if(item->body){
			float terrainHeight;
			UpdateTransform(&item->tform, item->body);
			pos = PeekPos(&item->tform);

			terrainHeight = TerrainHeightAt(Vec2(pos));
			pos.z -= 14.0;
			if(pos.z < terrainHeight){
				pos.z = terrainHeight;
				if(itemClass->isGun && (!item->ammo)){
					RemoveItem(itemId);
				}else{
					DematerializeItem(item, false, true);
				}
			}else{
				PinTransformZ(&item->tform, pos.z);
			}

		}else if(item->heldByToyId != -1){
			Toy_t* toy = GetToy(item->heldByToyId);
			ToyClass_t* toyClass = GetToyClass(toy->type);
			pos = PeekPos(&toy->limbs[0].tform);

			pos.z += toyClass->height;
			UpdateTransformPos(&item->tform, pos);
		}

		if((item->heldByToyId != -1) || item->body){
			item->sector[0] = (int)(pos.x / SECTOR_SIZE);
			item->sector[1] = (int)(pos.y / SECTOR_SIZE);
		}

		if(item->cooldown) item->cooldown--;
	}
}

void DrawItems(){

	forEachItem(item, itemId){
		ItemClass_t* itemClass = GetItemClass(item->type);
		if(!APPROX_NEAR(item->sector, client.sector, client.sectorsInView)) continue;

		if(item->heldByToyId != -1){
			Toy_t* toy = GetToy(item->heldByToyId);
			const Vector3 gripCenter[] = { {5, -75, 1}, {25, -90, 1}, {-5, -80, -1}, {-25, -90, 1} };
			float shake[] = {
				cosf(1.0  * 3.14 * toy->viz.runAccum),
				sinf(0.33 * 3.14 * toy->viz.runAccum)
			};
			int dir4 = toy->animation.direction8 >> 1;
			Vector3 pos3d = GetPos(&toy->limbs[0].tform);
			Vector3 pos = Project3d(pos3d);
			int spriteId = toy->animation.aiming ? itemClass->sprite.aimed : itemClass->sprite.hold;
			int frame = (dir4 > 1) ? 0 : 1;

			if(!CanSeeObject(client.playerBoyId, pos3d)) continue;

			pos = Vector3Add(pos, gripCenter[dir4]);
			pos.y += 2 * (shake[0]*shake[0]);
			pos.y += 6 * shake[1];
			DrawSprite(spriteId, frame, pos, WHITE, 1);
		}else{
			Vector3 pos = GetPos(&item->tform);
			float angle = GetAngleDeg(&item->tform);
			if(!CanSeeObject(client.playerBoyId, pos)) continue;

			DrawSprite(itemClass->sprite.aimed, 0, Project3d(pos), WHITE, 1);
			pos.z = TerrainHeightAt(Vec2(pos));
			BlitSprite(client.assets.sprite.shadowSmall, 0, Project2d(pos), WHITE, 1);
		}
	}
}
