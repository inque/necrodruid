#include "gameapp.h"
#include "effects.h"
#include "utils.h"
#include "a_toy_private.h"
#include "geometry.h"
#include "fogofwar.h"
#include "networking.h"
#include "string.h"
#include "stdio.h"

int SpawnToy(ToyType_e type, bool automated, int boyId, Position_t pos){
	ToyClass_t* toyClass = GetToyClass(type);

	for(int toyId=0; toyId < MAX_TOYS; toyId++){
		Toy_t* toy = GetToy(toyId);
		if(toy->active) continue;

		memset(toy, 0, sizeof(Toy_t));
		toy->active = true;
		toy->boyId = boyId;
		toy->type = type;
		toy->health = toyClass->maxHealth;
		toy->maxHealth = toyClass->maxHealth;
		toy->despawnTimer = 2*TICKS_PER_SEC;
		toy->walk.direction = (Vector2){0, 1};
		toy->walk.dampedDir = (Vector2){0, 1};
		toy->item.holdingItemId = -1;
		toy->item.lookingAtItemId = -1;
		toy->automation.enabled = automated;
		toy->automation.action.type = ACTION_IDLE;
		toy->automation.action.idle.timer = (toyId & 31);
		if(toyClass->immortal){
			//toy->immortal = true;
			toy->fatalizer = true;
		}

		toy->materializeTimer = toyClass->materializeDur * MUL_FRAMES;
		toy->materializeAt = pos;
		PinTransform(&toy->limbs[0].tform, pos, 0, (Vector3){100, 100, 200});
		toy->limbs[0].lastPos = Vec2(pos);
		toy->sector[0] = (int)(pos.x / SECTOR_SIZE);
		toy->sector[1] = (int)(pos.y / SECTOR_SIZE);

		toy->animation.state = ANIM_SPAWN;
		toy->animation.duration = toy->materializeTimer;
		toy->animation.lastChangeFrame = game.frame;

		SyncToy(toyId);

		return toyId;
	}
	return -1;
}

void RemoveToy(int id){
	Toy_t* toy = GetToy(id);
	toy->active = false;
	Dematerialize(toy, true);
	if(toy->item.holdingItemId != -1){
		SetItemOwner(toy->item.holdingItemId, -1);
	}
	SyncToy(id);
}

void SyncToy(int toyId){
	Toy_t* toy = GetToy(toyId);
	ResponseMessage_t* msg = GetResponseBuffer();
	if(app.role != ROLE_HOST) return;

	msg->code = RESPONSE_TOY;
	msg->toy.toyId = EncCharId(toyId);
	int length;
	if(toy->active){
		msg->toy.active = 1;
		msg->toy.boyId = EncCharId(toy->boyId);
		msg->toy.type = toy->type;
		msg->toy.health = MIN(255, toy->health);
		PackTform(&msg->toy.tform, &toy->limbs[0].tform);
		length = 1+sizeof(struct ResponseToy_st);
	}else{
		msg->toy.active = 0;
		length = 3;
	}
	BroadcastResponse(CHAN_STATUS, length);
}

cpBool CollisionPresolveToy(cpArbiter* arb, cpSpace* space, void *data){
	CP_ARBITER_GET_SHAPES(arb, b, a);
	ShapeData_t* ourData = (ShapeData_t*)cpShapeGetUserData(b);
	ShapeData_t* otherData = (ShapeData_t*)cpShapeGetUserData(a);

	if((otherData != NULL) && (ourData != NULL) && (ourData->type == SHAPE_TOY)){
		Toy_t* toy = GetToy(ourData->toyId);
		ToyClass_t* toyClass = GetToyClass(toy->type);
		Limb_t* limb = &toy->limbs[ourData->limbId];
		switch(otherData->type){
			case SHAPE_ITEM_SENSOR: {
				Item_t* item = GetItem(otherData->itemId);
				if(!toyClass->canPickUpItems) return;
				if((toy->item.holdingItemId == -1) && (item->heldByToyId == -1)){
					toy->item.lookingAtItemId = otherData->itemId;
					toy->item.lookingAtItemTimer = 2;
				}
			} break;
			default:
				break;
		}
	}
	return cpFalse;
}

void CollisionPostsolveToy(cpArbiter* arb, cpSpace* space, void *data){
	CP_ARBITER_GET_SHAPES(arb, a, b);
	ShapeData_t* ourData = (ShapeData_t*)cpShapeGetUserData(a);
	ShapeData_t* otherData = (ShapeData_t*)cpShapeGetUserData(b);
	if(otherData && ourData){
		Toy_t* toy = GetToy(ourData->toyId);
		//Limb_t* limb = &toy->limbs[ourData->limbId];
		ToyClass_t* toyClass = GetToyClass(toy->type);

		switch(otherData->type){
			case SHAPE_TOY: {
				Toy_t* otherToy = GetToy(otherData->toyId);
				cpVect point = cpArbiterGetPointA(arb, 0);
				cpVect normal = cpArbiterGetNormal(arb);
				
				if(toy->dash.timer && (!toy->walk.fallenTimer)){
					if(IsToyFriendly(ourData->toyId, otherData->toyId)) return; //no friendly fire
					bool dead = TakeDamage(otherData->toyId, toyClass->dash.damage);
					if(dead){
						//restore previous velocity
						cpBodySetVelocity(toy->limbs[0].body, toCpv(toy->limbs[0].lastVelocity));
					}else{
						float force = MUL_FORCE * 25 * toyClass->dash.knockback;
						cpBodyApplyImpulseAtWorldPoint(otherToy->limbs[0].body, cpvmult(normal, force), point);
					}
				}
			} break;
			default:
				break;
		}
	}
}

static void ProcessToyActions(int toyId){
	Toy_t* toy = GetToy(toyId);
	ToyClass_t* toyClass = GetToyClass(toy->type);
	MeleeAttackType_t* attack;
	cpBody* body = toy->limbs[0].body;
	Vector3 pos = PeekPos(&toy->limbs[0].tform);
	Vector2 direction = {toy->inputs.axes[0], toy->inputs.axes[1]};
	float magnitude = Vector2Length(direction);
	float speed = toyClass->walkSpeed;
	bool attackAllowed = (toy->dash.timer < (6 * MUL_FRAMES)) && (toy->item.holdingItemId == -1);

	//-- walk --
	if(magnitude > 0){
		toy->walk.direction = direction;
		toy->walk.dampedDir = Vector2Lerp(toy->walk.dampedDir, direction, 0.25);
	}
	if(toy->melee.preparation){
		toy->walk.dampedDir = Vector2Lerp(toy->walk.dampedDir, toy->melee.direction, 0.1);
	}

	if(Vector3Distance(pos, toy->loco.point[toy->loco.phase]) > toyClass->radius*1.1){
		//if(Vector3Distance(pos, toy->loco.point[1-toy->loco.phase]) > toyClass->radius*0.75){
			Vector3 locoDir = Vector3Normalize(Vector3Subtract(toy->loco.point[toy->loco.phase], pos));
			// float angle = 30*(toy->loco.phase?-1:1) + (90 - Vector2Angle(Vector2Zero(), direction));
			// Vector2 correction = Vector2Rotate(Vec2(locoDir), angle);
			// locoDir.x = correction.x;
			// locoDir.y = correction.y;
			Vector3 newLocoPos = Vector3Subtract(pos, (Vector3){locoDir.x*toyClass->radius*0.9, locoDir.y*toyClass->radius*0.9, 0});
			if(Vector3Distance(newLocoPos, toy->loco.point[1-toy->loco.phase]) > toyClass->radius*0.75){
				toy->loco.point[toy->loco.phase] = newLocoPos;
				toy->loco.phase = 1-toy->loco.phase;
				if(toy->loco.lastChangeFrame){
					toy->loco.lastChangeDelta = (game.frame - toy->loco.lastChangeFrame);
				}
				toy->loco.lastChangeFrame = game.frame;
			}
		//}
	}

	//-- actions --
	switch(toy->inputs.action.type){
		case BOY_ACTION_USECARD:
			if(game.state != STATE_PLAYING) break;
			if(!toyClass->immortal) break;
			if(toy->useCard.timer || toy->useCard.cooldown) break;
			if(toy->melee.preparation) break;
			if(toy->item.holdingItemId != -1) break;
			//if(toy->dash.timer) break;
			if(toy->boyId != -1){
				Boy_t* boy = GetBoy(toy->boyId);
				//find card slot by num
				int cardSlot = -1, cardNum = 0;
				for(int i=0; i < BOY_MAX_CARDS; i++){
					if(!boy->cards[i].active) continue;
					if(cardNum++ == toy->inputs.action.useCard.num){
						cardSlot = i;
						break;
					}
				}
				if(cardSlot == -1) break;

				//success
				toy->useCard.timer = TICKS_PER_SEC;
				toy->useCard.cardSlot = cardSlot;
			}
			break;
	}
	toy->inputs.action.type = BOY_ACTION_NONE;

	//-- use card --
	if(toy->useCard.timer){
		if(toy->melee.preparation || toy->dash.timer){
			toy->useCard.timer = 0;
		}else{
			if(!--toy->useCard.timer){
				if(toy->boyId != -1){
					BoyUseCard(toy->boyId, toy->useCard.cardSlot);
				}
				toy->useCard.cooldown = USECARD_COOLDOWN;
			}
		}
	}

	//-- use item --
	if(!toy->walk.fallenTimer){
		if(toy->item.holdingItemId != -1){
			Item_t* item = GetItem(toy->item.holdingItemId);
			ItemClass_t* itemClass = GetItemClass(item->type);

			if(toy->item.preparation){
				if(!--toy->item.preparation){
					Vector3 lookDir = (Vector3){toy->inputs.axes[2], toy->inputs.axes[3], 0};
					bool success = UseItem(toy->item.holdingItemId, lookDir);
					if(success) toy->item.lastUsedFrame = game.frame;
					toy->walk.dampedDir = Vec2(lookDir);
				}
			}else{
				bool lmb = ToyButtonPressed(toy, BTN_FIRE);
				bool rmb = ToyButtonPressed(toy, BTN_ALTFIRE);
				if(rmb || (lmb && itemClass->isGun && (!item->ammo))){
					SetItemOwner(toy->item.holdingItemId, -1);
				}else if(lmb){
					toy->item.preparation = itemClass->preparation;
				}
			}
		}else{
			if(toy->item.lookingAtItemId != -1){
				if(ToyButtonPressed(toy, BTN_USE)){
					SetItemOwner(toy->item.lookingAtItemId, toyId);
					return;
				}
			}
		}
	}

	//-- melee attack --
	if(toy->melee.preparation){
		attack = &toyClass->melee[toy->melee.variant];
		toy->melee.cooldown = attack->cooldown * MUL_FRAMES;
		speed *= 0.5;
		if(!--toy->melee.preparation){
			float bonus = 1;
			toy->melee.crit = false;
			if(attack->seriesLength){
				if(++toy->melee.series >= attack->seriesLength){
					toy->melee.series = 0;
					toy->melee.cooldown += attack->seriesInterval * MUL_FRAMES;
					toy->melee.crit = true;
					bonus = attack->seriesCrit;
					if(toyClass->immortal){
						ApplyEffect((EffectParams_t){
							.type = EFFECT_DAMAGE_CRIT,
							.amount = 10,
							.pos = (Vector3){pos.x, pos.y, pos.z+100}
						}, -1);
					}
				}else{
					toy->melee.seriesCooldown = attack->seriesInterval * MUL_FRAMES;
				}
			}
			PerformMeleeAttack(toyId, attack, bonus);
			toy->melee.lastPerformedFrame = game.frame;
			if(toy->melee.variant == 0){
				ApplyEffect((EffectParams_t){
					.type = EFFECT_PUNCH_WHOOSH,
					.pos = pos,
					.amount = (bonus != 1) ? 2 : 1
				}, -1);
			}
		}
		if(attack->dash != 0.0f){
			float t = MAX(0, 1 - toy->melee.preparation / (10.0 * MUL_FRAMES));
			//float c = fabsf(t - 0.5f);
			float c = t*t*t;
			float f = c * attack->dash * 5 * MUL_FORCE;
			direction = Vector2Add(direction, Vector2Scale(toy->melee.direction, f));
		}
	}else if(toy->melee.cooldown){
		toy->melee.cooldown--;
	}else{
		int variant = -1;
		if(toy->melee.seriesCooldown){
			if(!--toy->melee.seriesCooldown){
				toy->melee.series = 0;
			}
		}
		if(ToyButtonPressed(toy, BTN_FIRE)) variant = 0;
		if(ToyButtonPressed(toy, BTN_ALTFIRE)) variant = 1;
		if(variant != -1){
			attack = &toyClass->melee[variant];
			if(attack->damage && attackAllowed){
				if(toy->melee.series && (toy->melee.variant != variant)){
					toy->melee.series = 0;
				}
				toy->melee.variant = variant;
				toy->melee.preparation = attack->preparation * MUL_FRAMES;
				
				//toy->melee.direction = Vector2Normalize(toy->walk.dampedDir);
				toy->melee.direction = (Vector2){toy->inputs.axes[2], toy->inputs.axes[3]};
			}
		}
	}
	
	//-- dash mechanic --
	if(toyClass->dash.duration){
		if(toy->dash.timer){
			toy->dash.timer--;
			toy->dash.cooldown = toyClass->dash.cooldown * MUL_FRAMES;
			direction = toy->dash.direction;
			speed = toyClass->dash.speed;
		}else if(toy->dash.cooldown){
			toy->dash.cooldown--;
		}else{
			if(ToyButtonPressed(toy, BTN_JUMP)){
				Vector2 dashDir = Vector2Normalize(toy->walk.dampedDir);
				if(Vector2LengthSqr(dashDir) > 0){
					toy->dash.direction = dashDir;
					toy->dash.timer = toyClass->dash.duration * MUL_FRAMES;

					if(toy->melee.preparation > TICKS_PER_SEC/8){ //fixme: balance?
						toy->melee.preparation = 0;
					}
				}
			}
		}
	}

	//-- animation --
	{
		ToyAnimState_e newState = ANIM_IDLE;
		int newDirection8 = MatchDirection(toy->walk.dampedDir, 8);
		int duration = 0;
		MeleeAttackType_t* attack = &toyClass->melee[toy->melee.variant];
		int meleeCooldownDur = 6;
		int variant = 0;
		bool flash = false;
		bool aiming = false;
		bool stateChanged, directionChanged, durationChanged, itemStateChanged;
		if(toy->item.holdingItemId != -1){
			int elapsedSinceUsed = game.frame - toy->item.lastUsedFrame;
			aiming = toy->item.preparation || (elapsedSinceUsed < TICKS_PER_SEC/3);
		}
		if(toy->melee.preparation){
			newState = ANIM_ATTACK;
			duration = toy->melee.preparation;
			variant = toy->melee.variant;
			flash = false;
		}else if(toy->melee.cooldown && ((game.frame - toy->melee.lastPerformedFrame) < meleeCooldownDur)){
			newState = ANIM_ATTACK_REL;
			duration = meleeCooldownDur;
			variant = toy->melee.variant;
			flash = !!toy->melee.crit;
		}else if(toy->useCard.timer){
			newState = ANIM_USECARD;
			duration = toy->useCard.timer;
		}else if(toy->dash.timer){
			newState = ANIM_DASH;
			duration = toy->dash.timer;
		}else if( magnitude > 0.1 ){
			newState = ANIM_RUN;
			duration = toy->loco.lastChangeDelta;
		}else{
			newState = ANIM_IDLE;
		}
		stateChanged     = (toy->animation.state      != newState);
		directionChanged = (toy->animation.direction8 != newDirection8);
		durationChanged  = (toy->animation.duration   != duration) && (newState == ANIM_RUN); //!
		itemStateChanged = (toy->animation.aiming     != aiming);
		if(itemStateChanged){
			toy->animation.aiming = aiming;
		}
		if(stateChanged){
			toy->animation.state = newState;
			toy->animation.direction8 = newDirection8;
			toy->animation.lastChangeFrame = game.frame;
			toy->animation.duration = duration;
			toy->animation.variant = variant;
			toy->animation.flash = flash;
		}else if(directionChanged){
			toy->animation.direction8 = newDirection8;
		}else if(durationChanged){
			toy->animation.duration = duration;
		}
	}

	cpBodyApplyForceAtWorldPoint(body, cpvmult(toCpv(direction), speed * MUL_FORCE), cpBodyGetPosition(body));
}

void UpdateToys(){

	forEachToy(toy, toyId){
		ToyClass_t* toyClass = GetToyClass(toy->type);
		bool canWalk = (toy->health > 0) && (!toy->walk.fallenTimer) && (!toy->fatality.timer);

		forEachLimb(toy, limb, limbId){
			if(limb->body){
				cpVect position = cpBodyGetPosition(limb->body);
				cpVect velocity = cpBodyGetVelocity(limb->body);
				UpdateTransform(&limb->tform, limb->body);
				limb->lastPos = Vec2(position);
				limb->lastVelocity = Vec2(velocity);
				if(limbId == 0){
					toy->sector[0] = (int)(position.x / SECTOR_SIZE);
					toy->sector[1] = (int)(position.y / SECTOR_SIZE);
				}
			}
			//-- gravity --
			{
				Vector3 pos = PeekPos(&limb->tform);
				pos.z = TerrainHeightAt(Vec2(pos));
				UpdateTransformPos(&limb->tform, pos);
			}
		}

		if(toy->item.holdingItemId != -1){
			if(!canWalk){
				SetItemOwner(toy->item.holdingItemId, -1);
			}
		}

		if(toy->health <= 0){
			if(toy->animation.state != ANIM_DEATH){
				toy->animation.state = ANIM_DEATH;
				toy->animation.lastChangeFrame = game.frame;
			}
			if(!toy->despawnTimer--){
				RemoveToy(toyId);
			}else{
				if(toy->dematerializeTimer){
					if(!--toy->dematerializeTimer){
						Dematerialize(toy, false);
					}
				}
			}
			continue;
		}

		if(toy->automation.enabled){
			UpdateAutomation(toyId);
		}

		if(toyClass->healthRegenAmount != 0){
			if(toy->regenerateTimer){
				toy->regenerateTimer--;
			}else{
				toy->health = MAX(0, MIN(toy->health + toyClass->healthRegenAmount, toy->maxHealth));
				toy->regenerateTimer = toyClass->healthRegenDelay * MUL_FRAMES;
			}
		}

		if(toy->useCard.cooldown){
			toy->useCard.cooldown--;
		}

		if(toy->walk.fallenTimer){
			toy->walk.fallenTimer--;
			if(toy->animation.state != ANIM_FALL){
				toy->animation.state = ANIM_FALL;
				toy->animation.lastChangeFrame = game.frame;
			}
		}

		if(ToyUpdateFatality(toyId)){
			canWalk = false;
		}

		if(toy->limbs[0].body){
			if(canWalk) ProcessToyActions(toyId);
		}else{
			if(toy->materializeTimer){
				toy->materializeTimer--;
				if(toy->animation.state != ANIM_SPAWN){
					toy->animation.state = ANIM_SPAWN;
					toy->animation.duration = toy->materializeTimer;
					toy->animation.lastChangeFrame = game.frame;
				}
			}else{
				Materialize(toyId, toy->materializeAt);
			}
		}

		if(toy->item.lookingAtItemTimer){
			if(!--toy->item.lookingAtItemTimer){
				toy->item.lookingAtItemId = -1;
			}
		}
	}
}

static int SpriteAnimLen(int spriteId){
	return client.sprites[spriteId].framesUsed;
}

void DrawToys(){
	Limb_t* limb;
	Vector3 pos, posWorld;
	//float angle;
	// Color color, color2;
	// float radius;

	forEachToy(toy, toyId){
		Boy_t* boy = (toy->boyId == -1) ? NULL : GetBoy(toyId);
		ToyClass_t* toyClass = GetToyClass(toy->type);
		if(!APPROX_NEAR(client.sector, toy->sector, client.sectorsInView)) continue;
		if(!IsVisible(&toy->limbs[0].tform)) continue;
		if(!CanSeeToy(client.playerBoyId, toyId)) continue;

		switch(toy->type){
			default: {
				limb = &toy->limbs[0];
				posWorld = GetPos(&limb->tform);
				pos = Project3d(posWorld);
				//angle = GetAngleDeg(&limb->tform);

				bool hidden = false;
				int spriteId = 0, frameId = 0;
				int direction4 = toy->animation.direction8 >> 1;
				int elapsed = game.frame - toy->animation.lastChangeFrame;
				float t = (float)elapsed / MAX(0.1, toy->animation.duration);
				int animLen;
				switch(toy->animation.state){
					case ANIM_IDLE:
						spriteId = toyClass->sprite.idle;
						frameId = direction4;
						break;
					case ANIM_RUN: {
						float speed = 0.1 * (1.0 / MAX(0.1, toy->animation.duration));
						float delta = MIN(0.1, speed);
						toy->viz.runAccum += delta * 100 * SmoothOver(0.9, 0.5, client.deltaTime);
						spriteId = toyClass->sprite.run[direction4];
						//frameId = (elapsed>>2)%3;
						frameId = (int)(toy->viz.runAccum) % SpriteAnimLen(spriteId);
					} break;
					case ANIM_DASH:
						//spriteId = toyClass->sprite.dash[direction4];
						spriteId = toyClass->sprite.dash[0];
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((int)(t * animLen), animLen-1);
						break;
					case ANIM_SPAWN:
						spriteId = toyClass->sprite.spawn;
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((int)(t * animLen), animLen-1);
						//printf("%i. %i / %i\n", toyId, elapsed, toy->animation.duration);
						break;
					case ANIM_USECARD:
						spriteId = toyClass->sprite.usecard;
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((int)(t * animLen), animLen-1);
						break;
					case ANIM_ATTACK:
						//spriteId = toyClass->sprite.attack[direction4];
						spriteId = toyClass->sprite.attack[toy->animation.variant][0];
						//frameId = MIN(3, (elapsed>>1));
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((int)(t * animLen), animLen-1);
						break;
					case ANIM_ATTACK_REL:
						//spriteId = toyClass->sprite.attackRel[toy->animation.variant][direction4];
						spriteId = toyClass->sprite.attackRel[toy->animation.variant][0];
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((int)(t * animLen), animLen-1);
						if(toy->animation.flash){
							hidden = (int)(client.time*8)&1;
							// for(int i=0; i < 12; i++){
							// 	DrawRectangle(pos.x+GetRandomValue(-60, 60), pos.y+GetRandomValue(-220, 30), 10, 10, Fade(WHITE, GetRandomValue(10,40)/100.0 ));
							// }
							// client.camShakePendulum[3].x += 1.1;
							// client.camShakePendulum[6].y -= 2.2;
						}
						break;
					case ANIM_DEATH:
						spriteId = toyClass->sprite.death;
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((elapsed>>3), animLen-1);
						break;
					case ANIM_FALL:
						spriteId = toyClass->sprite.fall;
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((elapsed>>3), animLen-1);
						break;
					case ANIM_FATALITY:
						spriteId = toyClass->sprite.fatality;
						animLen = SpriteAnimLen(spriteId);
						frameId = MIN((elapsed>>3), animLen-1);
						break;
				}
				
				if(!hidden){
					float shadowSize = (toy->item.holdingItemId == -1) ? 0.75 : 0.82;
					float ground = TerrainHeightAt(Vec2(posWorld));
					Vector3 shadowProj = Project3d((Vector3){posWorld.x, posWorld.y, ground});
					DrawSprite(spriteId, frameId, pos, WHITE, 1);
					BlitSprite(client.assets.sprite.shadowMedium, 0, Vec2(shadowProj), WHITE, shadowSize);
				}



				// color = boy ? boy->appearance.color : GREEN;
				// color2 = BLACK;
				// radius = limb->tform.size.x;
				
				// DrawEllipse(pos.x, pos.y, radius, radius*app.config.persp_y, color2);
				// DrawEllipse(pos.x, pos.y, radius-6, (radius-6)*app.config.persp_y, color);
				
				// DrawLineV(Vec2(pos), Vector2Add(Vec2(pos), Vector2Scale(toy->walk.dampedDir, radius)), color2);

				// if(toy->melee.preparation){
				// 	DrawRectangle(pos.x, pos.y, 10,10, RED);
				// }else if(toy->melee.cooldown){
				// 	DrawRectangle(pos.x, pos.y, 10,10, GREEN);
				// }
			} break;
		}

	}
	
}
