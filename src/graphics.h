#ifndef GRAPHICS_H
#include "transforms.h"
#include "raylib.h"
#include "raymath.h"

typedef struct{
	int vertCount;
	Vector2* verts;
	int triangleCount;
	int* indices;
	Color fill;
} FigureDesc_t;

void DrawFigure(FigureDesc_t figure, Vector2 pos, float z);
void BeginTransformEx(Vector2 pos, float angle, float scaleX, float scaleY);
void BeginTransform2d(Transform_t* tform);
void EndTransform();

#define GRAPHICS_H
#endif
#ifdef IMP_GRAPHICS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "raylib.h"
#include "rlgl.h"

void DrawFigure(FigureDesc_t figure, Vector2 pos, float z){
	int indicesUsed = figure.triangleCount * 3;
	if(figure.fill.a){
		rlCheckRenderBatchLimit((figure.triangleCount-1)*4);
		//rlEnableDepthTest();
		//rlSetTexture(figure.texId);
	rlBegin(RL_QUADS);
		rlNormal3f(0, 0, 1);
		rlColor4ub(figure.fill.r, figure.fill.g, figure.fill.b, figure.fill.a);
			int blarp = 0;
			for(int i=indicesUsed-1; i >= 0; i--){
				Vector2 vert = figure.verts[figure.indices[i]];
				//rlTexCoord2f((vert.x+shape->uv.x) / sheet->tex.width, (vert.y+shape->uv.y) / sheet->tex.height);
				rlVertex3f(vert.x + pos.x, vert.y + pos.y, z);
				if((blarp++%3)==2) i++;
			}
		rlEnd();
		//rlDisableDepthTest();
	}
	// if(figure.stroke.a){
	// }
	BeginTransformEx(pos, 0, 1, 1);
	for(int i=0; i < figure.vertCount; i++){
		DrawLineEx(figure.verts[i], figure.verts[(i+1)%figure.vertCount], 2, BLACK);
		
	}
	EndTransform();
}

void BeginTransformEx(Vector2 pos, float angle, float scaleX, float scaleY) {
	rlPushMatrix();
	rlTranslatef(pos.x, pos.y, 0.0f);
	if(angle != 0.0f){
		rlRotatef(angle * RAD2DEG, 0.0f, 0.0f, 1.0f);
	}
	if((scaleX != 1.0f) || (scaleY != 1.0f)){
		rlScalef(scaleX, scaleY, 1.0f);
	}
}

void BeginTransform2d(Transform_t* tform){
	Vector2 pos = GetPos2d(tform);
	float angle = GetAngle(tform);
	BeginTransformEx(pos, angle, 1, 1);
}

void EndTransform(){
	rlPopMatrix();
}


#undef IMP_GRAPHICS_H
#endif
