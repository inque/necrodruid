#ifndef A_TOY_CLASS_H
#include "stdbool.h"

typedef enum{
	TOY_DRUID,
	TOY_SKELLY,
	TOY_TIGRA,
	_TOY_TYPE_COUNT
} ToyType_e;

typedef struct{
	int preparation;
	int cooldown;
	float rangeDist;
	float rangeRadius;
	int damage;
	float enemyKnockback;
	float selfKnockback;
	float dash;
	int seriesLength;
	int seriesInterval;
	float seriesCrit;
} MeleeAttackType_t;

typedef struct{
	char name[32];
	int maxHealth;
	int healthRegenAmount;
	int healthRegenDelay;
	int materializeDur;
	float bulletArmor;
	bool canPickUpItems;
	bool immortal;
	bool avian;
	bool female; // I'm sorry
	float mass;
	float radius;
	float height;
	float walkSpeed;
	struct{
		int duration;
		int cooldown;
		float speed;
		int damage;
		float knockback;
	} dash;
	MeleeAttackType_t melee[2];
	struct{
		int cost;
	} ai;
	struct{
		int idle;
		int run[4];
		int dash[4];
		int attack[2][4];
		int attackRel[2][4];
		int fall;
		int death;
		int avatar;
		int icon;
		int fatality;
		int spawn;
		int usecard;
		#define TOYCLASS_SPRITES_PER_SHEET 32
	} sprite;
} ToyClass_t;

extern ToyClass_t toyClasses[_TOY_TYPE_COUNT];

#define A_TOY_CLASS_H
#endif
#ifdef IMP_A_TOY_CLASS_H
//==== implementation starts here ====

ToyClass_t toyClasses[] = {
	[TOY_DRUID] = {
		.name = "Druid",
		.maxHealth = 100,
		.healthRegenAmount = 1,
		.healthRegenDelay = 30,
		.materializeDur = 0,
		.bulletArmor = 0.83,
		.canPickUpItems = true,
		.immortal = true,
		.mass = 10,
		.radius = 30,
		.height = 100,
		.walkSpeed = 60,
		.dash = {
			.duration = 12,
			.cooldown = 12,
			.speed = 420,
			.damage = 5,
			.knockback = 5
		},
		.melee = {
			{   .preparation = 6, //sword
				.cooldown = 8,
				.rangeDist = 75,
				.rangeRadius = 100,
				.damage = 20,
				.enemyKnockback = 4,
				.selfKnockback = 1,
				.dash = 1,
				.seriesLength = 3,
				.seriesInterval = 16,
				.seriesCrit = 1.64
			},
			{   .preparation = 12, //kick
				.cooldown = 16,
				.rangeDist = 80,
				.rangeRadius = 80,
				.damage = 30,
				.enemyKnockback = 9,
				.selfKnockback = 40,
				.dash = 0.5
			}
		},
		.ai = { .cost = 200 }
	},
	[TOY_SKELLY] = {
		.name = "Skeleton",
		.maxHealth = 60,
		.healthRegenAmount = -2,
		.healthRegenDelay = 100,
		.bulletArmor = 0.175,
		.canPickUpItems = true,
		.materializeDur = 30,
		.mass = 5,
		.radius = 30,
		.height = 100,
		.walkSpeed = 40,
		.dash = {
			.duration = 12,
			.cooldown = 20,
			.speed = 240,
			.damage = 6,
			.knockback = 5
		},
		.melee = {
			{   .preparation = 10,
				.cooldown = 16,
				.rangeDist = 40,
				.rangeRadius = 55,
				.damage = 8,
				.seriesLength = 3,
				.seriesInterval = 24
			}
		},
		.ai = { .cost = 5 }
	},
	[TOY_TIGRA] = {
		.name = "Tiger",
		.maxHealth = 90,
		.healthRegenAmount = -1,
		.healthRegenDelay = 80,
		.materializeDur = 0,
		.mass = 25,
		.radius = 50,
		.height = 100,
		.walkSpeed = 84,
		.dash = {
			.duration = 18,
			.cooldown = 70,
			.speed = 450,
			.damage = 17,
			.knockback = 14
		},
		.melee = {
			{   .preparation = 20,
				.cooldown = 20,
				.rangeDist = 35,
				.rangeRadius = 45,
				.damage = 21
			}
		},
		.ai = { .cost = 5 }
	}
};

#undef IMP_A_TOY_CLASS_H
#endif
