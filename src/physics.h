#ifndef PHYSICS_H
#include "chipmunk/chipmunk.h"
#include "chipmunk/chipmunk_private.h"

typedef cpSpace      Space_t;
typedef cpBody       Body_t;
typedef cpShape      Shape_t;
typedef cpConstraint Joint_t;

#define toCpv(v) cpv(v.x, v.y)
#define Vec2(v) (Vector2){v.x, v.y}

enum{
	COLLISION_TYPE_TOY,
	COLLISION_TYPE_ITEM,
	COLLISION_TYPE_CAR,
	COLLISION_TYPE_LEVEL,
	COLLISION_TYPE_SENSOR,
};

typedef enum{
	SHAPE_TOY,
	SHAPE_ITEM,
	SHAPE_ITEM_SENSOR,
	SHAPE_CAR,
	SHAPE_LV_BLOCK
} ShapeOwnerType_e;

typedef struct{
	ShapeOwnerType_e type;
	union{
		int _id;
		int toyId, itemId, carId;
	};
	union{
		int _sub_id;
		int limbId;
	};
} ShapeData_t;

void InitializePhysics();
void UpdatePhysics();


#define PHYSICS_H
#endif
#ifdef IMP_PHYSICS_H
//==== implementation starts here ====

#include "gameapp.h"

void InitializePhysics(){
	cpCollisionHandler* handler;

	cpSpaceInit(&game.space);
	cpSpaceSetGravity(&game.space, cpvzero);
	cpSpaceSetDamping(&game.space, 1 - SPACE_DRAG);

	//toy -> any
	handler = cpSpaceAddWildcardHandler(&game.space, COLLISION_TYPE_TOY);
	handler->postSolveFunc = CollisionPostsolveToy;

	//toy -> sensor
	handler = cpSpaceAddCollisionHandler(&game.space, COLLISION_TYPE_TOY, COLLISION_TYPE_SENSOR);
	handler->preSolveFunc = CollisionPresolveToy;
}

void UpdatePhysics(){
	const int steps = 1;
	const float dt = 7.2 / TICKS_PER_SEC;
	for(int i=0; i < steps; i++){
		cpSpaceStep(&game.space, dt);
	}
}

#undef IMP_PHYSICS_H
#endif
