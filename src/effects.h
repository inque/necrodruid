#ifndef EFFECTS_H

typedef enum{
	EFFECT_DAMAGE_TOY,
	EFFECT_DAMAGE_LOCAL,
	EFFECT_DAMAGE_CRIT,
	EFFECT_GUNFIRE_SHOTGUN,
	EFFECT_PELLET_SHOTGUN,
	EFFECT_CARD_USE,
	EFFECT_FATALITY_START,
	EFFECT_FATALITY_KILL,
	EFFECT_VICTORY,
	EFFECT_PICKUP_GUN,
	EFFECT_PUNCH_WHOOSH
} ParticleEffect_e;

typedef struct{
	ParticleEffect_e type;
	int amount;
	Vector3 pos;
	Vector3 velocity;
} EffectParams_t;

void ApplyEffect(EffectParams_t effect, int receiverBoyId);

#define EFFECTS_H
#endif
#ifdef IMP_EFFECTS_H
//==== implementation starts here ====

void ApplyEffect(EffectParams_t effect, int receiverBoyId){

	if(app.headless) return;
	if((receiverBoyId != -1) && (receiverBoyId != client.playerBoyId)) return;

	switch(effect.type){
		case EFFECT_DAMAGE_TOY: {
			Vector3 minVel = Vector3Add(effect.velocity, (Vector3){-0.2, -0.6, 0});
			Vector3 maxVel = Vector3Add(effect.velocity, (Vector3){0.2, -0.15, 0});
			if(effect.amount > 40) effect.amount = 40;
			for(int i=0; i < effect.amount; i++){
				float life = 250+(40*(i&3));
				AddParticle(&client.fxWorld, PARTICLE_BLOOD, effect.pos, life, 20, minVel, maxVel);
			}
			MyPlaySound3d(client.assets.sounds.bodyImpact, effect.pos);
		} break;
		case EFFECT_DAMAGE_LOCAL: {
			float t = MIN(30, effect.amount);
			client.camShakePendulum[3] = (Vector2){t, -t/3};
			client.camShakePendulum[4] = (Vector2){-t, t/3};
		} break;
		case EFFECT_DAMAGE_CRIT: {
			Vector3 minVel = Vector3Add(effect.velocity, (Vector3){-2.6, -2.6, -2.2});
			Vector3 maxVel = Vector3Add(effect.velocity, (Vector3){2.6, 2.6, 4.6});
			for(int i=0; i < effect.amount; i++){
				float life = 110+(30*(i&7));
				AddParticle(&client.fxWorld, PARTICLE_SPARKY, effect.pos, life, 120, minVel, maxVel);
			}
		} break;
		case EFFECT_GUNFIRE_SHOTGUN: {
			Vector3 velocity = Vector3Scale(effect.velocity, 0.01);
			Vector3 smokeVel[] = { {-2, -2, 3}, {-2, -2, 8} };
			AddParticle(&client.fxWorldOrdered, PARTICLE_GUNFIRE, effect.pos, 100, 10, velocity, velocity);
			for(int i=0; i < 3; i++)
				AddParticle(&client.fxWorldOrdered, PARTICLE_GUNSMOKE, effect.pos, 200, 20, smokeVel[0], smokeVel[1]);
			MyPlaySound3d(client.assets.sounds.shotgunShot, effect.pos);
		} break;
		case EFFECT_PELLET_SHOTGUN: {
			Vector3 velocity = Vector3Scale(effect.velocity, -3);
			Vector3 vel2[] = {Vector3Add(velocity, (Vector3){-0.5, -0.5, 3}), Vector3Add(velocity, (Vector3){0.5, 0.5, 8})};
			for(int i=0; i < effect.amount; i++){
				AddParticle(&client.fxWorldOrdered, PARTICLE_PELLET_SHOTGUN, effect.pos, 400, 20, vel2[0], vel2[1]);
			}
		} break;
		case EFFECT_CARD_USE: {
			Vector3 pos = {client.halfWidth, client.height-100, 0};
			for(int i=0; i < effect.amount; i++){
				AddParticle(&client.fxScreen, PARTICLE_CARD, pos, 800, 18.0f, (Vector3){-2, -2.75, 0}, (Vector3){2, -0.8, 0});
			}
		} break;
		case EFFECT_FATALITY_START:
			MyPlaySound(client.assets.sounds.fatality, 0.75, 0);
			break;
		case EFFECT_FATALITY_KILL:
			client.camShakePendulum[1] = (Vector2){40, 20};
			client.camShakePendulum[2] = (Vector2){-20, -40};
			Vector3 pos = {effect.pos.x, effect.pos.y, effect.pos.z + 250};
			for(int i=0; i < effect.amount; i++){
				float life = 250+(40*(i&3));
				AddParticle(&client.fxWorld, PARTICLE_LIGHTNING, pos, life, 70, Vector3Zero(), Vector3Zero());
			}
			break;
		case EFFECT_VICTORY:
			MyPlaySound(client.assets.sounds.victory, 0.75, 0);
			break;
		case EFFECT_PICKUP_GUN:
			MyPlaySound(client.assets.sounds.shotgunCock, 0.7, 0);
			break;
		case EFFECT_PUNCH_WHOOSH:
			MyPlaySound3d((effect.amount == 1) ? client.assets.sounds.whoosh : client.assets.sounds.whoosh2, effect.pos);
			break;
	}
}

#undef IMP_EFFECTS_H
#endif
