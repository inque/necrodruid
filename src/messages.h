#ifndef MESSAGES_H

#define CHAT_MESSAGE_LEN 64
#define CHAT_HISTORY_MAX 64

typedef enum{
	MESSAGE_CHAT,
	MESSAGE_CUTSCENE
} MessageType_e;

typedef struct{
	int sentFrame;
	int boyId;
	char text[CHAT_MESSAGE_LEN];
} ChatMessage_t;

void Shout(MessageType_e type, int boyId, const char* text, int param1, int param2);
void DrawChatBox();

typedef struct{
	ChatMessage_t history[CHAT_HISTORY_MAX];
	int messageNext;
	int messagesTotal;
	int messagesShowing;
	int inputOpen;
	char inputText[CHAT_MESSAGE_LEN];
} ChatQueue_t;

#define MESSAGES_H
#endif
#ifdef IMP_MESSAGES_H
//==== implementation starts here ====
#include "gameapp.h"
#include "level_ops.h"
//#include "gameclient.h"
#include "networking.h"
#include "raygui.h"
#include "string.h"
#include "stdio.h"

static bool ProcessChatCommandLocal(const char* input){
	char text[1024];

	if(!strncmp(input, "/fps", 4)){
		int arg = atoi(input + 4);
		SetTargetFPS(arg);
		return true;
	}

	//if(client.screen == SCREEN_EDITOR){
		if(!strcmp(input, "/terrain")){
			LvGenRandomTerrain();
			return true;
		}
	//}

	if(!strncmp(input, "/cut ", 5)){
		Shout(MESSAGE_CUTSCENE, -1, input+5, 0, 0);
		return true;
	}


	//layers debug
	if(!strcmp(input, "/ll")){
		forEachLvLayer(layer, layerId){
			sprintf(text, "%i. %s: %i-%i (%i)", layerId, layer->name, layer->brushStartId, layer->brushStartId+layer->brushCount, layer->brushCount);
			Shout(MESSAGE_CHAT, -1, text, 0, 0);
		}
		Shout(MESSAGE_CHAT, -1, "-------", 0, 0);
		return true;
	}
	if(!strncmp(input, "/ls", 3)){
		char* arg1 = strtok(input+4, " ");
		char* arg2 = strtok(NULL, " ");
		if(arg1 && arg2){
			game.level.layers[atoi(arg1)].brushStartId = atoi(arg2);
		}
		return true;
	}
	if(!strncmp(input, "/lc", 3)){
		char* arg1 = strtok(input+4, " ");
		char* arg2 = strtok(NULL, " ");
		if(arg1 && arg2){
			game.level.layers[atoi(arg1)].brushCount = atoi(arg2);
		}
		return true;
	}

	if(!strcmp(input, "/vars")){
		for(int i=0; i < game.level.varCount; i++){
			LvVariable_t* var = &game.level.vars[i];
			printf("%s = %i.\n", var->name, var->val);
		}
		printf("---------\n");
	}


	return false;
}

static int FindBoyByName(const char* name){
	forEachBoy(boy, boyId){
		if(!strncmp(boy->name, name, sizeof(boy->name))){
			return boyId;
		}
	}
	return -1;
}

static bool ProcessChatCommandRemote(int boyId, const char* input){
	Boy_t* boy = GetBoy(boyId);
	if((boyId == -1) || boy->isOperator){
		if(!strcmp(input, "/r")){
			app.shouldRestartGame = true;
			return true;
		}
		
		if(!strncmp(input, "/set ", 5)){
			char* varName = strtok((input + 5), " ");
			char* varValue_s = strtok(NULL, " ");
			if(varName && varValue_s){
				LvSetVar(varName, atoi(varValue_s));
			}
			return true;
		}

		if(!strncmp(input, "/automate ", 10)){ //   /automate [name] [1/0]
			char* arg1 = strtok((input + 10), " ");
			char* arg2 = strtok(NULL, " ");
			if(arg1 && arg2){
				int boyId = FindBoyByName(arg1);
				if(boyId != -1){
					Boy_t* boy = GetBoy(boyId);
					if(boy->toyId != -1){
						Toy_t* toy = GetToy(boy->toyId);
						toy->automation.enabled = !!atoi(arg2);
					}
				}
			}
			return true;
		}

		if(!strncmp(input, "/despawn ", 9)){ //   /despawn [name]
			char* arg1 = strtok((input + 9), " ");
			if(arg1){
				int boyId = FindBoyByName(arg1);
				if(boyId != -1){
					Boy_t* boy = GetBoy(boyId);
					if(boy->toyId != -1){
						if(boy->heroId == boy->toyId) boy->heroId = -1;
						RemoveToy(boy->toyId);
						boy->toyId = -1;
					}
				}
			}
			return true;
		}

		if(!strncmp(input, "/spawn ", 7)){ //   /spawn [name] [toyType] [amount] [x] [y] [z]
			char* arg1 = strtok((input + 7), " ");
			char* arg2 = strtok(NULL, " ");
			char* arg3 = strtok(NULL, " ");
			char* arg4 = strtok(NULL, " ");
			char* arg5 = strtok(NULL, " ");
			char* arg6 = strtok(NULL, " ");
			if(arg1 && arg2 && arg3 && arg4 && arg5 && arg6){
				int boyId = FindBoyByName(arg1);
				int amount = MIN(8, atoi(arg3));
				for(int i=0; i < amount; i++){
					Vector3 pos = {atof(arg4), atof(arg5), atof(arg6)};
					SpawnToy(atoi(arg2), true, boyId, pos);
				}
			}
			return true;
		}

		if(!strncmp(input, "/item ", 6)){ //   /item [itemType] [x] [y] [z] [opt. ammo]
			char* arg1 = strtok((input + 6), " ");
			char* arg2 = strtok(NULL, " ");
			char* arg3 = strtok(NULL, " ");
			char* arg4 = strtok(NULL, " ");
			char* arg5 = strtok(NULL, " ");
			if(arg1 && arg2 && arg3 && arg4 && arg5){
				Vector3 pos = {atof(arg2), atof(arg3), atof(arg4)};
				int itemId = SpawnItem(atoi(arg1), -1, pos);
				if(arg5){
					Item_t* item = GetItem(itemId);
					item->ammo = atoi(arg5);
				}
			}
			return true;
		}

		if(!strncmp(input, "/give ", 6)){ //   /item [name] [itemType] [x] [y] [z]
			char* arg1 = strtok((input + 6), " ");
			char* arg2 = strtok(NULL, " ");
			char* arg3 = strtok(NULL, " ");
			char* arg4 = strtok(NULL, " ");
			char* arg5 = strtok(NULL, " ");
			if(arg1 && arg2 && arg3 && arg4 && arg5){
				int boyId = FindBoyByName(arg1);
				if(boyId != -1){
					Boy_t* boy = GetBoy(boyId);
					Vector3 pos = {atof(arg3), atof(arg4), atof(arg5)};
					int itemId = SpawnItem(atoi(arg2), -1, pos);
				}
			}
			return true;
		}

		if(!strncmp(input, "/givecard ", 10)){ //   /givecard [type] [opt. name]
			char* arg1 = strtok((input + 10), " ");
			char* arg2 = strtok(NULL, " ");
			if(arg2){
				int boyId = FindBoyByName(arg2);
				if(boyId != -1){
					GiveCard(boyId, atoi(arg1) );
				}
			}else{
				forEachBoy(boy, boyId){
					if(!boy->isRobot){
						GiveCard(boyId, atoi(arg1) );
					}
				}
			}
			return true;
		}

		if(!strncmp(input, "/aiwalk ", 8)){ //   /aiwalk [name] [x] [y] [z]
			char* arg1 = strtok((input + 8), " ");
			char* arg2 = strtok(NULL, " ");
			char* arg3 = strtok(NULL, " ");
			char* arg4 = strtok(NULL, " ");
			if(arg1 && arg2 && arg3 && arg4){
				int boyId = FindBoyByName(arg1);
				if(boyId != -1){
					Boy_t* boy = GetBoy(boyId);
					if(boy->toyId != -1){
						Toy_t* toy = GetToy(boy->toyId);
						Vector3 pos = {atof(arg2), atof(arg3), atof(arg4)};
						toy->automation.enabled = true;
						toy->automation.action.type = ACTION_WALK;
						toy->automation.action.walk.timer = 7*TICKS_PER_SEC;
						toy->automation.action.walk.target = pos;
						toy->automation.action.walk.attackEnemyScoreThreshold = 105;
						toy->automation.action.walk.away = false;
						toy->automation.action.walk.checkEnemiesNearbyTimer = 3*TICKS_PER_SEC;
						toy->automation.action.walk.radius = 100;
					}
				}
			}
			return true;
		}

		if(!strncmp(input, "/nextmap ", 9)){ //   /nextmap [name]
			strncpy(game.level.filePath, input+9, sizeof(game.level.filePath));
			printf("success. %s\n", game.level.filePath);
			return true;
		}


	}
	return false;
}

void Shout(MessageType_e type, int boyId, const char* text, int param1, int param2){
	ChatMessage_t* msg = &client.chat.history[client.chat.messageNext];
	if(type == MESSAGE_CHAT){
		if(text[0] == 0) return;
		if(boyId == client.playerBoyId){
			if(ProcessChatCommandLocal(text)) return;
		}
		if(app.role != ROLE_REMOTE){
			if(ProcessChatCommandRemote(boyId, text)) return;
		}
	}
	msg->boyId = boyId;
	msg->sentFrame = game.frame;
	switch(type){
		case MESSAGE_CHAT:
			strncpy(msg->text, text, CHAT_MESSAGE_LEN);
			msg->text[CHAT_MESSAGE_LEN-1] = 0;
			client.chat.messageNext = (client.chat.messageNext + 1) % CHAT_HISTORY_MAX;
			if(client.chat.messagesTotal < CHAT_HISTORY_MAX){
				client.chat.messagesTotal++;
			}
			if(client.chat.messagesShowing < CHAT_HISTORY_MAX){
				client.chat.messagesShowing++;
			}
			break;
		case MESSAGE_CUTSCENE:
			if((boyId != -1) && (boyId != client.playerBoyId)) return;
			if(client.ui.cutsceneCount < _countof(client.ui.cutscene) ){
				char* sprName = strtok(text, "$");
				char* cutText = strtok(NULL, "$");
				if(sprName && cutText){
					int id = client.ui.cutsceneCount++;
					client.ui.cutscene[id].spriteId = FindSpriteByName(sprName);
					strncpy(client.ui.cutscene[id].text, cutText, sizeof(client.ui.cutscene[id].text));
					client.ui.cutscene[id].startTime = client.time;
				}
			}
			break;
	}

	if(app.role == ROLE_HOST){
		ResponseMessage_t* msg = GetResponseBuffer();
		msg->code = RESPONSE_CHATMSG;
		msg->chat.type = (char)type;
		msg->chat.boyId = (char)boyId;
		msg->chat.param1 = (char)param1;
		msg->chat.param2 = (char)param2;
		strncpy((char*)&msg->chat.text, text, sizeof(msg->chat.text));
		BroadcastResponse(CHAN_STATUS, 1+sizeof(struct ResponseChat_st));
	}
}

void DrawChatBox(){
	char buffer[2048];
	char* text;
	Vector2 chatPos = (Vector2){client.width - 420, client.height*3/4};
	Rectangle chatBox = (Rectangle){chatPos.x, chatPos.y + 22, 240, 22};
	
	for(int i=1; i <= client.chat.messagesShowing; i++){
		int msgId = mod2(client.chat.messageNext - i, CHAT_HISTORY_MAX);
		ChatMessage_t* msg = &client.chat.history[msgId];
		float elapsed = (game.frame + client.deltaFrame - msg->sentFrame);
		float offsetX = (elapsed < 20) ? 40*powf(1-elapsed/20, 2) : 0;
		int displayDur = 5*TICKS_PER_SEC;
		int fadingDur = 1*TICKS_PER_SEC;
		Color color = Fade(WHITE, 0.6);
		if(msg->boyId == -1){
			text = &msg->text[0];
		}else{
			Boy_t* sender = GetBoy(msg->boyId);
			snprintf(buffer, sizeof(buffer), "%s [%i]: %s", sender->name, msg->boyId, msg->text);
			text = &buffer[0];
		}
		if(elapsed > displayDur){
			color = Fade(color, 1-((elapsed-displayDur-1)/fadingDur));
		}
		DrawText(text, chatPos.x+offsetX, chatPos.y, 20, color);
		chatPos.y -= 20;
		if(elapsed >= (displayDur+fadingDur)){
			client.chat.messagesShowing--;
		}
	}

	if(client.chat.inputOpen){
		GuiTextBox(chatBox, client.chat.inputText, CHAT_MESSAGE_LEN, true);
		if(IsKeyPressed(KEY_ENTER)){
			if(app.role == ROLE_REMOTE){
				RequestMessage_t* msg = GetRequestBuffer();
				msg->code = REQUEST_CHATMSG;
				
				strncpy((char*)&msg->chat.text, client.chat.inputText, sizeof(msg->chat.text));
				SendRequest(CHAN_STATUS, 1+sizeof(struct RequestChat_st));
			}else{
				Shout(MESSAGE_CHAT, client.playerBoyId, client.chat.inputText, 0, 0);
			}
			client.chat.inputOpen = false;
		}else if(IsKeyPressed(KEY_ESCAPE)){
			client.chat.inputOpen = false;
		}
	}else{
		if(IsKeyPressed(KEY_ENTER)){
			if(client.ui.cutsceneCount){
				client.ui.cutsceneSkipped = true;
			}else{
				client.chat.inputOpen = true;
				client.chat.inputText[0] = 0;
			}
		}
	}
}

#undef IMP_MESSAGES_H
#endif
