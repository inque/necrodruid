#ifndef LEVEL_OPS_H
#include "level.h"
#include "stdint.h"

void InitBrush(LvBrush_t* brush);
void LvAddBrush(int layerId, LvBrush_t desc);
void LvRemoveBrush(int brushId);
int LvAddLayer();
void LvRemoveLayer(int layerId);
void LvUpdateBounds();
void LvSetVar(const char* name, int val);
int LvGetVar(const char* name);

void SaveLevel();
void LvGenRandomTerrain();
void LvFixLayerGaps();

#pragma pack(push, 1)

typedef struct{
	uint32_t blockLength;
	uint32_t width, height;
	uint32_t sheetCount;
	uint32_t brushCount;
	uint32_t layerCount;
	uint32_t entityCount;
	uint32_t scriptCount;
} MapFile_header_t;

typedef struct{
	uint32_t blockLength;
	char sprPath[64];
} MapFile_sheetData_t;

typedef struct{
	uint32_t blockLength;
	char spriteName[64];
	uint32_t spriteFrame;
	unsigned char tint[4];
	float pos[3];
	float size;
	float z1, z2;
	uint32_t flags;
} MapFile_brushData_t;

typedef struct{
	uint32_t blockLength;
	char name[24];
	///1 - visible, 2 - additive blend
	uint32_t flags;
	uint32_t order;
	uint32_t brushStartId;
	uint32_t brushCount;
} MapFile_layerData_t;

typedef struct{
	uint32_t blockLength;
	uint32_t type;
	float pos[3];
	struct{
		char name[48];
		uint32_t toyType;
		uint32_t color;
		uint32_t healthMult;
		uint32_t team;
		uint32_t flags;
	} robot;
} MapFile_entityData_t;

typedef struct{
	struct{
		uint32_t blockLength;
		uint32_t flags;
		char comment[12];
	} header;
	struct{
		uint32_t blockLength;
		uint32_t type;
		union{
			struct{
				char name[16];
				uint32_t value;
			} var;
			struct{
				float pos[3];
				float radius;
			} point;
		};
	} condition;
	struct{
		uint32_t blockLength;
		uint32_t type;
		union{
			char chat[64];
			struct{
				uint32_t flags;
				char text[128];
			} cutscene;
		};
	} action;
} MapFile_scriptData_t;

#pragma pack(pop)

#define LEVEL_OPS_H
#endif
#ifdef IMP_LEVEL_OPS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "macros.h"
#include "sprites.h"
#include "utils.h"
#include "stdio.h"

//#define STB_PERLIN_IMPLEMENTATION
#include "stb_perlin.h"        // Required for: stb_perlin_fbm_noise3


static void BrushUpdateBbox(LvBrush_t* brush){
	Sprite_t* sprite = &client.sprites[brush->spriteId];
	SpriteShape_t* shape = &client.shapes[sprite->shapeId[brush->spriteFrame]];
	Vector2 pos = Project2d(brush->pos);
	Vector2 firstVert = shape->verts[0];
	float bbox[4] = {firstVert.x*brush->size+pos.x, firstVert.y*brush->size+pos.y, firstVert.x*brush->size+pos.x, firstVert.y*brush->size+pos.y};

	for(int i=1; i < shape->vertCount; i++){
		Vector2 vert = Vector2Add(Vector2Scale(shape->verts[i], brush->size), pos);
		if(vert.x < bbox[0]) bbox[0] = vert.x;
		if(vert.y < bbox[1]) bbox[1] = vert.y;
		if(vert.x > bbox[2]) bbox[2] = vert.x;
		if(vert.y > bbox[3]) bbox[3] = vert.y;
	}
	memcpy(&brush->bbox, &bbox, sizeof(bbox));
}

void InitBrush(LvBrush_t* brush){
	BrushUpdateBbox(brush);
	brush->sector[0] = (int)(brush->pos.x / SECTOR_SIZE);
	brush->sector[1] = (int)(brush->pos.y / SECTOR_SIZE);
	if(brush->flags & BRUSH_PHYSICAL){
		Sprite_t* sprite = &client.sprites[brush->spriteId];
		SpriteShape_t* shape = &client.shapes[sprite->shapeId[brush->spriteFrame]];

		if(brush->shape) cpSpaceRemoveShape(&game.space, brush->shape);
		//cpTransform tform = cpTransformMult(cpTransformScale(brush->size, brush->size), cpTransformTranslate(toCpv(brush->pos)));
		// cpTransform tform = cpTransformTranslate(toCpv(brush->pos));
		cpBody* body = cpSpaceGetStaticBody(&game.space);
		// brush->shape = cpSpaceAddShape(&game.space, cpPolyShapeNew(body, shape->vertCount, (cpVect*)&shape->verts, tform, 1));
		{
			cpVect vertices[MAX_SHAPE_VERTS];
			for(int i=0; i < shape->vertCount; i++){
				vertices[i] = cpvadd(cpvmult(toCpv(shape->verts[i]), brush->size), toCpv(brush->pos));
			}
			brush->shape = cpSpaceAddShape(&game.space, cpPolyShapeNew(body, shape->vertCount, (const cpVect*)&vertices, cpTransformIdentity, 1));
		}
	}
}

void LvAddBrush(int layerId, LvBrush_t desc){
	Level_t* level = &game.level;
	LvLayer_t* layer = &level->layers[layerId];
	int tailBrushId = layer->brushStartId + layer->brushCount;
	LvBrush_t* tailBrush = &level->brushes[tailBrushId];
	
	if(tailBrushId >= LV_MAX_BRUSHES) return;
	InsertElements(&level->brushCount, tailBrushId, 1, level->brushCount, sizeof(LvBrush_t));
	level->brushCount++;
	layer->brushCount++;

	forEachLvLayer(otherLayer, otherLayerId){
		if(otherLayerId <= layerId) continue; //???
		if(otherLayer->brushStartId >= tailBrushId){
			otherLayer->brushStartId += 1;
		}
	}

	*tailBrush = desc;
	InitBrush(tailBrush);

	// printf("layer: %i-%i. tail: %i. brush count: %i \n", layer->brushStartId, layer->brushCount, tailBrushId, level->brushCount);
	// printf("------ brushes -----\n");
	// forEachLvBrush(brush, brushId){
	// 	printf("id: %i. pos: %f %f\n", brushId, brush->pos.x, brush->pos.y);
	// }
	// printf("--------------------\n");
}

void LvRemoveBrush(int brushId){
	Level_t* level = &game.level;
	forEachLvLayer(layer, layerId){
		if(!layer->brushCount) continue;
		if(brushId >= layer->brushStartId){
			if(brushId < (layer->brushStartId+layer->brushCount)){
				layer->brushCount--;
			}
			layer->brushStartId--;
		}
	}
	RemoveElement(&level->brushes, brushId, level->brushCount, sizeof(LvBrush_t));
	level->brushCount--;
}

int LvAddLayer(){
	Level_t* level = &game.level;
	int layerId = level->layerCount;
	int tailBrushId = 0;
	forEachLvLayer(otherLayer, otherLayerId){
		int tail = otherLayer->brushStartId + otherLayer->brushCount;
		if(tail > tailBrushId) tailBrushId = tail;
	}
	level->layers[layerId] = (LvLayer_t){
		.name = "",
		.visible = true,
		.brushStartId = tailBrushId
	};
	level->layerCount++;
	return layerId;
}

void LvRemoveLayer(int layerId){
	Level_t* level = &game.level;
	LvLayer_t* layer = &level->layers[layerId];
	int tailBrushId = layer->brushStartId + layer->brushCount;
	
	RemoveElements(&level->brushes, layer->brushStartId, layer->brushCount, level->brushCount, sizeof(LvBrush_t));
	level->brushCount -= layer->brushCount;

	forEachLvLayer(otherLayer, otherLayerId){
		if(otherLayerId <= layerId) continue; //???
		if(otherLayer->brushStartId >= tailBrushId){
			otherLayer->brushStartId -= layer->brushCount;
		}
	}

	RemoveElement(&level->layers, layerId, level->layerCount, sizeof(LvLayer_t));
	level->layerCount--;
}

void LvUpdateBounds(){
	Level_t* level = &game.level;
	cpBody* body = cpSpaceGetStaticBody(&game.space);
	cpVect topLeft = cpv(0, 0);
	cpVect topRight = cpv(level->mapSize[0]*LV_TILESIZE, 0);
	cpVect bottomRight = cpv(level->mapSize[0]*LV_TILESIZE, level->mapSize[1]*LV_TILESIZE);
	cpVect bottomLeft = cpv(0, level->mapSize[1]*LV_TILESIZE);
	level->bounds[0] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(body, topLeft, topRight, LV_TILESIZE));
	level->bounds[1] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(body, topRight, bottomRight, LV_TILESIZE));
	level->bounds[2] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(body, bottomRight, bottomLeft, LV_TILESIZE));
	level->bounds[3] = cpSpaceAddShape(&game.space, cpSegmentShapeNew(body, bottomLeft, topLeft, LV_TILESIZE));
}

void LvSetVar(const char* name, int val){
	LvVariable_t* var;
	for(int i=0; i < game.level.varCount; i++){
		var = &game.level.vars[i];
		if(!strncmp(var->name, name, sizeof(var->name))){
			var->val = val;
			return;
		}
	}
	var = &game.level.vars[game.level.varCount++];
	strncpy(var->name, name, sizeof(var->name)-1);
	var->val = val;
}

int LvGetVar(const char* name){
	for(int i=0; i < game.level.varCount; i++){
		LvVariable_t* var = &game.level.vars[i];
		if(!strncmp(var->name, name, sizeof(var->name))){
			return var->val;
		}
	}
	return 0;
}


void SaveLevel(){
	
	MapFile_header_t header             = {.blockLength = sizeof(MapFile_header_t)};
	MapFile_sheetData_t spriteSheetData = {.blockLength = sizeof(MapFile_sheetData_t)};
	MapFile_brushData_t brushData       = {.blockLength = sizeof(MapFile_brushData_t)};
	MapFile_layerData_t layerData       = {.blockLength = sizeof(MapFile_layerData_t)};
	MapFile_entityData_t entityData     = {.blockLength = sizeof(MapFile_entityData_t)};
	MapFile_scriptData_t scriptData;
	scriptData.header.blockLength    = sizeof(scriptData.header);
	scriptData.condition.blockLength = sizeof(scriptData.condition);
	scriptData.action.blockLength    = sizeof(scriptData.action);

	Level_t* level = &game.level;
	char actualPath[128];
	sprintf(actualPath, "maps/%s", level->filePath);
	FILE* file = fopen(actualPath, "wb");
	if(!file) return;

	uint32_t data[2];
	data[0] = CHR4('n','M','A','P');
	data[1] = 100;
	fwrite(data, 4, 2, file);

	header.blockLength = sizeof(header);
	header.width = level->mapSize[0];
	header.height = level->mapSize[1];
	header.sheetCount = level->sheetCount;
	header.brushCount = level->brushCount;
	header.layerCount = level->layerCount;
	header.entityCount = level->entityCount;
	header.scriptCount = level->scriptCount;
	fwrite(&header, sizeof(header), 1, file);

	//sheets
	for(int i=0; i < level->sheetCount; i++){
		SpriteSheet_t* sheet = &client.sheets[level->sheets[i]];

		strncpy((char*)&spriteSheetData.sprPath, sheet->spr_path, sizeof(spriteSheetData.sprPath));
		fwrite(&spriteSheetData, sizeof(spriteSheetData), 1, file);
	}

	//brushes
	for(int i=0; i < level->brushCount; i++){
		LvBrush_t* brush = &level->brushes[i];
		Sprite_t* sprite = &client.sprites[brush->spriteId];

		strncpy((char*)&brushData.spriteName, sprite->name, sizeof(brushData.spriteName));
		brushData.spriteFrame = brush->spriteFrame;
		brushData.tint[0] = brush->tint.r;
		brushData.tint[1] = brush->tint.g;
		brushData.tint[2] = brush->tint.b;
		brushData.tint[3] = brush->tint.a;
		brushData.pos[0] = brush->pos.x;
		brushData.pos[1] = brush->pos.y;
		brushData.pos[2] = brush->pos.z;
		brushData.size = brush->size;
		brushData.z1 = brush->z1;
		brushData.z2 = brush->z2;
		brushData.flags = brush->flags;
		fwrite(&brushData, sizeof(brushData), 1, file);
	}

	//layers
	for(int i=0; i < level->layerCount; i++){
		LvLayer_t* layer = &level->layers[i];

		strncpy((char*)&layerData.name, layer->name, sizeof(layerData.name));
		layerData.flags = (1 & layer->visible) | (2 & layer->blendMode);
		layerData.order = layer->order;
		layerData.brushStartId = layer->brushStartId;
		layerData.brushCount = layer->brushCount;
		fwrite(&layerData, sizeof(layerData), 1, file);
	}

	//entities
	for(int i=0; i < level->entityCount; i++){
		LvEntity_t* entity = &level->entities[i];
		if(entity->type == LVENT_NONE) continue;

		entityData.pos[0] = entity->pos.x;
		entityData.pos[1] = entity->pos.y;
		entityData.pos[2] = entity->pos.z;
		entityData.type = entity->type;
		switch(entity->type){
			case LVENT_ROBOTSPAWN:
				memcpy(entityData.robot.name, entity->robot.name, sizeof(entity->robot.name));
				entityData.robot.toyType = entity->robot.toyType;
				entityData.robot.color = *(uint32_t*)&entity->robot.color;
				entityData.robot.healthMult = entity->robot.healthMult;
				entityData.robot.team = entity->robot.team;
				entityData.robot.flags = 0;
				if(entity->robot.immortal) entityData.robot.flags |= 1;
				if(entity->robot.fatalizer) entityData.robot.flags |= 2;
				if(entity->robot.automated) entityData.robot.flags |= 4;
				if(entity->robot.leaderOnly) entityData.robot.flags |= 8;
				break;
		}
		fwrite(&entityData, sizeof(entityData), 1, file);
	}

	//terrain
	for(int iy=0; iy < level->mapSize[1]; iy++)
	for(int ix=0; ix < level->mapSize[0]; ix++){
		fwrite(&level->terrainHeight[ix][iy], 1, 1, file);
		fwrite(&level->terrainColor[ix][iy], 1, 1, file);
	}

	//script
	for(int i=0; i < level->scriptCount; i++){
		LvScriptLine_t* exp = &level->script[i];
		
		scriptData.header.flags = (1 & exp->multiUse);
		memcpy(&scriptData.header.comment, &exp->comment, sizeof(scriptData.header.comment));

		scriptData.condition.type = exp->condition.type;
		switch(exp->condition.type){
			case LVCOND_VAR_EQ:
				memcpy(&scriptData.condition.var.name, exp->condition.var.name, sizeof(scriptData.condition.var.name));
				scriptData.condition.var.value = exp->condition.var.value;
				break;
			case LVCOND_TRIGGER:
				scriptData.condition.point.pos[0] = exp->condition.point.pos.x;
				scriptData.condition.point.pos[1] = exp->condition.point.pos.y;
				scriptData.condition.point.pos[2] = exp->condition.point.pos.z;
				scriptData.condition.point.radius = exp->condition.point.radius;
				break;
		}

		scriptData.action.type = exp->action.type;
		switch(exp->action.type){
			case LVEXP_CHAT:
				memcpy(&scriptData.action.chat, &exp->action.chat, sizeof(scriptData.action.chat));
				break;
			case LVEXP_CUTSCENE:
				scriptData.action.cutscene.flags = 0;
				if(exp->action.cutscene.global) scriptData.action.cutscene.flags |= 1;
				memcpy(&scriptData.action.cutscene.text, &exp->action.cutscene.text, sizeof(scriptData.action.cutscene.text));
				break;
		}
		fwrite(&scriptData, sizeof(scriptData), 1, file);
	}

	fclose(file);
}

static inline unsigned char GetPixel(Image img, int ix, int iy){
	unsigned char* pix = (unsigned char*)img.data;
	return pix[(ix+(iy*img.width))*4];
}


static Image GenImagePerlinNoiseCustom(int width, int height, int offsetX, int offsetY, float scale)
{
    Color *pixels = (Color *)RL_MALLOC(width*height*sizeof(Color));
	float seed_x = 140000*randf();
	float seed_y = 140000*randf();

    for (int y = 0; y < height; y++)
    {
        for (int x = 0; x < width; x++)
        {
            float nx = (float)(x + offsetX + seed_x)*scale/(float)width;
            float ny = (float)(y + offsetY + seed_y)*scale/(float)height;

            // Typical values to start playing with:
            //   lacunarity = ~2.0   -- spacing between successive octaves (use exactly 2.0 for wrapping output)
            //   gain       =  0.5   -- relative weighting applied to each successive octave
            //   octaves    =  6     -- number of "octaves" of noise3() to sum

            // NOTE: We need to translate the data from [-1..1] to [0..1]
            float p = (stb_perlin_fbm_noise3(nx, ny, 1.0f, 2.0f, 0.5f, 6) + 1.0f)/2.0f;

            int intensity = (int)(p*255.0f);
            pixels[y*width + x] = (Color){intensity, intensity, intensity, 255};
        }
    }

    Image image = {
        .data = pixels,
        .width = width,
        .height = height,
        .format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8,
        .mipmaps = 1
    };

    return image;
}


void LvGenRandomTerrain(){
	Level_t* level = &game.level;
	Image imgHeight = GenImagePerlinNoiseCustom(level->mapSize[0], level->mapSize[1], 0, 0, 16.0);
	Image imgColor = GenImagePerlinNoiseCustom(level->mapSize[0], level->mapSize[1], 0, 0, 3.3);

	for(int iy=0; iy < level->mapSize[1]; iy++)
	for(int ix=0; ix < level->mapSize[0]; ix++){
		unsigned char* terrainHeight = &level->terrainHeight[ix][iy];
		unsigned char* terrainColor = &level->terrainColor[ix][iy];
		
		*terrainHeight = GetPixel(imgHeight, ix, iy);
		*terrainColor = GetPixel(imgColor, ix, iy);

		int valueRange = (LV_TERRAINTEXROWS * LV_TERRAINTEXROWS);
		int colorIndex = (*terrainColor / valueRange);
		int baseVal = colorIndex * valueRange;
		if(colorIndex & 1) *terrainColor = baseVal + (valueRange-1-(*terrainColor - baseVal));
		
	}
	UnloadImage(imgHeight);
	UnloadImage(imgColor);
}

void LvFixLayerGaps(){
	// Level_t* level = &game.level;
	// int lastTailBrush = 0;
	// forEachLvLayer(layer, layerId){
	// 	if(layer->brushStartId < 0) layer->brushStartId = 0;
	// 	if(layer->brushStartId != lastTailBrush){
	// 		int toRemove = (layer->brushStartId - lastTailBrush);
	// 		printf("something is wrong on layer '%s'. removing %i elements\n", layer->name, toRemove);
	// 		RemoveElements(&level->brushes, lastTailBrush, toRemove, level->brushCount, sizeof(LvLayer_t));
	// 		level->brushCount--;
	// 		forEachLvLayer(layer2, layer2Id){
	// 			if(layer2->brushStartId >= lastTailBrush) layer2->brushStartId -= toRemove;
	// 		}
	// 	}
	// 	//printf("'%s': %i-%i\n", layer->name, layer->brushStartId, layer->brushStartId + layer->brushCount);
	// 	lastTailBrush = layer->brushStartId + layer->brushCount;
	// }
}

#undef IMP_LEVEL_OPS_H
#endif
