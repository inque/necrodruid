
#ifdef __EMSCRIPTEN__
	#define GLSL_HEAD "#version 100\nprecision mediump float;\n"
#else
	#define GLSL_HEAD "#version 120\n"
#endif

static const char* VS_BASIC = GLSL_HEAD "\
\
attribute vec3 vertexPosition;     \n\
attribute vec2 vertexTexCoord;     \n\
attribute vec3 vertexNormal;       \n\
attribute vec4 vertexColor;        \n\
varying vec2 fragTexCoord;         \n\
varying vec4 fragColor;            \n\
varying vec3 fragNormal;           \n\
\
uniform mat4 mvp;                      \n\
\
void main(){                           \n\
        fragTexCoord = vertexTexCoord; \n\
        fragColor = vertexColor;       \n\
		fragNormal = normalize(vertexNormal); \n\
        gl_Position = mvp*vec4(vertexPosition, 1.0); \n\
}";

static const char* FS_BASIC = GLSL_HEAD "\
\
varying vec2 fragTexCoord; \n\
varying vec4 fragColor; \n\
varying vec3 fragNormal; \n\
\
uniform sampler2D texture0; \n\
uniform vec4 colDiffuse; \n\
\
void main(){ \n\
    vec4 texelColor = texture2D(texture0, fragTexCoord)*colDiffuse*fragColor; \n\
    //float light = dot(fragNormal, vec3(0.299, 0.587, 0.114)); \n\
	//texelColor.rgb *= vec3(0.7) + light*0.3;\n\
    gl_FragColor = texelColor; \n\
}";
