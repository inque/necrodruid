#ifndef SETTINGS_H

// To register a new option, add it in:
// - AppConfig_t
// - DefaultSettings()
// - LoadSettings()
// - SaveSettings()

typedef struct{
	int fullscreen;
	int vsync;
	int resolution[2];
	int windowPos[2];
	float scale;
	float soundVolume;
	float musicVolume;
	int interpolation;
	int gamepad;
	int port;
	char serverTitle[48];
	char playerName[32];
	int battleDur;
	char mapName[64];

	//secret.txt
	int tickrate;
	float persp_y, persp_z;

	//tmp
	float tmpDrag, tmpMulFrames, tmpMulForce;
} AppConfig_t;

void DefaultSettings();
void LoadSettings();
void SaveSettings();


#define SETTINGS_H
#endif
#ifdef IMP_SETTINGS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "string.h"
#include "stdlib.h"
#include "stdbool.h"
#include "stdio.h"

void DefaultSettings(){
	app.config = (AppConfig_t){
		.port = 56098,
		.serverTitle = "Untitled Server",
		.battleDur = 15,
		.mapName = "mp_dm",
		
		.fullscreen = 0,
		.vsync = 0,
		.resolution = {1024, 720},
		.windowPos = {-1, -1},
		.scale = 1,
		.soundVolume = 0.725f,
		.musicVolume = 0.675f,
		.tickrate = TICKS_PER_SEC,
		.interpolation = 1,
		.gamepad = 0,
		.persp_y = 0.75f,
		.persp_z = 0.25f,

		.tmpDrag = 0.4,
		.tmpMulForce = 1.5,
		.tmpMulFrames = 1
	};
	snprintf(app.config.playerName, sizeof(app.config.playerName), "Player %i", GetRandomValue(10, 99));
}

static void* FindCfgKey(char* cfg, size_t len, const char* key){
	size_t keyLen = strlen(key);
	size_t p = 0;
	while(p < len){
		if(!strncmp(key, cfg, keyLen)){
			while(p < len){
				p++; cfg++;
				if(cfg[-1] == '=') break;
			}
			size_t valLen = 0;
			while((p + valLen) < len){
				valLen++;
				if(cfg[valLen] == 0x0A) break;
				if(cfg[valLen] == 0)    break;
			}
			cfg[valLen] = 0;
			return (void*)cfg;
		}
		while(p < len){
			p++; cfg++;
			if(cfg[-1] == 0x0A) break;
			if(cfg[-1] == 0)   break;
		}
	}
	return NULL;
}

static void GetCfgOption_i(char* cfg, int len, const char* key, int* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atoi(val);
}

static void GetCfgOption_f(char* cfg, int len, const char* key, float* out){
	void* val = FindCfgKey(cfg, len, key);
	if(val) *out = atof(val);
}

static void GetCfgOption_s(char* cfg, int len, const char* key, char* out, size_t size){
	void* val = FindCfgKey(cfg, len, key);
	if(val) strncpy(out, val, size);
}

static void WriteCfgOption_i(const char* key, int value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%i\n", key, value);
	fwrite(line, 1, len, file);
}

static void WriteCfgOption_f(const char* key, float value, FILE* file){
	char line[1024];
	int len = sprintf(line, "%s=%f\n", key, value);
	fwrite(line, 1, len, file);
}

static void WriteCfgOption_s(const char* key, char* value, FILE* file){
	char line[1024];
	SanitizeString(value, false);
	int len = sprintf(line, "%s=%s\n", key, value);
	fwrite(line, 1, len, file);
}

static void SyncBeforeRead();
static void SyncAfterWrite();

#ifdef __EMSCRIPTEN__
	//make the next function callable from js
	EMSCRIPTEN_KEEPALIVE
#endif

void LoadSettings(){
	DefaultSettings();
	SyncBeforeRead();
	
	FILE* file = fopen(app.configPath, "r");
	if(file){
		int scale = 100;
		int volume = 70;
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg), file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "fullscreen",  &app.config.fullscreen);
		GetCfgOption_i(cfg, len, "vsync",       &app.config.vsync);
		GetCfgOption_i(cfg, len, "width",       &app.config.resolution[0]);
		GetCfgOption_i(cfg, len, "height",      &app.config.resolution[1]);
		GetCfgOption_i(cfg, len, "windowpos_x", &app.config.windowPos[0]);
		GetCfgOption_i(cfg, len, "windowpos_y", &app.config.windowPos[1]);
		GetCfgOption_i(cfg, len, "scale", &scale);
		app.config.scale = scale/100.0f;
		GetCfgOption_i(cfg, len, "sound_volume", &volume);
		app.config.soundVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "music_volume", &volume);
		app.config.musicVolume = volume/100.0f;
		GetCfgOption_i(cfg, len, "interpolation", &app.config.interpolation);
		GetCfgOption_i(cfg, len, "gamepad_num",   &app.config.gamepad);

		GetCfgOption_i(cfg, len, "server_port", &app.config.port);
		GetCfgOption_s(cfg, len, "server_title", app.config.serverTitle, sizeof(app.config.serverTitle));
		GetCfgOption_s(cfg, len, "player_name", app.config.playerName, sizeof(app.config.playerName));
		GetCfgOption_s(cfg, len, "map_name", app.config.mapName, sizeof(app.config.mapName));
	}

	// secret options
	file = fopen("secret.txt", "r");
	if(file){
		char cfg[8192];
		size_t len = fread(cfg, 1, sizeof(cfg), file);
		cfg[len] = 0;
		fclose(file);

		GetCfgOption_i(cfg, len, "gamespeed",     &app.config.tickrate);
		GetCfgOption_f(cfg, len, "perspective_y", &app.config.persp_y);
		GetCfgOption_f(cfg, len, "perspective_z", &app.config.persp_z);
	}
}

void SaveSettings(){
	FILE* file = fopen(app.configPath, "w");
	if(!app.config.fullscreen){
		Vector2 windowPos = GetWindowPosition();
		app.config.windowPos[0] = (int)windowPos.x;
		app.config.windowPos[1] = (int)windowPos.y;
		app.config.resolution[0] = GetScreenWidth();
		app.config.resolution[1] = GetScreenHeight();
	}
	if(file){
		WriteCfgOption_i("fullscreen",  app.config.fullscreen,    file);
		WriteCfgOption_i("vsync",       app.config.vsync,         file);
		WriteCfgOption_i("width",       app.config.resolution[0], file);
		WriteCfgOption_i("height",      app.config.resolution[1], file);
		WriteCfgOption_i("windowpos_x", app.config.windowPos[0],  file);
		WriteCfgOption_i("windowpos_y", app.config.windowPos[1],  file);
		WriteCfgOption_i("scale",        (int)(app.config.scale*100), file);
		WriteCfgOption_i("sound_volume", (int)(app.config.soundVolume*100), file);
		WriteCfgOption_i("music_volume", (int)(app.config.musicVolume*100), file);
		WriteCfgOption_i("interpolation", app.config.interpolation, file);
		WriteCfgOption_i("gamepad_num",   app.config.gamepad, file);

		WriteCfgOption_i("server_port", app.config.port, file);
		WriteCfgOption_s("server_title", app.config.serverTitle, file);
		WriteCfgOption_s("player_name", app.config.playerName, file);
		WriteCfgOption_s("map_name", app.config.mapName, file);

		fclose(file);
	}
	
	// write secret options if secret.txt exists
	file = fopen("secret.txt", "r");
	if(file){
		fclose(file);
		file = fopen("secret.txt", "w");

		WriteCfgOption_i("gamespeed",     app.config.tickrate, file);
		WriteCfgOption_f("perspective_y", app.config.persp_y, file);
		WriteCfgOption_f("perspective_z", app.config.persp_z, file);
		fclose(file);
	}

	SyncAfterWrite();
}

#ifdef __EMSCRIPTEN__
	static void SyncBeforeRead(){
		strcpy(app.configPath, "/mygame/config.txt");
		EM_ASM(
			if(Module.syncdone != 1){
				FS.mkdir('/mygame');
				FS.mount(IDBFS, {}, '/mygame');
				Module.syncdone = 0;
				FS.syncfs(true, function(err){
					if(err) return console.error(err);
					Module.syncdone = 1;
					Module.ccall('LoadSettings', null, null, null);
				});
			}
		);
	}

	static void SyncAfterWrite(){
		EM_ASM(
			FS.syncfs(false, function(err){if(err) console.error(err);} );
		);
	}
#else
	static void SyncBeforeRead(){};
	static void SyncAfterWrite(){};
#endif

#undef IMP_SETTINGS_H
#endif
