#include "gameapp.h"
#include "fogofwar.h"
#include "networking.h"
#include "utils.h"
#include "string.h"

int CreateBoy(int isRobot){
	Color defaultColors[] = {BLUE, RED, ORANGE, DARKGREEN, PURPLE, LIGHTGRAY, YELLOW, (Color){100,230,230,255}};
	for(int boyId=0; boyId < MAX_BOYS; boyId++){
		Boy_t* boy = GetBoy(boyId);
		if(boy->active) continue;

		memset(boy, 0, sizeof(Boy_t));
		boy->active = true;
		boy->team = boyId;
		boy->toyId = -1;
		boy->isRobot = isRobot;
		boy->leaderOnly = true;
		strcpy(boy->name, "Oleg");
		boy->ready = false;
		if(boyId < 8){
			boy->appearance.color = defaultColors[boyId];
		}else{
			boy->appearance.color = ColorFromHSV(GetRandomValue(0, 360), 0.8, 0.8);
		}
		return boyId;
	}
	return -1;
}

void RemoveBoy(int id){
	Boy_t* boy = GetBoy(id);
	boy->active = false;
	SyncBoy(id);
}

void SyncBoy(int boyId){
	int length;
	Boy_t* boy = GetBoy(boyId);
	ResponseMessage_t* msg = GetResponseBuffer();
	if(app.role != ROLE_HOST) return;
	msg->code = RESPONSE_BOY;
	msg->boy.boyId = (char)boyId;
	if(boy->active){
		msg->boy.active = 1;
		msg->boy.toyId = EncCharId(boy->toyId);
		strncpy((char*)&msg->boy.name, (char*)&boy->name, sizeof(boy->name));
		msg->boy.color = *(uint32_t*)&boy->appearance.color;
		length = 1+sizeof(struct ResponseBoy_st);
	}else{
		msg->boy.active = 0;
		length = 3;
	}
	BroadcastResponse(CHAN_STATUS, length);
}

void ResetBoyStats(int boyId){
	Boy_t* boy = GetBoy(boyId);
	
	boy->ready = false;

	for(int i=0; i < BOY_MAX_CARDS; i++){
		boy->cards[i].active = false;
	}

	memset(&boy->fog.visible, 0, sizeof(boy->fog.visible));
	memset(&boy->fog.visited, 0, sizeof(boy->fog.visited));
}

void UpdateBoys(){
	forEachBoy(boy, boyId){
		for(int i=0; i < BOY_MAX_CARDS; i++){
			PlayCard_t* card = &boy->cards[i];
			if(card->despawnTimer){
				if(!--card->despawnTimer){
					card->active = false;
				}
			}
		}
		UpdateFog(boyId);
		switch(boy->inputs.action.type){
			case BOY_ACTION_READY:
				boy->ready = !boy->ready;
				break;
		}
		if(boy->toyId != -1){
			Toy_t* toy = GetToy(boy->toyId);
			memcpy(&toy->inputs, &boy->inputs, sizeof(BoyInputs_t));
		}
		boy->inputs.buttonsPrev = boy->inputs.buttons;
		boy->inputs.buttons = 0;
		boy->inputs.action.type = BOY_ACTION_NONE;
	}
}
