#ifndef GAMECLIENT_H

#define CUTSCENE_QUEUE_LEN 8

#include "particles.h"
#include "messages.h"
#include "sprites.h"
#include "audio.h"
#include "assets.h"
#include "raylib.h"
#include "stdio.h"

typedef enum{
	SCREEN_MAINMENU,
	SCREEN_GAME,
	SCREEN_EDITOR
} ClientScreen_e;

typedef enum{
	SUBMENU_MAIN,
	SUBMENU_SERVERLIST,
	SUBMENU_LOADING,
	SUBMENU_CREDITS,
	SUBMENU_OPTIONS
} ClientSubmenu_e;

typedef struct{
	ClientScreen_e screen;
	ClientSubmenu_e submenu;
	int width, height;
	int halfWidth, halfHeight;
	float dpi, scale;
	float time, deltaTime;
	float lastTickTime, deltaFrame;
	int shouldClose;
	int paused;
	int playerBoyId;
	Camera2D cam, uiCam;
	Rectangle viewBounds;
	int tileViewBox[4];
	int sector[2];
	int sectorsInView;
	ParticleSystem_t fxWorld, fxWorldOrdered;
	ParticleSystem_t fxScreen;
	ChatQueue_t chat;
	SpriteSheet_t sheets[MAX_SHEETS];
	SpriteShape_t shapes[MAX_SHAPES];
	Sprite_t sprites[MAX_SPRITES];
	AppAssets_t assets;
	AudioContext_t audio;
	bool mouseOverUI;
	struct{
		int cardSelected;
		int cardLastHighlightedNum;
		float cardLastHighlightedTime;

		struct{
			int spriteId;
			char text[128];
			float startTime;
		} cutscene[CUTSCENE_QUEUE_LEN];
		int cutsceneCount;
		bool cutsceneSkipped;
		float combatActionLastTime;
	} ui;
	struct{
		bool abortable;
		bool failed;
		char headerText[64];
		char subText[64];
	} loading;
	struct{
		float lastMouseAim;
		float lastGamepadAim;
		float aimMagnitude;
		Vector2 dampedLookDir;
		float zoom;
		float dampedZoom;
	} controls;
	Vector2 camShakePendulum[8];
	Vector2 camShakeVelocity[8];
	float camShakeZoom;
} ClientState_t;

extern ClientState_t client;

void UpdateClient();
void PlayerControls();
void ResetClient();

#define GAMECLIENT_H
#endif
#ifdef IMP_GAMECLIENT_H
//==== implementation starts here ====

#include "gameapp.h"
#include "utils.h"
#include "string.h"

ClientState_t client = {
	.scale = 1
};

static void UpdateGameCamera();

void UpdateClient(){
	#ifdef __EMSCRIPTEN__
		client.dpi = emscripten_get_device_pixel_ratio();
	#else
		//client.dpi = GetWindowScaleDPI().x;
		client.dpi = 1;
	#endif
	client.width = GetScreenWidth();
	client.height = GetScreenHeight();
	client.halfWidth = client.width / 2;
	client.halfHeight = client.height / 2;
	client.cam.offset.x = (float)client.halfWidth;
	client.cam.offset.y = (float)client.halfHeight;
	client.time = (float)GetTime();
	client.deltaTime = GetFrameTime();
	client.deltaFrame = ((client.time - client.lastTickTime) * (float)GET_GAME_TICKRATE());
	
	{
		//todo: consider camera angle
		Vector2 topLeft = GetScreenToWorld2D(Vector2Zero(), client.cam);
		Vector2 bottomRight = GetScreenToWorld2D((Vector2){GetScreenWidth(), GetScreenHeight()}, client.cam);
		client.viewBounds = (Rectangle){topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y};

		client.sector[0] = (int)((topLeft.x+bottomRight.x)*0.5 / SECTOR_SIZE);
		client.sector[1] = (int)((topLeft.y+bottomRight.y)*0.5 / SECTOR_SIZE);
		client.sectorsInView = MAX(
			(int)ceilf(client.width  / (float)SECTOR_SIZE) + 1,
			(int)ceilf(client.height / (float)SECTOR_SIZE) + 1
		);

		client.tileViewBox[0] = (int)(client.viewBounds.x / LV_TILESIZE);
		client.tileViewBox[1] = (int)(client.viewBounds.y / app.config.persp_y / LV_TILESIZE);
		client.tileViewBox[2] = (int)ceilf((client.viewBounds.x + client.viewBounds.width) / LV_TILESIZE);
		client.tileViewBox[3] = (int)ceilf((client.viewBounds.y + client.viewBounds.height) / app.config.persp_y / LV_TILESIZE);
		client.tileViewBox[0] = MAX(0, MIN(game.level.mapSize[0], client.tileViewBox[0]));
		client.tileViewBox[1] = MAX(0, MIN(game.level.mapSize[1], client.tileViewBox[1]));
		client.tileViewBox[2] = MAX(0, MIN(game.level.mapSize[0], client.tileViewBox[2]));
		client.tileViewBox[3] = MAX(0, MIN(game.level.mapSize[1], client.tileViewBox[3]));
		//todo: check if these are even correct...

	}

	client.scale = client.dpi;
	client.cam.zoom = client.scale;
	client.uiCam.zoom = client.scale;

	UpdateGameCamera();
}

void ResetClient(){
	client.cam = (Camera2D){
		.target = {1000, 800},
		.rotation = 0,
		.zoom = 1
	};
	client.fxWorld.particleCount = 0;
	client.fxWorld.nextFreeParticle = 0;
	client.fxWorldOrdered.particleCount = 0;
	client.fxWorldOrdered.nextFreeParticle = 0;
	client.fxScreen.particleCount = 0;
	client.fxScreen.nextFreeParticle = 0;
	client.chat.messagesTotal = 0;
	client.chat.messageNext = 0;
	client.chat.messagesShowing = 0;
	memset(&client.ui, 0, sizeof(client.ui));
	client.ui.cardLastHighlightedNum = -1;
	client.controls.zoom = 1;
}

void PlayerControls(){
	int pad = app.config.gamepad;
	bool mouseAllowed = !client.mouseOverUI;
	bool keyboardAllowed = !client.chat.inputOpen;
	bool gamepadAllowed = true;
	bool actionsAllowed = (client.ui.cutsceneCount == 0);
	bool doFire = false, doJump = false, doUse = false, doAltFire = false;
	
	if(mouseAllowed){
		doFire |= IsMouseButtonDown(MOUSE_BUTTON_LEFT);
		doAltFire |= IsMouseButtonDown(MOUSE_BUTTON_RIGHT); //tmp
	}
	if(keyboardAllowed){
		doFire |= IsKeyDown(KEY_LEFT_CONTROL);
		doJump |= IsKeyDown(KEY_SPACE);
		doUse  |= IsKeyDown(KEY_E);
		doAltFire |= IsKeyDown(KEY_F);
	}
	if(gamepadAllowed){
		doFire |= IsGamepadButtonDown(pad, GAMEPAD_BUTTON_RIGHT_TRIGGER_1);
		doJump |= IsGamepadButtonDown(pad, GAMEPAD_BUTTON_RIGHT_FACE_DOWN);
		doUse  |= IsGamepadButtonDown(pad, GAMEPAD_BUTTON_RIGHT_FACE_LEFT);
		doAltFire |= IsGamepadButtonDown(pad, GAMEPAD_BUTTON_LEFT_TRIGGER_2);
	}

	if(client.playerBoyId != -1){
		Boy_t* player = GetBoy(client.playerBoyId);
		if(player->toyId != -1){
			Toy_t* toy = GetToy(player->toyId);

			if(actionsAllowed){
				if(doFire) player->inputs.buttons |= BTN_FIRE;
				if(doJump) player->inputs.buttons |= BTN_JUMP;
				if(doUse)  player->inputs.buttons |= BTN_USE;
				if(doAltFire) player->inputs.buttons |= BTN_ALTFIRE;
			}else{
				if(doFire || doJump){
					client.ui.cutsceneSkipped = true;
				}
			}

			//move direction
			{
				player->inputs.axes[0] = (IsKeyDown(KEY_D) - IsKeyDown(KEY_A));
				player->inputs.axes[1] = (IsKeyDown(KEY_S) - IsKeyDown(KEY_W));
				player->inputs.axes[0] += GetGamepadAxisMovement(pad, GAMEPAD_AXIS_LEFT_X);
				player->inputs.axes[0] += GetGamepadAxisMovement(pad, GAMEPAD_AXIS_LEFT_Y);
			}

			//look direction
			{
				Vector2 padRightStick = {
					GetGamepadAxisMovement(pad, GAMEPAD_AXIS_RIGHT_X),
					GetGamepadAxisMovement(pad, GAMEPAD_AXIS_RIGHT_Y)
				};
				if(Vector2Length(padRightStick) > 0.18){
					client.controls.lastGamepadAim = client.time;
				}
				if(Vector2Length(GetMouseDelta()) > 0){
					client.controls.lastMouseAim = client.time;
				}
				if(client.controls.lastGamepadAim > client.controls.lastMouseAim){
					Vector2 lookDir = Vector2Normalize(padRightStick);
					player->inputs.axes[2] = lookDir.x;
					player->inputs.axes[3] = lookDir.y;
					client.controls.aimMagnitude = Vector2Length(padRightStick);
				}else{
					Vector2 pos = Project2d(GetPos(&toy->limbs[0].tform));
					Vector2 mouse = GetScreenToWorld2D(GetMousePosition(), client.cam);
					Vector2 diff = Vector2Subtract(mouse, pos);
					Vector2 lookDir = Vector2Normalize(diff);
					player->inputs.axes[2] = lookDir.x;
					player->inputs.axes[3] = lookDir.y;
					client.controls.aimMagnitude = Vector2Length(diff) / (client.width+client.height)/2;
				}
			}
		}

		if(actionsAllowed){
			if(player->inputs.action.type == BOY_ACTION_NONE){
				//printf("ass ass ass2\n");
				if( (keyboardAllowed && IsKeyDown(KEY_R)) || ((game.state == STATE_LOBBY) && doFire) ){
					player->inputs.action.type = BOY_ACTION_READY;
				}

				if( (keyboardAllowed && IsKeyDown(KEY_U)) || (mouseAllowed && IsMouseButtonDown(MOUSE_BUTTON_MIDDLE)) ){
					player->inputs.action.type = BOY_ACTION_USECARD;
					player->inputs.action.useCard.num = client.ui.cardSelected;
				}
				if(keyboardAllowed){
					for(int i=0; i < 9; i++){
						if(IsKeyDown(KEY_ONE+i)){
							player->inputs.action.type = BOY_ACTION_USECARD;
							player->inputs.action.useCard.num = i;
						}
					}
				}
			}
		}
	}
}

void UpdateGameCamera(){
	if(client.screen != SCREEN_GAME) return;
	if(client.playerBoyId != -1){
		Boy_t* player = GetBoy(client.playerBoyId);
		if(player->toyId != -1){
			Toy_t* toy = GetToy(player->toyId);
			Vector3 pos = GetPos(&toy->limbs[0].tform);
			Vector2 targetPos = Project2d((Vector3){pos.x, pos.y, 0});
			float t  = SmoothOver(0.9, 0.5, client.deltaTime);
			float t2 = SmoothOver(0.7, 0.17, client.deltaTime);

			Vector2 lookDir = {
				toy->inputs.axes[2] * client.controls.aimMagnitude * 500,
				toy->inputs.axes[3] * client.controls.aimMagnitude * 500
			};
			client.controls.dampedLookDir = Vector2Lerp(client.controls.dampedLookDir, lookDir, t);;
			targetPos = Vector2Add(targetPos, client.controls.dampedLookDir);
			
			client.cam.target = Vector2Lerp(client.cam.target, targetPos, t);
		}
	}
	for(int i=0; i < 8; i++){
		float power = 1.0 / (1+i);
		float t = SmoothOver(0.9, power * 0.5, client.deltaTime);
		Vector2* pendulum = &client.camShakePendulum[i];
		Vector2* velocity = &client.camShakeVelocity[i];
		Vector2 offset = Vector2Scale(*pendulum, power);
		float magnitude = Vector2LengthSqr(*pendulum);
		if(magnitude < 0.05) continue;
		client.cam.target = Vector2Add(client.cam.target, offset);
		*velocity = Vector2Lerp(*velocity, Vector2Negate(*pendulum), t);
		*pendulum = Vector2Add(*pendulum, *velocity);
		*pendulum = Vector2Rotate(*pendulum, 2.0*i);
		if(magnitude < 0.5){
			*pendulum = Vector2Scale(*pendulum, 0.5);
			//*velocity = Vector2Scale(*velocity, 0.5);
		}
	}
	
	if(IsKeyDown(KEY_TAB)){
		Vector2 mouseDelta = GetMouseDelta();
		client.controls.zoom = MIN(1.25, MAX(0.75, client.controls.zoom + 0.025 * mouseDelta.x));
	}
	client.controls.dampedZoom += (client.controls.zoom - client.controls.dampedZoom) * SmoothOver(0.9, 0.6, client.deltaTime);

	client.cam.zoom *= client.controls.dampedZoom;
	client.cam.zoom += client.camShakeZoom;
	client.camShakeZoom *= SmoothOver(0.9, 0.95, client.deltaTime);
}

#undef IMP_GAMECLIENT_H
#endif
