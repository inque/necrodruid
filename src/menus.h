#ifndef MENUS_H
#include "gameapp.h"

void MenuBackground();
void MainMenu();
void PauseMenu();

#define MENUS_H
#endif
#ifdef IMP_MENUS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "geometry.h"
#include "utils.h"
#include "masterserver.h"
#include "raylib.h"
#include "raygui.h"

typedef struct{
	int selected;
	float lastChanged;
} ButtonDeck_t;

static struct{
	ButtonDeck_t mainDeck;
	ButtonDeck_t pauseDeck;
	ButtonDeck_t optionsDeck;
	ButtonDeck_t serverListDeck;
} menu;


static unsigned char ButtonDeck(const char** options, ButtonDeck_t* state, Rectangle* bounds, bool focused){
	float elapsed = (client.time - state->lastChanged);
	bool clicked = false;
	int optionId = 0;
	const char* option;
	while(option = options[optionId]){
		Rectangle zone = CutTop(bounds, 35);
		bool selected = (state->selected == optionId);
		bool highlighted = IsMouseInRect(zone);
		Color color = selected ? WHITE : (Color){200, 200, 200, highlighted ? 240 : 180};
		float offset = selected ? 1-powf(1-MIN(elapsed, 0.175) / 0.175, 2) : 0;
		DrawText((option[1]=='/') ? (option+2) : option, zone.x + offset*16, zone.y, 30, color);
		if(selected){
			DrawCircle(zone.x - 8, zone.y + 12, 4, color);
		}
		if(highlighted && IsMouseButtonPressed(MOUSE_BUTTON_LEFT)){
			state->selected = optionId;
			clicked = true;
		}
		CutTop(bounds, 2);
		optionId++;
	}

	if(focused){
		const float stickThreshold = 0.75;
		static float leftStickLast;
		float leftStick = GetGamepadAxisMovement(app.config.gamepad, GAMEPAD_AXIS_LEFT_Y);

		bool pressingEnter = (
			IsKeyPressed(KEY_ENTER)
			|| IsKeyPressed(KEY_SPACE)
			|| IsKeyPressed(KEY_D)
			|| IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)
			|| clicked
		);
		bool pressingUp = (
			IsKeyPressed(KEY_S)
			|| IsKeyPressed(KEY_DOWN)
			|| IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_LEFT_FACE_DOWN)
			|| ((leftStick < -stickThreshold) && (leftStickLast >= -stickThreshold))
		);
		bool pressingDown = (
			IsKeyPressed(KEY_W)
			|| IsKeyPressed(KEY_UP)
			|| IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_LEFT_FACE_UP)
			|| ((leftStick > stickThreshold) && (leftStickLast <= stickThreshold))
		);
		leftStickLast = leftStick;

		if(pressingEnter){
			MyPlaySound(client.assets.sounds.click[0], 0.7, 0);
			return (options[state->selected][1]=='/') ? options[state->selected][0] : state->selected;
		}
		if(pressingUp){
			state->selected = (state->selected + 1) % optionId;
			state->lastChanged = client.time;
		}
		if(pressingDown){
			state->selected = mod2(state->selected - 1, optionId);
			state->lastChanged = client.time;
		}
		if(pressingUp ^ pressingDown){
			MyPlaySound(client.assets.sounds.click[1], 0.7, pressingUp ? -0.2 : 0.2);
		}
	}
	return 255;
}

void MenuBackground(){
	Color bg = {16, 16, 32, 200};
	int ms = Millisecs();
	int animTick = (ms >> 4);

	DrawRectangleRec(FsRect(), bg);

	if((animTick >> 4) & 1){
		Color noise = {224, 160, 160, 255};
		for(int i = 0; i < (animTick & 3); i++){
			DrawRectangle((client.width * randf()), (client.height * randf()), 7, 2, noise);
		}
	}

}

void MainMenu(){
	unsigned char press;

	//MasterUpdate(true, false);

	switch(client.submenu){
		case SUBMENU_LOADING:
			DrawText(client.loading.headerText, client.halfWidth - MeasureText(client.loading.headerText, 30)/2, client.halfHeight - 50, 30, client.loading.failed ? RED : WHITE);
			DrawText(client.loading.subText, client.halfWidth - MeasureText(client.loading.subText, 18)/2, client.halfHeight + 20, 18, LIGHTGRAY);

			if(client.loading.abortable){
				if(IsKeyPressed(KEY_ESCAPE) || IsMouseButtonPressed(MOUSE_BUTTON_LEFT) || IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_RIGHT_FACE_DOWN)){
					client.submenu = SUBMENU_MAIN;
					CleanUpNetworking();
				}
			}
			break;
		case SUBMENU_SERVERLIST: {
			Rectangle bounds = {client.halfWidth - 300, client.height - 500, 200, 400};
			int serverCount, serversActive = 0;
			GameServerEntry_t* servers = GetServerList(&serverCount);
			char* options[SERVERS_PER_PAGE+2];
			options[0] = "Go back";
			for(int i=0; i < serverCount; i++){
				GameServerEntry_t* server = &servers[serversActive];
				if(server->isSynchronized){
					snprintf(server->textLine, sizeof(server->textLine), ">> %s(%i/%i) '%s'. %ims", server->isCompatible ? "" : "[NOT COMPAT] ", server->playersOn, server->playersMax, server->title, server->ping);
					options[1+serversActive++] = server->textLine;
				}else{
					if((!server->lastPinged) || ((Millisecs() - server->lastPinged) > 500)){
						PingServer(server);
					}
				}
			}
			options[1+serversActive] = NULL;
			press = ButtonDeck(options, &menu.serverListDeck, &bounds, true);
			if(press == 0){
				client.submenu = SUBMENU_MAIN;
			}else if(press != 255){
				serversActive = 0;
				for(int i=0; i < serverCount; i++){
					GameServerEntry_t* server = &servers[serversActive];
					if(server->isSynchronized){
						if(serversActive == (press-1)){
							NetworkHostJoin((uint32_t*)&server->ip, server->port);
							break;
						}else{
							serversActive++;
						}
					}
				}
			}
		} break;
		case SUBMENU_OPTIONS: {
			Rectangle bounds = {client.halfWidth - 300, client.height - 500, 200, 400};
			char textVolSound[128];
			char textVolMusic[128];
			char textGamepad[128];
			sprintf(textVolSound, "s/Sound volume: %i%%", (int)(app.config.soundVolume*100));
			sprintf(textVolMusic, "m/Music volume: %i%%", (int)(app.config.musicVolume*100));
			sprintf(textGamepad, "g/Gamepad: '%s' [%i/4]", IsGamepadAvailable(app.config.gamepad) ? GetGamepadName(app.config.gamepad) : "empty slot", app.config.gamepad);

			//const char* options[] = {"b/< Save and go back", "s/Sound volume", "m/Music volume", "g/Gamepad", NULL};
			const char* options[] = {"b/[Save and go back]", textVolSound, textVolMusic, textGamepad, NULL};
			press = ButtonDeck(options, &menu.optionsDeck, &bounds, true);
			if(press == 'b'){
				client.submenu = SUBMENU_MAIN;
				SaveSettings();
			}else if(press == 'g'){
				app.config.gamepad = (app.config.gamepad + 1) % 4;
			}else{
				bool pressedLeft = (IsKeyPressed(KEY_A) || IsKeyPressed(KEY_LEFT) || (IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_LEFT_FACE_LEFT)));
				bool pressedRight = (IsKeyPressed(KEY_D) || IsKeyPressed(KEY_RIGHT) || (IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_LEFT_FACE_RIGHT)));
				switch(menu.optionsDeck.selected){
					case 1:
						if(pressedLeft) app.config.soundVolume = MAX(app.config.soundVolume-0.1, 0);
						if(pressedRight) app.config.soundVolume = MIN(app.config.soundVolume+0.1, 1);
						break;
					case 2:
						if(pressedLeft) app.config.musicVolume = MAX(app.config.musicVolume-0.1, 0);
						if(pressedRight) app.config.musicVolume = MIN(app.config.musicVolume+0.1, 1);
						break;
				}

			}

		} break;
		case SUBMENU_CREDITS: {
			DrawText("Thank you for playing my game.\n\nMaking it was extremly challenging.\nAll levels and story were made up in 3 hours before the deadline.\nToo bad there wasn't enough time to finish multiplayer.\nSpecial thanks to everyone who was around.\nBye bye.", client.halfWidth, client.halfHeight, 20, GREEN);
			if(IsKeyPressed(KEY_ESCAPE)) client.submenu = SUBMENU_MAIN;
		} break;
		case SUBMENU_MAIN:
		default: {
			Rectangle bounds = {50, client.height - 500, 200, 400};

			BlitSprite(client.assets.sprite.menuBg, 0, (Vector2){0, client.halfHeight-300}, WHITE, client.width/1024.0 );
			DrawRectangle(30, 0, 230, client.height, Fade(BLACK, 0.4));

			DrawText("Made by Nikolay N.\nfor GaijinJam1\nin August 2021\nv0.1", client.width-250,client.height-170,20,GREEN);

			const char* options[] = {"p/Play", "w/Survarium", "o/Options", "q/Quit", "l/Dev tools", NULL};
			press = ButtonDeck(options, &menu.mainDeck, &bounds, true);
			switch(press){
				case 255: break;
				case 'p':
					client.screen = SCREEN_GAME;
					StartGame(ROLE_LOCAL, "spc_1.nmp");
					game.mode = MODE_STORY;
					break;
				case 'w':
					client.screen = SCREEN_GAME;
					StartGame(ROLE_LOCAL, "surv.nmp");
					game.mode = MODE_SURVARIUM;
					break;
				case 'o':
					client.submenu = SUBMENU_OPTIONS;
					break;
				// case 'j':
				// 	client.submenu = SUBMENU_SERVERLIST;
				// 	break;
				// case 'h':
				// 	if(NetworkHostCreate()){
				// 		client.screen = SCREEN_GAME;
				// 		StartGame(ROLE_HOST, app.config.mapName);
				// 		game.mode = MODE_BATTLE;
				// 	}
				// 	break;
				case 'q':
					client.shouldClose = true;
					break;
				case 'l':
					client.screen = SCREEN_EDITOR;
			}
		} break;
	}

}

void PauseMenu(){
	Rectangle bounds = {client.width/4, client.halfHeight, 250, 350};
	//const char* options[] = {"c/Continue", "o/Options", "q/Quit to menu", NULL};
	const char* options[] = {"c/Continue", "q/Quit to menu", NULL};
	char press;

	DrawRectangleGradientV(bounds.x, bounds.y, bounds.width, bounds.height, Fade(DARKGRAY, 0.35), Fade(BLACK, 0));
	Extend(&bounds, -20);
	press = ButtonDeck(options, &menu.pauseDeck, &bounds, true);
	switch(press){
		case 255: break;
		case 'c':
			client.paused = false;
			break;
		case 'q':
			client.paused = false;
			CloseGame(true);
			client.screen = SCREEN_MAINMENU;
			break;
	}
}

#undef IMP_MENUS_H
#endif
