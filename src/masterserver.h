#ifndef MASTERSERVER_H
#define SERVERS_PER_PAGE 64

#include "stdbool.h"
#include "stdint.h"

#define MASTER_HOST "nikky.dev"
#define MASTER_PORT 56998

typedef struct{
	bool isSynchronized;
	bool isCompatible;
	int lastPinged;
	int lastReceived;
	uint32_t ip[4];
	uint16_t port;
	unsigned int ping;
	int version;
	int playersOn;
	int playersMax;
	char gameType[16];
	char title[48];
	char textLine[64];
} GameServerEntry_t;

void InitMasterSocket();
void MasterRequestServers();
void MasterUpdate(bool list, bool host);
void PingServer(GameServerEntry_t* server);
GameServerEntry_t* GetServerList(int* outCount);
bool ProcessInfoRequest(void* _msg);


#define MASTERSERVER_H
#endif
#ifdef IMP_MASTERSERVER_H
//==== implementation starts here ====
#include "enet.h"
#include "masterpackets.h"
#include "networking.h"
#include "utils.h"
#include "stdio.h"

struct{
	ENetAddress address;
	ENetSocket sock;
	union{
		MasterRequestMessage_t requestData;
		InfoRequestMessage_t infoRequestData;
	};
	union{
		MasterResponseMessage_t responseData;
		InfoResponseMessage_t infoResponseData;
	};
	ENetBuffer requestBuffer;
	ENetBuffer responseBuffer;
	int lastSent;
	int lastReceived;
	bool serverListNeedsRefreshing;
	int serverListLastRequested;
	int serverCount;
	GameServerEntry_t servers[SERVERS_PER_PAGE];
} master;

void InitMasterSocket(){
	enet_address_set_host(&master.address, MASTER_HOST);
	master.address.port = MASTER_PORT;

	master.sock = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM);
	enet_socket_set_option(master.sock, ENET_SOCKOPT_NONBLOCK, 1);
	enet_socket_set_option(master.sock, ENET_SOCKOPT_IPV6_V6ONLY, 0);
	enet_socket_bind(master.sock, NULL);

	master.requestBuffer.data = (void*)&master.requestData;
	master.responseBuffer.data = (void*)&master.responseData;
	master.requestBuffer.dataLength = sizeof(master.requestData);
	master.responseBuffer.dataLength = sizeof(master.responseData);
}

static void GuessMasterRequestSize(){
	size_t res = 5;
	size_t maxSize = sizeof(master.requestData);
	switch(master.requestData.code){
	case MASTER_REQUEST_SERVERLIST:
		res += sizeof(struct MasterRequestServerList_st);
		break;
	case MASTER_REQUEST_HOST:
		res += sizeof(struct MasterRequestHost_st);
		break;
	}
	if(res > maxSize) res = maxSize;
	master.requestBuffer.dataLength = res;
}

static void SendMasterPacket(bool host){
	master.lastSent = Millisecs();
	GuessMasterRequestSize();
	master.requestData.magic = NET_MAGIC_ID;

	ENetSocket sock = master.sock;
	if(host){
		ENetHost* host = GetHost();
		if(!host) return;
		sock = host->socket;
	}
	int res = enet_socket_send(sock, &master.address, &master.requestBuffer, 1);
	
	// if(res == -1){
	// 	printf("last error: %i\n", WSAGetLastError());
	// }
}

void MasterRequestServers(){
}

static void ProcessMasterPacket(MasterResponseMessage_t* msg, int bytes){
	if(msg->magic != NET_MAGIC_ID) return;
	switch(msg->code){
		case MASTER_RESPONSE_SERVERLIST: {
			if((msg->serverList.serverSize * msg->serverList.serverCount) > (bytes - 9)){
				//printf("Wrong message length. (SERVER_LIST)\n");
				return;
			}
			master.serverCount = msg->serverList.serverCount;
			if(master.serverCount >= SERVERS_PER_PAGE){
				master.serverCount = (SERVERS_PER_PAGE - 1);
			}
			struct MasterResponseServerListEntry_st* src = &msg->serverList.servers[0];
			for(int i = 0; i < master.serverCount; i++){
				GameServerEntry_t* dst = &master.servers[i];
				dst->isSynchronized = false;
				dst->lastPinged = 0;
				dst->lastReceived = Millisecs();
				memcpy(&dst->ip, &src->ip, sizeof(dst->ip));
				dst->port = src->port;
				dst->title[0] = 0;
				
				AddPtr(&src, msg->serverList.serverSize);
			}
			master.serverListNeedsRefreshing = false;
		} break;
		default:
			break;
	}
}

static void ProcessInfoResponse(InfoResponseMessage_t* msg, GameServerEntry_t* server, int bytes){
	if(msg->magic != NET_MAGIC_ID) return;
	switch(msg->code){
		case INFO_RESPONSE_STATUS: {
			if(!server->isSynchronized){
				server->version = msg->status.version;
				server->isCompatible = msg->status.isCompatible;
				strncpy((char*)&server->title, (char*)&msg->status.title, sizeof(server->title));
				server->title[sizeof(server->title)-1] = 0;
				SanitizeString(server->title, false);
				server->isSynchronized = true;
			}
			strncpy((char*)&server->gameType, (char*)&msg->status.gameType, sizeof(msg->status.gameType));
			server->gameType[sizeof(server->gameType)-1] = 0;
			SanitizeString(server->gameType, false);
			server->playersOn = msg->status.playersOn;
			server->playersMax = msg->status.playersMax;
			server->ping = Millisecs() - msg->status.clientTimestamp;
			server->lastReceived = Millisecs();
		} break;
	}
}

void MasterUpdate(bool list, bool host){
	ENetAddress addr;
	bool receivedAny = false;
	int timeNow = Millisecs();
	int elapsedSinceLastSent = timeNow - master.lastSent;

	while(true){
		int bytesReceived = enet_socket_receive(master.sock, &addr, &master.responseBuffer, 1);
		// if(bytesReceived == -1){
		// 	printf("recv last error: %i\n", WSAGetLastError());
		// }
		if(bytesReceived <= 0) break;
		if((addr.port == master.address.port) && in6_equal(addr.host, master.address.host)){
			master.lastReceived = Millisecs();
			ProcessMasterPacket(&master.responseData, bytesReceived);
			receivedAny = true;
		}else if(list){
			for(int i=0; i < master.serverCount; i++){
				GameServerEntry_t* server = &master.servers[i];
				if((addr.port == server->port) && (memcmp(&addr.host, &server->ip, sizeof(addr.host)) == 0)){
					ProcessInfoResponse(&master.infoResponseData, server, bytesReceived);
				}

			}
		}
	}

	if((!master.serverListLastRequested) || ((timeNow - master.serverListLastRequested) > 8000)){
		master.serverListNeedsRefreshing = true;
		master.serverListLastRequested = timeNow;
	}

	if(list){
		if(master.serverListNeedsRefreshing && (!receivedAny) && (elapsedSinceLastSent > 250)){
			master.requestData.code = MASTER_REQUEST_SERVERLIST;
			master.requestData.serverList.version = 2;
			master.requestData.serverList.offset = 0;
			SendMasterPacket(false);
			 printf("sent packet %i.\n", timeNow);
		}
		for(int i = 0; i < master.serverCount; i++){
			GameServerEntry_t* server = &master.servers[i];
			if(server->isSynchronized && ((timeNow - server->lastReceived) > 4000)){
				server->isSynchronized = false;
			}
		}
	}

	if(host){
		if(elapsedSinceLastSent > 1000){
			master.requestData.code = MASTER_REQUEST_HOST;
			master.requestData.host.gameId = NET_MAGIC_ID; //NCDR
			master.requestData.host.unused = 0;
			SendMasterPacket(true);
		}
	}

}

void PingServer(GameServerEntry_t* server){
	ENetAddress addr;
	addr.port = server->port;
	memcpy(&addr.host, &server->ip, sizeof(addr.host));
	//enet_address_set_host(&addr, "localhost");

	// char ip[1024];
	// enet_address_get_host_ip(&addr, (char*)&ip, sizeof(ip));
	// printf("pinging. ip = %s. port = %i\n", (char*)&ip, server->port);

	addr.sin6_scope_id = 0;
	//addr.host.__in6_u.__u6_addr8 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xFF, 0xFF, 127, 0, 0, 1};
	//enet_address_set_host_ip(&addr, "::1/128");
	master.infoRequestData.magic = NET_MAGIC_ID;
	master.infoRequestData.code = INFO_REQUEST_STATUS;
	master.infoRequestData.status.version = 1;
	master.infoRequestData.status.clientTimestamp = Millisecs();
	master.requestBuffer.dataLength = 5 + sizeof(struct InfoResponseStatus_st);
	enet_socket_send(master.sock, &addr, &master.requestBuffer, 1);
	server->lastPinged = Millisecs();
}

bool ProcessInfoRequest(void* _msg){
	InfoRequestMessage_t* msg = (InfoRequestMessage_t*)_msg;
	ENetHost* host = GetHost();
	switch(msg->code){
		case INFO_REQUEST_STATUS: {
			master.infoResponseData.magic = NET_MAGIC_ID;
			master.infoResponseData.code = INFO_RESPONSE_STATUS;
			master.infoResponseData.status.clientTimestamp = msg->status.clientTimestamp;
			master.infoResponseData.status.version = 1;
			strcpy((char*)&master.infoResponseData.status.gameType, "Normal");
			master.infoResponseData.status.isCompatible = 1;
			master.infoResponseData.status.playersMax = 32;
			master.infoResponseData.status.playersOn = (char)app.playerCount;
			strncpy((char*)&master.infoResponseData.status.title, app.config.serverTitle, sizeof(master.infoResponseData.status.title));
			master.responseBuffer.dataLength = 5 + sizeof(struct InfoResponseStatus_st);
			enet_host_send_raw(host, &host->receivedAddress, (enet_uint8*)&master.infoResponseData, master.responseBuffer.dataLength);
			//char tmp[1024];
			//enet_address_get_host_ip(&host->receivedAddress, (char*)&tmp, sizeof(tmp));
			//printf("info responded to: %s\n", tmp);
		} break;
		default:
			return 0;
	}
	return 1;
}

GameServerEntry_t* GetServerList(int* outCount){
	*outCount = master.serverCount;
	return (GameServerEntry_t*)&master.servers;
}

#undef IMP_MASTERSERVER_H
#endif
