#include "gameapp.h"

#include "safe_windows.h"
#define ENET_IMPLEMENTATION
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include "enet.h"

#define IMP_MASTERSERVER_H
#include "masterserver.h"

#include "netpackets.h"


typedef struct{
	ENetHost* host;
	ENetPeer* peer;
	RequestMessage_t request;
	ResponseMessage_t response;
	int lastSentFrame;
	int lastReceivedFrame;
} NetworkingState_t;

static NetworkingState_t networking;


static int InterceptCallback(ENetHost* host, ENetEvent* event){
	InfoRequestMessage_t* msg;
	enet_host_get_received_data(host, (enet_uint8**)&msg);
	//printf("got intercept. %i\n", msg->magic);
	if(msg->magic != NET_MAGIC_ID) return 0;
	return ProcessInfoRequest(msg) ? 1 : 0;
}

void InitNetworking(){
    enet_initialize();
}

bool NetworkHostJoin(uint32_t* ip, uint16_t port){
	CleanUpNetworking();
	app.role = ROLE_REMOTE;

	ENetAddress address;
	memcpy(&address.host, ip, sizeof(address.host));
	address.port = port;
	address.sin6_scope_id = 0;

	client.screen = SCREEN_MAINMENU;
	client.submenu = SUBMENU_LOADING;
	client.loading.abortable = true;
	client.loading.failed = false;
	strcpy((char*)&client.loading.headerText, "Connecting...");
	//client.loading.subText[0] = 0;
	enet_address_get_host_ip(&address, (char*)&client.loading.subText, sizeof(client.loading.subText));

	networking.host = enet_host_create(NULL, 1, CHAN__COUNT, 0, 0);
	if(!networking.host){
		printf("something went wrong while creating host\n");
	}
	networking.peer = enet_host_connect(networking.host, &address, 3, 0);
	if(!networking.peer){
		printf("something went wrong while creating peer\n");
	}

	return true;
}

bool NetworkHostCreate(){
	CleanUpNetworking();

	ENetAddress address = (ENetAddress){
		.host = ENET_HOST_ANY,
		.port = app.config.port
	};
	
	networking.host = enet_host_create(&address, MAX_BOYS, CHAN__COUNT, 0, 0);
	if(networking.host == NULL){
		printf("Error creating a server.\n");
		return false;
	}

	enet_host_set_intercept(networking.host, (ENetInterceptCallback)InterceptCallback);
	
	return true;
}

void CleanUpNetworking(){
	if(app.role == ROLE_HOST){
        forEachBoy(boy, boyId){
            if(!boy->session.connected) continue;
			enet_peer_disconnect_now(boy->session.peer, 0);
			boy->session.connected = 0;            
        }
	}

	if(networking.peer){
		enet_peer_disconnect_now(networking.peer, 0);
		networking.peer = NULL;
	}

	if(networking.host){
		enet_host_destroy(networking.host);
		networking.host = NULL;
	}
}

void ResetNetworkingCache(){
	networking.lastReceivedFrame = -1;
	networking.lastSentFrame = -1;
}

static void SendResponse(ENetPeer* peer, int channel, int bytes){
	bool reliable = (channel == CHAN_STATUS);
	ENetPacketFlag flag = reliable ? ENET_PACKET_FLAG_RELIABLE : ENET_PACKET_FLAG_UNRELIABLE_FRAGMENT;
	ENetPacket* packet = enet_packet_create((void*)&networking.response, bytes, flag);
	enet_peer_send(peer, channel, packet);
	//enet_host_flush(networking.host);
}

static bool ProcessResponse(ResponseMessage_t* msg, int bytes);
static void ProcessRequest(RequestMessage_t* msg, ENetPeer* peer, int bytes);

static void BroadcastFrameData(){
	networking.lastSentFrame = game.frame;
	networking.response.code = RESPONSE_FRAME;
	networking.response.frame.frame = game.frame;
	networking.response.frame.mode = game.mode;
	networking.response.frame.state = game.state;
	networking.response.frame.timer = game.timer;
	networking.response.frame.progress = (game.progress * 255);
	int length = 1+13;
	// networking.response.frame.characterCount = 0;
	// networking.response.frame.itemCount = 0;
	// void* payload = &networking.response.frame._rest;
	// FrameResponseCharacterInfo_t* characterInfo = payload;
	// for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
	// 	Character_t* character = &game.characters[characterId];
	// 	if(!character->active) continue;
	// 	//if(!HasChanged(&character->tform)) continue;
	// 	int charInfoNum = networking.response.frame.characterCount++;
	// 	characterInfo[charInfoNum].characterId = (char)characterId;
	// 	characterInfo[charInfoNum].health = character->health;
	// 	characterInfo[charInfoNum].inventorySecondarySelectionSlot = character->inventorySecondarySelectionSlot;
	// 	PackTform(&characterInfo[charInfoNum].tform, &character->tform);
	// 	length += sizeof(FrameResponseCharacterInfo_t);
	// }
	// FrameResponseItemInfo_t* itemInfo = (FrameResponseItemInfo_t*)&characterInfo[networking.response.frame.characterCount];
	// for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
	// 	Item_t* item = &game.items[itemId];
	// 	if(!item->active) continue;
	// 	//if(!HasChanged(&item->tform)) continue;
	// 	int itemInfoNum = networking.response.frame.itemCount++;
	// 	itemInfo[itemInfoNum].itemId = (char)itemId;
	// 	PackTform(&itemInfo[itemInfoNum].tform, &item->tform);
	// 	itemInfo[itemInfoNum].posOffset[0] = item->posOffset.x;
	// 	itemInfo[itemInfoNum].posOffset[1] = item->posOffset.y;
	// 	length += sizeof(FrameResponseItemInfo_t);
	// 	//printf("packed item %i (%i)\n", itemId, itemInfoNum);
	// }

	// if((!game.projectileSystemState.projectileNextMinFreeId) && (!game.projectileSystemState.projectileMaxActiveId)){
	// 	networking.response.frame.projectileCount = 0;
	// }else{
	// 	FrameResponseProjectileInfo_t* projectileInfo = (FrameResponseProjectileInfo_t*)&itemInfo[networking.response.frame.itemCount];
	// 	int baseId = networking.lastSentProjectileId;
	// 	if(baseId >= game.projectileSystemState.projectileMaxActiveId) baseId = 0;
	// 	networking.response.frame.projectileBaseId = baseId;
	// 	networking.response.frame.projectileCount = 0;
	// 	for(int projectileId = baseId; projectileId < (baseId + MAX_PROJECTILES_PER_PACKET); projectileId++){
	// 		if(projectileId > game.projectileSystemState.projectileMaxActiveId) break;
	// 		Projectile_t* projectile = &game.projectileSystemState.projectiles[projectileId];
	// 		PackProjectile(projectileInfo, projectile);
	// 		if(projectile->type == PROJ_INACTIVE){
	// 			length += 1;
	// 			projectileInfo = (FrameResponseProjectileInfo_t*)((char*)projectileInfo + 1);
	// 		}else{
	// 			length += sizeof(FrameResponseProjectileInfo_t);
	// 			projectileInfo++;
	// 		}
	// 		networking.response.frame.projectileCount++;
	// 		networking.lastSentProjectileId = projectileId;
	// 	}
	// 	//printf("sent projectiles %i-%i (%i)\n", baseId, networking.lastSentProjectileId, baseId+networking.response.frame.projectileCount-1);
	// }

	forEachBoy(boy, boyId){
		if(boy->session.connected){
			if(boy->toyId != -1){
				Toy_t* toy = GetToy(boy->toyId);
				networking.response.frame.yourToy.useCardCooldown = MIN(255, toy->useCard.cooldown/10);
				networking.response.frame.yourToy.fallenTimer = MIN(255, toy->walk.fallenTimer);
			}else{
				networking.response.frame.yourToy.useCardCooldown = 0;
				networking.response.frame.yourToy.fallenTimer = 0;
			}
			SendResponse(boy->session.peer, CHAN_GAME, length);
		}
	}

	// for(int playerId = 0; playerId < MAX_PLAYERS; playerId++){
	// 	Player_t* player = &game.players[playerId];
	// 	if(player->active && player->session.connected){
	// 		if(player->characterId != -1){
	// 			Character_t* character = &game.characters[player->characterId];
	// 			networking.response.frame.yourCharacterLookingAtItemId = character->lookingAtItemId;
	// 		}else{
	// 			networking.response.frame.yourCharacterLookingAtItemId = -1;
	// 		}
	// 		SendResponse(player->session.peer, CHAN_GAME, length);
	// 	}
	// }
}

void UpdateNetworking(){
	if(networking.host){

		if(app.role == ROLE_HOST){
			if(networking.lastSentFrame != game.frame){
				BroadcastFrameData();
			}
		}

		ENetEvent event;
		bool shouldStop = false;
		while((!shouldStop) && enet_host_service(networking.host, &event, 0)){
			//printf("got something!!!!!!\n");
			bool timedOut = false;
			switch(event.type){
				case ENET_EVENT_TYPE_CONNECT: {
					if(app.role == ROLE_HOST){
						event.peer->data = NULL;
					}else if(app.role == ROLE_REMOTE){
						networking.request.code = REQUEST_AUTH;
						networking.request.auth.version = 1;
						strncpy((char*)&networking.request.auth.username, app.config.playerName, sizeof(networking.request.auth.username));
						SendRequest(CHAN_STATUS, 1+sizeof(struct RequestAuth_st));
					}
					printf("[E] received connect\n");
				} break;
				case ENET_EVENT_TYPE_RECEIVE: {
					if(app.role == ROLE_HOST){
						ProcessRequest((RequestMessage_t*)event.packet->data,event.peer, event.packet->dataLength);
					}else if(app.role == ROLE_REMOTE){
						if(ProcessResponse((ResponseMessage_t*)event.packet->data, event.packet->dataLength)){
							shouldStop = true;
						}
					}
					enet_packet_destroy(event.packet);
					//printf("[E] received receive\n");
				} break;
				case ENET_EVENT_TYPE_DISCONNECT_TIMEOUT:
					timedOut = true;
				case ENET_EVENT_TYPE_DISCONNECT: {
					if(app.role == ROLE_HOST){
						if(event.peer->data){
							int boyId = (int)event.peer->data;
							Boy_t* boy = GetBoy(boyId);
							char text[2048];
							if(!timedOut){
								sprintf(text, "> %s left.", boy->name);
							}else{
								sprintf(text, "> %s timed out.", boy->name);
							}
							Shout(MESSAGE_CHAT, -1, text, 1, 0);
							// forEachToy(toy, toyId){
							// 	if(toy->boyId == boyId){
							// 		toy->boyId = -1;
							// 	}
							// }
							// if(boy->heroId != -1){
							// 	RemoveToy(boy->heroId);
							// 	boy->heroId = -1;
							// 	boy->toyId = -1;
							// }
							// forEachToy(toy, toyId){
							// 	if(toy->boyId == boyId){
							// 		RemoveToy(toyId);
							// 	}
							// }
							RemoveBoy(boyId);
						}
					}else if(app.role == ROLE_REMOTE){
						client.screen = SCREEN_MAINMENU;
						client.submenu = SUBMENU_LOADING;
						app.role = ROLE_LOCAL;
						strcpy(client.loading.headerText, "Disconnected.");
						if(timedOut){
							strcpy(client.loading.subText, "Connection timed out.");
						}else{
							client.loading.subText[0] = 0;
						}
						client.loading.failed = true;
						client.loading.abortable = true;
					}
					printf("[E] received disconnect\n");
				} break;
				case ENET_EVENT_TYPE_NONE:
					break;
			}
		}
	}
}

void GetIPv6(const char* ip, uint32_t* out){
	ENetAddress tmpAddr;
	enet_address_set_host(&tmpAddr, ip);
	memcpy(out, &tmpAddr.host, sizeof(tmpAddr.host));
}

void* GetHost(){
	return networking.host;
}

RequestMessage_t* GetRequestBuffer(){
	return &networking.request;
}

ResponseMessage_t* GetResponseBuffer(){
	return &networking.response;
}

void SendRequest(int channel, int bytes){
	bool reliable = (channel == CHAN_STATUS);
	ENetPacketFlag flag = reliable ? ENET_PACKET_FLAG_RELIABLE : 0;
	ENetPacket* packet = enet_packet_create((void*)&networking.request, bytes, flag);
	enet_peer_send(networking.peer, channel, packet);
	//enet_host_flush(networking.host);
}

void BroadcastResponse(int channel, int bytes){
	forEachBoy(boy, boyId){
		if(boy->session.connected){
			SendResponse(boy->session.peer, channel, bytes);
		}
	}
}

static void ProcessRequest(RequestMessage_t* msg, ENetPeer* peer, int bytes){
	//printf("[REQ] 0x%x\n", msg->code);
	if(!peer->data){
		switch(msg->code){
			case REQUEST_AUTH: {
				bool failed = false;
				int boyId = CreateBoy(0);
				if(boyId == -1) failed = true;

				if(failed){
					networking.response.code = RESPONSE_AUTH;
					networking.response.auth.succeded = false;
					strcpy((char*)&networking.response.auth.failure.errorMessage, "You are banned! Fool!");
					SendResponse(peer, CHAN_STATUS, 1+1+sizeof(struct ResponseAuthFailure_st));
					//enet_host_flush(networking.host);
					enet_peer_disconnect_now(peer, 0);
				}else{
					Boy_t* boy = GetBoy(boyId);
					boy->session.connected = 1;
					boy->session.peer = peer;
					peer->data = (void*)(size_t)boyId;
					boy->session.localAccountId = 42;
					SanitizeString((char*)&boy->name, false);
					strncpy(boy->name, msg->auth.username, sizeof(msg->auth.username));
					boy->name[sizeof(boy->name)-1] = 0;
					//UnpackCustomizations(&player->customizations, &msg->auth.customizations);

					networking.response.code = RESPONSE_AUTH;
					networking.response.auth.succeded = true;
					networking.response.auth.success.playerId = (char)boyId;
					SendResponse(peer, CHAN_STATUS, 1+1+sizeof(struct ResponseAuthSuccess_st));

					networking.response.code = RESPONSE_NEWGAME;
					networking.response.newGame.gameMode = (char)game.mode;
					strncpy(networking.response.newGame.mapName, game.level.filePath, sizeof(networking.response.newGame.mapName));
					SendResponse(peer, CHAN_STATUS, 1+sizeof(struct ResponseNewGame_st));

					//todo: sync existing game state
					forEachBoy(anotherBoy, anotherBoyId){
						if(anotherBoyId == boyId) continue;
						SyncBoy(anotherBoyId);
					}

					forEachToy(toy, toyId){
						SyncToy(toyId);
					}

					// for(int characterId = 0; characterId < MAX_CHARACTERS; characterId++){
					// 	if(game.characters[characterId].active){
					// 		int length = PrepareResponseForCharacter(characterId);
					// 		SendResponse(peer, CHAN_STATUS, length);
					// 	}
					// }

					// for(int itemId = 0; itemId < MAX_ITEMS; itemId++){
					// 	if(game.items[itemId].active){
					// 		int length = PrepareResponseForItem(itemId);
					// 		SendResponse(peer, CHAN_STATUS, length);
					// 	}
					// }

					SyncBoy(boyId);

					char text[2048];
					sprintf(text, "> %s joined.", boy->name);
					//SendChatMessage(CHATMSG_GLOBAL, -1, text, 0);
					Shout(MESSAGE_CHAT, -1, text, 1, 0);
				}
			} break;
		}
	}else{
		int boyId = (int)peer->data;
		Boy_t* boy = GetBoy(boyId);
		switch(msg->code){
			// case REQUEST_INPUTS: {
			// 	UnpackControls(&player->controls, &msg->inputs.controls);
			// } break;
			case REQUEST_CHATMSG: {
				//PlayerSay(playerId, (const char*)msg->chat.text);
				//SanitizeString((char*)&msg->chat.text, false);
				Shout(MESSAGE_CHAT, boyId, (const char*)msg->chat.text, 0, 0);
			} break;
		}
	}
}

static bool ProcessResponse(ResponseMessage_t* msg, int bytes){
	//printf("[RES] 0x%x\n", msg->code);
	switch(msg->code){
		case RESPONSE_AUTH: {
			if(msg->auth.succeded){
				if(client.screen != SCREEN_GAME){
					client.screen = SCREEN_GAME;
					StartGame(ROLE_REMOTE, NULL);
					client.playerBoyId = msg->auth.success.playerId;
					MyPlaySound(client.assets.sounds.success, 0.8, 0);
				}
			}else{
				client.screen = SCREEN_MAINMENU;
				client.submenu = SUBMENU_LOADING;
				app.role = ROLE_LOCAL;
				strcpy(client.loading.headerText, "Connection refused.");
				strncpy(client.loading.subText, msg->auth.failure.errorMessage, sizeof(msg->auth.failure.errorMessage));
				client.loading.failed = true;
				client.loading.abortable = true;
				return true;
			}
		} break;
		case RESPONSE_NEWGAME: {
			game.mode = msg->newGame.gameMode;
			
			strncpy((char*)&game.level.filePath, msg->newGame.mapName, sizeof(msg->newGame.mapName));
			game.level.filePath[sizeof(game.level.filePath)-1] = 0;
			SanitizeString((char*)&game.level.filePath, true);

			app.shouldRestartGame = true;
			ResetNetworkingCache();

			forEachBoy(boy, boyId){
				boy->toyId = -1;
			}

			printf("got newgame\n");
			return true;
		} break;
		case RESPONSE_CHATMSG: {
			Shout(msg->chat.type, DecCharId(msg->chat.boyId), (const char*)&msg->chat.text, msg->chat.param1, msg->chat.param2);
		} break;
		case RESPONSE_BOY: {
			Boy_t* boy = GetBoy(msg->boy.boyId);
			if(msg->boy.active){
				boy->active = true;
				boy->toyId = DecCharId(msg->boy.toyId);
				strncpy((char*)boy->name, (char*)&msg->boy.name, sizeof(boy->name));
			}else{
				boy->active = 0;
			}
			printf("got player info. id: %i char: %i\n", msg->boy.boyId, msg->boy.toyId);
		} break;
		case RESPONSE_TOY: {
			Toy_t* toy = GetToy(msg->toy.toyId);
			if(msg->toy.active){
				int newBoyId = DecCharId(msg->toy.boyId);
				if(toy->active && (toy->boyId != -1)){
					Boy_t* oldBoy = GetBoy(toy->boyId);
					oldBoy->toyId = -1;
				}
				if(newBoyId != -1){
					Boy_t* newBoy = GetBoy(newBoyId);
					newBoy->toyId = msg->toy.toyId;
				}
				toy->boyId = newBoyId;
				toy->active = 1;
				toy->type = msg->toy.type;
				toy->health = msg->toy.health;
				UnpackTform(&toy->limbs[0].tform, &msg->toy.tform);
			}else{
				if(toy->active && (toy->boyId != -1)){
					Boy_t* boy = GetBoy(toy->boyId);
					if(boy->toyId == msg->toy.toyId){
						boy->toyId = -1;
					}
				}
				toy->active = 0;
			}
			printf("got char info. id: %i . active = %i. player: %i\n", msg->toy.toyId, msg->toy.active, msg->toy.boyId);
		} break;
		// case RESPONSE_ITEM: {
		// 	Item_t* item = &game.items[msg->item.itemId];
		// 	if(msg->item.active){
		// 		if(item->active){
		// 			if(item->ownerCharacterId != -1){
		// 				Character_t* owner = &game.characters[item->ownerCharacterId];
		// 				for(int i=0; i < CHAR_MAX_INVENTORY_SLOTS; i++){
		// 					if(owner->inventoryItemIds[i] == msg->item.itemId){
		// 						owner->inventoryItemIds[i] = -1;
		// 						break;
		// 					}
		// 				}
		// 			}
		// 		}
		// 		item->active = 1;
		// 		item->type = msg->item.type;
		// 		InitializeItemData(msg->item.itemId);
		// 		UnpackTform(&item->tform, &msg->item.tform);
		// 		if(msg->item.ownerId == 0xFF){
		// 			item->ownerCharacterId = -1;
		// 		}else{
		// 			if(msg->item.inventorySlot > CHAR_MAX_INVENTORY_SLOTS){
		// 				//printf("bigger than max inv slot. owner = %i. slot = %i.\n", msg->item.ownerId, msg->item.inventorySlot);
		// 				return false;
		// 			}
		// 			item->ownerCharacterId = msg->item.ownerId;
		// 			Character_t* owner = &game.characters[item->ownerCharacterId];
		// 			int existingItemId = owner->inventoryItemIds[msg->item.inventorySlot];
		// 			if(existingItemId != -1){
		// 				Item_t* existingItem = &game.items[existingItemId];
		// 				existingItem->ownerCharacterId = -1;
		// 			}
		// 			owner->inventoryItemIds[msg->item.inventorySlot] = msg->item.itemId;
		// 		}
		// 		//item->ownerCharacterId = (msg->item.ownerId == 0xFF) ? -1 : msg->item.ownerId;
		// 		//item->ownerCharacterId = -1;
		// 	}else{
		// 		item->active = 0;
		// 	}
		// 	//printf("got item info for %i. active = %i. owner = %i. slot = %i\n", msg->item.itemId, item->active, msg->item.ownerId, msg->item.inventorySlot);
		// } break;
		// case RESPONSE_PARTICLES: {
		// 	Vector2 pos = (Vector2){msg->particles.pos[0], msg->particles.pos[1]};
		// 	SpawnParticles(msg->particles.type, msg->particles.amount, pos);
		// } break;
		// case RESPONSE_PLAYER_STATS: {
		// 	Player_t* player = &game.players[msg->playerStats.playerId];
		// 	UnpackPlayerStats(&player->stats, &msg->playerStats.stats);
		// } break;
		case RESPONSE_FRAME: {
			if( (msg->frame.frame < networking.lastReceivedFrame) && (networking.lastReceivedFrame != -1) ){
				//printf("frame out of order\n");
				return false;
			}
			networking.lastReceivedFrame = msg->frame.frame;
			game.frame = msg->frame.frame;
			//printf("frame packet: %i\n", game.frame);
			game.mode = msg->frame.mode;
			game.state = msg->frame.state;
			game.progress = msg->frame.progress / 255.0;
			game.timer = msg->frame.timer;
			Boy_t* player = GetBoy(client.playerBoyId);
			if(player->toyId != -1){
				Toy_t* toy = GetToy(player->toyId);
				toy->useCard.cooldown = msg->frame.yourToy.useCardCooldown * 10;
				toy->walk.fallenTimer = msg->frame.yourToy.fallenTimer;
			}
			// void* payload = &msg->frame._rest;
			// FrameResponseCharacterInfo_t* characterInfo = payload;
			// for(int i=0; i < msg->frame.characterCount; i++){
			// 	Character_t* character = &game.characters[characterInfo->characterId];
			// 	if(characterInfo->health != character->health){
			// 		character->healthLastDamageFrame = game.frame;
			// 		character->health = characterInfo->health;
			// 	}
			// 	character->inventorySecondarySelectionSlot = characterInfo->inventorySecondarySelectionSlot;
			// 	UnpackTform(&character->tform, &characterInfo->tform);
			// 	characterInfo++;
			// 	//printf("unpacked char id: %i (%i, %p)\n", characterInfo->characterId, i, &characterInfo);
			// }
			// FrameResponseItemInfo_t* itemInfo = (FrameResponseItemInfo_t*)characterInfo;
			// for(int i=0; i < msg->frame.itemCount; i++){
			// 	Item_t* item = &game.items[itemInfo->itemId];
			// 	UnpackTform(&item->tform, &itemInfo->tform);
			// 	item->posOffset.x = itemInfo->posOffset[0];
			// 	item->posOffset.y = itemInfo->posOffset[1];
			// 	//printf("unpacked item %i (%i, %p)\n", itemInfo->itemId, i, &itemInfo);
			// 	itemInfo++;
			// }
			// if(!msg->frame.projectileCount){
			// 	game.projectileSystemState.projectileMaxActiveId = 0;
			// 	game.projectileSystemState.projectileNextMinFreeId = 0;
			// 	game.projectileSystemState.projectiles[0].type = PROJ_INACTIVE;
			// }else{
			// 	FrameResponseProjectileInfo_t* projectileInfo = (FrameResponseProjectileInfo_t*)itemInfo;
			// 	for(int i=0; i < msg->frame.projectileCount; i++){
			// 		int projectileId = msg->frame.projectileBaseId + i;
			// 		if(!projectileInfo->type){
			// 			RemoveProjectile(projectileId);
			// 			projectileInfo = (FrameResponseProjectileInfo_t*)((char*)projectileInfo+1);
			// 		}else{
			// 			Projectile_t* projectile = &game.projectileSystemState.projectiles[projectileId];
			// 			if(projectile->type == PROJ_INACTIVE){
			// 				AddedProjectileAt(projectileId);
			// 			}
			// 			UnpackProjectile(projectile, projectileInfo);
			// 			projectileInfo++;
			// 		}
			// 	}
			// }
		} break;
	}
	return false;
}


