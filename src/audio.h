#ifndef AUDIO_H
#include "soloud_c.h"
#include "raylib.h"
#include "stdbool.h"

typedef Wav* Sound_t;
typedef WavStream* Music_t;

typedef struct{
	Soloud *soloud;
	
	struct GameMusic_st{
		int handle;
		bool active;
		float mix;
		Music_t music;
	} music[2];
} AudioContext_t;

void InitAudio();
void DestroyAudio();

void MyLoadSound(Sound_t* sound, const char* filename);
void MyLoadMusic(Music_t* music, const char* filename);
int MyPlaySound(Sound_t sound, float volume, float pan);
int MyPlaySound3d(Sound_t sound, Vector3 pos);
int MyPlayMusic(Music_t music, float volume, float pan);
void UpdateAudio();

#define AUDIO_H
#endif
#ifdef IMP_AUDIO_H
//==== implementation starts here ====
#include "gameapp.h"
#include "utils.h"

void InitAudio(){
	bool noAudio = false;
	client.audio.soloud = Soloud_create();
	Soloud_initEx(client.audio.soloud, SOLOUD_CLIP_ROUNDOFF | SOLOUD_ENABLE_VISUALIZATION, noAudio ? SOLOUD_NULLDRIVER : SOLOUD_AUTO, SOLOUD_AUTO, SOLOUD_AUTO, SOLOUD_AUTO);
}

void DestroyAudio(){
	Soloud_deinit(client.audio.soloud);
	Soloud_destroy(client.audio.soloud);
}

void MyLoadSound(Sound_t* sound, const char* filename){
	*sound = Wav_create();
	Wav_load(*sound, filename);
}

void MyLoadMusic(Music_t* music, const char* filename){
	*music = WavStream_create();
	WavStream_setLooping(*music, 1);
	WavStream_load(*music, filename);
}

static bool TooManySounds(){
	static int soundCount;
	static int frame;
	if(client.screen != SCREEN_GAME) return false;
	if(game.frame != frame){
		frame = game.frame;
		soundCount = 0;
	}else{
		if(++soundCount > 2) return true;
	}
	return false;
}

int MyPlaySound(Sound_t sound, float volume, float pan){
	if(TooManySounds()) return 0;
	return Soloud_playEx(client.audio.soloud, sound, volume * app.config.soundVolume, pan, 0, 0);
}

int MyPlaySound3d(Sound_t sound, Vector3 pos){
	if(TooManySounds()) return 0;
	return Soloud_play3dEx(client.audio.soloud, sound, pos.x, pos.z, pos.y, 0, 0, 0, app.config.soundVolume, 0, 0);
}

int MyPlayMusic(Music_t music, float volume, float pan){
	return Soloud_playEx(client.audio.soloud, music, volume * app.config.musicVolume, pan, 0, 0);
}

static void UpdateGameMusic(){
	float t;
	float attenuation = 0.05;

	bool action = (client.screen == SCREEN_GAME) && ((client.time - client.ui.combatActionLastTime) < 30) && (client.ui.combatActionLastTime != 0.0f);
	if(action){
		client.audio.music[0].active = false;
		client.audio.music[1].active = true;
	}else if((client.screen == SCREEN_GAME) && (game.state != STATE_ENDED)){
		client.audio.music[0].active = true;
		client.audio.music[1].active = false;
	}else{
		client.audio.music[0].active = false;
		client.audio.music[1].active = false;
		attenuation = 0.5;
	}
	t = SmoothOver(0.9, 0.5, attenuation * client.deltaTime);

	for(int i=0; i < 2; i++){
		struct GameMusic_st* mus = &client.audio.music[i];
		if(mus->active){
			if(!mus->handle){
				mus->handle = MyPlayMusic(mus->music, mus->mix * app.config.musicVolume, 0);
			}
			if(mus->mix < 0.99){
				mus->mix = mus->mix + (1 - mus->mix) * t;
			}
		}else{
			if(mus->mix > 0.01){
				mus->mix = mus->mix - mus->mix * t;
			}else{
				Soloud_stop(client.audio.soloud, mus->handle);
				mus->handle = 0;
			}
		}
		Soloud_setVolume(client.audio.soloud, mus->handle, mus->mix * app.config.musicVolume);
	}

}

void UpdateAudio(){
	Soloud_set3dListenerAt(client.audio.soloud, client.cam.target.x, 0, client.cam.target.y / app.config.persp_y);
	Soloud_set3dListenerUp(client.audio.soloud, 0, 1, 0);

	UpdateGameMusic();
}

#undef IMP_AUDIO_H
#endif
