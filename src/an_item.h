#pragma once
#include "transforms.h"
#include "physics.h"
#include "stdbool.h"

typedef enum{
    ITEM_SHOTGUN,
    _ITEM_TYPE_COUNT
} ItemType_e;

typedef struct{
    char name[32];
    int cooldown;
    int preparation;
    bool isGun;
    struct{
        int ammo;
        int bulletsPerShot;
        float damage;
        float distance;
        float spread;
        float knockbackEnemy;
        float knockbackSelf;
    } gun;
    struct{
        int icon;
        int ammo;
        int hold;
        int aimed;
        #define ITEMCLASS_SPRITES_PER_SHEET 4
    } sprite;
} ItemClass_t;

extern ItemClass_t itemClasses[_ITEM_TYPE_COUNT];

typedef struct{
    bool active;
    ItemType_e type;
    int ammo;
    int cooldown;
    int heldByToyId;
    int retainedOwnerId;
    Transform_t tform;
    int sector[2];
    Body_t* body;
    Shape_t* shape;
    Shape_t* sensor;
    ShapeData_t shapeData;
    ShapeData_t sensorData;
} Item_t;

int SpawnItem(ItemType_e type, int toyId, Vector3 pos);
void RemoveItem(int itemId);
void SetItemOwner(int itemId, int toyId);
bool UseItem(int itemId, Vector3 direction);
void UpdateItems();
void DrawItems();
