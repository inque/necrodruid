#pragma once
#include "physics.h"
#include "raylib.h"
#include "raymath.h"
#include "stdbool.h"

#define LV_MAX_TILECOUNT 96
#define LV_TILESIZE 128.0
#define LV_TERRAINHEIGHT 128.0
#define LV_TERRAINTEXROWS 4

#define LV_SPRITESHEETS 4
#define LV_MAX_LAYERS 16
#define LV_MAX_BRUSHES 1024
#define LV_BRUSH_VERTS 16
#define LV_MAX_ENTITIES 512
#define LV_VARIABLES 32
#define LV_SCRIPTS 32

typedef enum {
	BRUSH_PHYSICAL = 1
	//BRUSH_RANDOM_FRAME
} LvBrushFlags_e;

typedef struct{
	LvBrushFlags_e flags;
	int spriteId;
	int spriteFrame;
	Color tint;
	Vector3 pos;
	float size;
	float z1, z2;
	int sector[2];
	float bbox[4];
	// int vertCount;
	// Vector2 verts[LV_BRUSH_VERTS];
	// int triangleCount;
	// int triangleIndices[LV_BRUSH_VERTS * 3];
	// Color fillColor;
	// cpBody* body;
	cpShape* shape;
	// Transform_t tform;
} LvBrush_t;

typedef struct{
	int brushStartId;
	int brushCount;
	bool visible;
	int order;
	int blendMode;
	char name[24];
} LvLayer_t;

typedef enum{
	LVENT_NONE,
	LVENT_PLAYERSPAWN,
	LVENT_ROBOTSPAWN,
	_LVENT_COUNT
} LvEntity_e;

typedef struct{
	LvEntity_e type;
	Vector3 pos;
	union{
		struct{
			bool used;
		} spawn;
		struct{
			char name[48];
			int toyType;
			Color color;
			int healthMult;
			int team;
			bool immortal;
			bool fatalizer;
			bool automated;
			bool leaderOnly;
		} robot;
	};
} LvEntity_t;

typedef struct{
	char name[16];
	int val;
} LvVariable_t;

typedef enum{
	LVCOND_NONE,
	LVCOND_VAR_EQ,
	LVCOND_TRIGGER
} LvScriptCondition_e;

typedef enum{
	LVEXP_CHAT,
	LVEXP_CUTSCENE
} LvScriptAction_e;

typedef struct{
	struct{
		LvScriptCondition_e type;
		union{
			struct {
				char name[16];
				int value;
			} var;
			struct{
				Vector3 pos;
				float radius;
			} point;
		};
	} condition;
	struct{
		LvScriptAction_e type;
		union{
			char chat[64];
			struct{
				char text[128];
				bool global;
			} cutscene;
		};
	} action;
	bool isUsed;
	bool multiUse;
	char comment[12];
} LvScriptLine_t;

typedef struct{
	char filePath[64];
	Color bgColor;
	int mapSize[2]; //in tiles
	unsigned char terrainHeight[LV_MAX_TILECOUNT][LV_MAX_TILECOUNT];
	unsigned char terrainColor[LV_MAX_TILECOUNT][LV_MAX_TILECOUNT];
	cpShape* bounds[4];
	int sheetCount;
	int sheets[LV_SPRITESHEETS];
	int layerCount;
	LvLayer_t layers[LV_MAX_LAYERS];
	int brushCount;
	LvBrush_t brushes[LV_MAX_BRUSHES];
	int entityCount;
	LvEntity_t entities[LV_MAX_ENTITIES];
	int varCount;
	LvVariable_t vars[LV_VARIABLES];
	int scriptCount;
	LvScriptLine_t script[LV_SCRIPTS];
} Level_t;

float TerrainHeightAt(Vector2 point);
bool LoadLevel(const char* path);
void ClearLevel();
void UpdateLevel();
void DrawLevelTerrain();
void DrawLevel(int minOrder, int maxOrder);

static inline bool CoordOutOfBounds(int tile[2], Level_t* level){
	return (
	     (tile[0] < 0) || (tile[0] > level->mapSize[0])
	  || (tile[1] < 0) || (tile[1] > level->mapSize[1])
	);
}

