#ifndef EDITOR_SPRITES_H

void SpriteEditor();

#define EDITOR_SPRITES_H
#endif
#ifdef IMP_EDITOR_SPRITES_H
//==== implementation starts here ====
#include "gameapp.h"
#include "utils.h"
#include "raymath.h"
#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"


typedef struct{
	int sheetId;
	int spriteId;
	int frameNum;
	int vertId;
	bool editSheetPath;
	bool editTexname;
	bool editSprName;
	int editSprNameId;
	bool editSprOpen;
	Camera2D texViewCam,sprViewCam;
	float sprViewWidth;
	Vector2 mouseLast;
} SpriteEditorState_t;

static SpriteEditorState_t editor = {
	.sheetId = -1,
	.spriteId = -1,
	.vertId = -1,
	.texViewCam = {.zoom = 1, .target = {512,512}},
	.sprViewCam = {.zoom = 1, .target = {-512,512}},
	//.sprViewCam = {.zoom = 1},
	.sprViewWidth = 500
};

static bool IsMouseInRectWorld(Rectangle bounds, Camera2D cam){
	Vector2 mousePos = GetScreenToWorld2D(GetMousePosition(), cam);
	if(mousePos.x < bounds.x) return false;
	if(mousePos.y < bounds.y) return false;
	if(mousePos.x > (bounds.x + bounds.width)) return false;
	if(mousePos.y > (bounds.y + bounds.height)) return false;
	return true;
}

static void CamControls(Camera2D* cam, Rectangle bounds){
	if(!IsMouseInRect(bounds)) return;

	cam->zoom *= 1 + 0.025f * (float)GetMouseWheelMove();
	if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON)){
		Vector2 mouseDelta = Vector2Subtract(GetMousePosition(), editor.mouseLast);
		cam->target = Vector2Add(cam->target, Vector2Scale(mouseDelta, -cam->zoom));
	}
}

static void ExportSPR(int sheetId, const char* path){
	SpriteSheet_t* sheet = &client.sheets[sheetId];
	int spriteCount = 0;
	
	FILE* file = fopen(path, "wb");
	if(!file) return;

	for(int i=0; i < MAX_SPRITES; i++){
		Sprite_t* sprite = &client.sprites[i];
		if(!sprite->active) continue;
		if(sprite->sheetId != sheetId) continue;
		spriteCount++;
	}
	
	uint32_t data[4];
	data[0] = CHR4('n','S','P','R');
	data[1] = 100;
	data[2] = 8 + sizeof(sheet->texname); //block length
	data[3] = spriteCount;
	fwrite(data, 4, 4, file);
	fwrite(sheet->texname, 1, sizeof(sheet->texname), file);

	for(int spriteId=0; spriteId < MAX_SPRITES; spriteId++){
		Sprite_t* sprite = &client.sprites[spriteId];
		if(!sprite->active) continue;
		if(sprite->sheetId != sheetId) continue;
		
		data[0] = 8 + sizeof(sprite->name); //block length
		fwrite(data, 4, 1, file);
		fwrite(sprite->name, 1, sizeof(sprite->name), file);
		fwrite(&sprite->framesUsed, 4, 1, file);
		for(int i=0; i < sprite->framesUsed; i++){
			SpriteShape_t* shape = &client.shapes[sprite->shapeId[i]];
			data[0] = 4+4*(2+1+2*shape->vertCount); //block length
			fwrite(data, 4, 1, file);
			fwrite(&shape->uv, 4, 2, file);
			fwrite(&shape->vertCount, 4, 1, file);
			fwrite(&shape->verts, 4, 2*shape->vertCount, file);
		}
	}
	fclose(file);
}

void SpriteEditor(){
	char text[4096];
	bool noedit = !(editor.editTexname || editor.editSprName || editor.editSheetPath);
	//Rectangle texView = {0,0,(client.width-200)/2,client.height};
	Rectangle texView = {0,0,client.width-editor.sprViewWidth,client.height};
	Rectangle sprView = {texView.width,0,(client.width-texView.width),client.height};
	bool texViewActive = IsMouseInRect(texView);
	bool sprViewActive = IsMouseInRect(sprView);
	Vector2 mouseDelta = Vector2Subtract(GetMousePosition(), editor.mouseLast);

	editor.texViewCam.offset = (Vector2){texView.width/2, texView.height/2};
	editor.sprViewCam.offset = (Vector2){sprView.width/2, sprView.height/2};
	if(IsKeyDown(KEY_O)){
		editor.sprViewWidth -= mouseDelta.x;
		if(editor.sprViewWidth < 50) editor.sprViewWidth = 50;
		if(editor.sprViewWidth > (client.halfWidth+200)) editor.sprViewWidth = client.halfWidth+200;
	}

	//-- editor

	//texture view
	DrawRectangleRec(texView, LIGHTGRAY);
	BeginScissorMode(0, 0, texView.width, texView.height);
	CamControls(&editor.texViewCam, texView);
	BeginMode2D(editor.texViewCam);
	if(editor.sheetId != -1){
		SpriteSheet_t* sheet = &client.sheets[editor.sheetId];
		if(sheet->tex.id){
			DrawTexture(sheet->tex, 0, 0, Fade(WHITE, 0.5));
		}
		for(int spriteId=0; spriteId < MAX_SPRITES; spriteId++){
			Sprite_t* sprite = &client.sprites[spriteId];
			if(!sprite->active) continue;
			if(sprite->sheetId != editor.sheetId) continue;
			for(int i=0; i < sprite->framesUsed; i++){
				SpriteShape_t* shape = &client.shapes[sprite->shapeId[i]];
				bool sameSprite = (spriteId == editor.spriteId);
				bool selected = sameSprite && (i == editor.frameNum);
				if(sameSprite){
					DrawLine(shape->uv.x, shape->uv.y-10, shape->uv.x, shape->uv.y+10, BLACK);
					DrawLine(shape->uv.x-10, shape->uv.y, shape->uv.x+10, shape->uv.y, BLACK);
				}
				for(int i2=0; i2 < shape->vertCount; i2++){
					Vector2 pos = Vector2Add(shape->uv, shape->verts[i2]);
					Vector2 posLast = Vector2Add(shape->uv, shape->verts[ mod2(i2-1, shape->vertCount) ]);
					Rectangle rec = {pos.x, pos.y, 10, 10};
					DrawLineEx(posLast, pos, 2, selected ? Fade(BLUE, 0.5) : Fade(BLACK, 0.3));
					DrawRectangleRec(rec, (selected ? ((editor.vertId == i2) ? RED : WHITE) : Fade(WHITE, 0.5)) );
					if(i2==(shape->vertCount-1)) DrawRectangleLinesEx(rec, 1, Fade(BLACK, 0.4));

					if(texViewActive && IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
						if(IsMouseInRectWorld((Rectangle){pos.x-5, pos.y-5, 20, 20}, editor.texViewCam)){
							if(selected){
								editor.vertId = i2;
							}else{
								editor.spriteId = spriteId;
								editor.frameNum = i;
								editor.vertId = -1;
							}
						}else if(selected && (editor.vertId == i2)){
							editor.vertId = -1;
						}
					}
					if(selected && ((editor.vertId == i2) || IsKeyDown(KEY_LEFT_CONTROL))){
						if(noedit && IsKeyDown(KEY_G)){
							shape->verts[editor.vertId] = Vector2Add(shape->verts[editor.vertId], Vector2Scale(mouseDelta, editor.texViewCam.zoom));
						}
					}
				}

				if(selected && texViewActive){
					if(noedit && IsKeyPressed(KEY_V) && (shape->vertCount < MAX_SHAPE_VERTS)){
						Vector2 cur = GetScreenToWorld2D(GetMousePosition(), editor.texViewCam);
						shape->verts[shape->vertCount++] = Vector2Subtract(cur, shape->uv);
						shape->indicesUsed = 0;
					}
					if(noedit && IsKeyPressed(KEY_P)){
						Vector2 oldUV = shape->uv;
						Vector2 newUV = GetScreenToWorld2D(GetMousePosition(), editor.texViewCam);
						Vector2 diff = Vector2Subtract(oldUV, newUV);
						shape->uv = newUV;
						for(int i2=0; i2 < shape->vertCount; i2++){
							shape->verts[i2] = Vector2Add(shape->verts[i2], diff);
						}

						shape->indicesUsed = 0; //hack
					}
				}
			}
		}
	}
	EndMode2D();
	EndScissorMode();

	//sprite view
	DrawRectangleRec(sprView, RAYWHITE);
	BeginScissorMode(sprView.x,sprView.y,sprView.width,sprView.height);
	CamControls(&editor.sprViewCam, sprView);
	BeginMode2D(editor.sprViewCam);
	BeginDrawingSprites();

	for(int ix=-5; ix < 5; ix++)
	for(int iy=-5; iy < 5; iy++){
		DrawRectangleLines(ix*50, iy*50, 50, 50, LIGHTGRAY);
	}
	DrawRectangleLines(-50, -50, 100, 100, GREEN);
	if(editor.spriteId != -1){
		DrawSprite(editor.spriteId, editor.frameNum, Vector3Zero(), WHITE, 1);
	}

	EndDrawingSprites();
	EndMode2D();
	EndScissorMode();

	//-- right panel

	Rectangle bnd = {client.width-195, 30, 190, 24};
	GuiWindowBox( (Rectangle){client.width-200, 0, 200, client.height}, "Sprite editor");

	//sheets

	GuiLabel(bnd, "Spritesheets");
	bnd.y += 24;
	for(int i=0; i < MAX_SHEETS; i++){
		SpriteSheet_t* sheet = &client.sheets[i];
		if(!sheet->active) continue;

		if(GuiToggle(bnd, sheet->spr_path[0] ? sheet->spr_path : "Untitled*", editor.sheetId == i)){
			if(editor.sheetId != i){
				editor.sheetId = i;
				editor.spriteId = -1;
				editor.frameNum = 0;
				editor.vertId = -1;
			}
		}
		bnd.y += 24;
	}
	if(GuiButton((Rectangle){bnd.x, bnd.y, 32, 24}, "+")){
		for(int i=0; i < MAX_SHEETS; i++){
			SpriteSheet_t* sheet = &client.sheets[i];
			if(sheet->active) continue;

			sheet->active = 1;
			sheet->spr_path[0] = 0;
			sheet->texname[0] = 0;
			sheet->tex.id = 0;
			editor.sheetId = i;
			break;
		}
	}
	if(GuiButton((Rectangle){bnd.x+32+4, bnd.y, 32, 24}, "-")){
		if(editor.sheetId != -1){
			RemoveSheet(editor.sheetId);
			editor.sheetId = -1;
		}
	}
	if(editor.sheetId != -1){
		SpriteSheet_t* sheet = &client.sheets[editor.sheetId];
		if(GuiTextBox( (Rectangle){bnd.x+(32+4)*2, bnd.y, 150, 24}, sheet->texname, 24, editor.editTexname)){
			if(editor.editTexname){
				ReloadSheet(editor.sheetId);
			}
			editor.editTexname = !editor.editTexname;
		}
		bnd.y += 24;
		if((noedit && IsKeyDown(KEY_LEFT_CONTROL) && IsKeyPressed(KEY_S)) ||
		   (GuiButton((Rectangle){bnd.x, bnd.y, 64, 24}, "Save"))){
			if(sheet->spr_path[0] == 0){
				editor.editSheetPath = true;
				editor.editSprOpen = false;
			}else{
				ExportSPR(editor.sheetId, sheet->spr_path);
			}
		}
		if((noedit && IsKeyDown(KEY_LEFT_CONTROL) && IsKeyPressed(KEY_O)) ||
		   (GuiButton((Rectangle){bnd.x+56, bnd.y, 64, 24}, "Open"))){
				editor.editSheetPath = true;
				editor.editSprOpen = true;
		}
		if(editor.editSheetPath && GuiTextBox( (Rectangle){bnd.x, bnd.y, 196, 24}, sheet->spr_path, sizeof(sheet->spr_path), editor.editSheetPath)){
			if(editor.editSheetPath){
				if(editor.editSprOpen){
					RemoveSheet(editor.sheetId);
					editor.sheetId = LoadSpriteSheet(sheet->spr_path, NULL);
				}else{
					ExportSPR(editor.sheetId, sheet->spr_path);
				}
				editor.editSheetPath = false;
			}
		}
	}
	bnd.y += 32;

	//sprites
	if(editor.sheetId != -1){
		static Vector2 scroll;
		static int spriteCount;
		Rectangle scrollPanel = {bnd.x, bnd.y, bnd.width, 400};
		Rectangle content = {bnd.x, bnd.y+24, bnd.width-20, 24 * spriteCount};
		Rectangle lineBnd = {bnd.x, scrollPanel.y + scroll.y, bnd.width-20, 24};

		GuiLabel(bnd, "Sprites");
		bnd.y += 24;
		
		GuiScrollPanel(scrollPanel, content, &scroll);

		BeginScissorMode(scrollPanel.x, scrollPanel.y, scrollPanel.width, scrollPanel.height);
		spriteCount=0;
		for(int i=0; i < MAX_SPRITES; i++){
			Sprite_t* sprite = &client.sprites[i];
			if(!sprite->active) continue;
			if(sprite->sheetId != editor.sheetId) continue;

			if(GuiToggle(lineBnd, sprite->name[0] ? sprite->name : "Unnamed*", i == editor.spriteId)){
				if(editor.spriteId != i){
					editor.spriteId = i;
					editor.frameNum = 0;
					editor.vertId = -1;
				}
			}
			lineBnd.y += 24;
			spriteCount++;
		}
		EndScissorMode();
		bnd.y += scrollPanel.height;

		if(GuiButton((Rectangle){bnd.x, bnd.y, 32, 24}, "+")){
			//shift right
			if(editor.spriteId != -1){
				editor.spriteId++;
				for(int spriteId=MAX_SPRITES-1; spriteId > editor.spriteId; spriteId--){
					if(!client.sprites[spriteId-1].active) continue;
					client.sprites[spriteId] = client.sprites[spriteId-1];
				}
			}
			{
				int spriteId = (editor.spriteId == -1) ? 0 : editor.spriteId;
				Sprite_t* sprite = &client.sprites[spriteId];

				sprite->active = 1;
				//sprite->name[0] = 0;
				sprintf_s(sprite->name, sizeof(sprite->name), "S_%i", GetRandomValue(0,333000999));
				sprite->sheetId = editor.sheetId;
				sprite->framesUsed = 0;
				for(int i2=0; i2 < MAX_SHAPES; i2++){
					SpriteShape_t* shape = &client.shapes[i2];
					if(shape->active) continue;

					shape->active = 1;
					shape->vertCount = 0;
					shape->indicesUsed = 0;
					shape->uv = Vector2Zero();
					sprite->shapeId[sprite->framesUsed++] = i2;
					break;
				}
				//editor.spriteId = spriteId;
				editor.frameNum = 0;
			}
		}
		if(editor.spriteId != -1){
			Sprite_t* sprite = &client.sprites[editor.spriteId];
			if(GuiButton((Rectangle){bnd.x+32+4, bnd.y, 32, 24}, "-")){
				for(int i=0; i < sprite->framesUsed; i++){
					client.shapes[sprite->shapeId[i]].active = 0;
				}
				sprite->active = 0;
				// if(client.sprites[editor.spriteId-1].active){
				// 	editor.spriteId--;
				// }else{
					editor.spriteId = -1;
				// }
			}
			if(GuiButton((Rectangle){bnd.x+(32+4)*2, bnd.y, 32, 24}, "R")){
				editor.editSprName = true;
				editor.editSprNameId = editor.spriteId;
			}
			if(GuiButton((Rectangle){bnd.x+(32+4)*3, bnd.y, 32, 24}, "<")){
				if(editor.spriteId > 0){
					Sprite_t tmp = client.sprites[editor.spriteId-1];
					client.sprites[editor.spriteId-1] = client.sprites[editor.spriteId];
					client.sprites[editor.spriteId] = tmp;
					editor.spriteId--;
				}
			}
			if(GuiButton((Rectangle){bnd.x+(32+4)*4, bnd.y, 32, 24}, ">")){
				if(editor.spriteId < MAX_SPRITES){
					Sprite_t tmp = client.sprites[editor.spriteId+1];
					client.sprites[editor.spriteId+1] = client.sprites[editor.spriteId];
					client.sprites[editor.spriteId] = tmp;
					editor.spriteId++;
				}
			}
			if(editor.editSprName){
				if(GuiTextBox((Rectangle){bnd.x+(32+4)*2, bnd.y, 150, 24}, client.sprites[editor.editSprNameId].name, sizeof(sprite->name), true)){
					editor.editSprName = false;
				}
			}
		}
		bnd.y += 24;
	}
	bnd.y += 32;

	//frames
	if(editor.spriteId != -1){
		Sprite_t* sprite = &client.sprites[editor.spriteId];
		GuiLabel(bnd, "Frames");
		bnd.y += 24;

		int x = 0;
		for(int i=0; i < sprite->framesUsed; i++){
			SpriteShape_t* shape = &client.shapes[sprite->shapeId[i]];
			if(!shape->active){
				GuiLabel(bnd, "FRAME ERROR");
				continue;
			}
			if(x > 170){
				x = 0;
				bnd.y += 26;
			}
			sprintf(text, "%i", i);
			if(GuiToggle((Rectangle){bnd.x+x, bnd.y, 32, 24}, text, editor.frameNum == i)){
				if(editor.frameNum != i){
					editor.frameNum = i;
					//...
				}
			}
			x += 36;
		}
		bnd.y += 24;
		if(GuiButton((Rectangle){bnd.x, bnd.y, 32,24}, "+") && (sprite->framesUsed+1 < MAX_SPRITE_FRAMES)){
			for(int i2=0; i2 < MAX_SHAPES; i2++){
				SpriteShape_t* shape = &client.shapes[i2];
				if(shape->active) continue;

				shape->active = 1;
				shape->vertCount = 0;
				shape->indicesUsed = 0;
				shape->uv = Vector2Zero();
				sprite->shapeId[sprite->framesUsed] = i2;
				editor.frameNum = sprite->framesUsed++;
				break;
			}
		}
		if((sprite->framesUsed>1) && GuiButton((Rectangle){bnd.x+32+4, bnd.y, 32,24}, "-")){
			int toMove = sprite->framesUsed - editor.frameNum - 1;
			if(toMove > 0){
				memmove(&sprite->shapeId[editor.frameNum], &sprite->shapeId[editor.frameNum+1], toMove*sizeof(int));
			}
			sprite->framesUsed--;
			editor.frameNum--;
		}

		if(noedit && IsKeyPressed(KEY_COMMA)){
			editor.frameNum = mod2(editor.frameNum-1, sprite->framesUsed);
		}
		if(noedit && IsKeyPressed(KEY_PERIOD)){
			editor.frameNum = (editor.frameNum+1) % sprite->framesUsed;
		}

	}

	editor.mouseLast = GetMousePosition();
}



#undef IMP_EDITOR_SPRITES_H
#endif
