#ifndef GAMEINTERFACE_H

void DrawInterfaceWorld();
void DrawInterfaceScreen();
void ShadeScreen();
void DrawCutscene();
void LobbyScreen();

#define GAMEINTERFACE_H
#endif
#ifdef IMP_GAMEINTERFACE_H
//==== implementation starts here ====

#include "gameapp.h"
#include "fogofwar.h"

void DrawInterfaceWorld(){
	static bool cursorHiddenLast;
	bool showCursor = client.mouseOverUI || client.paused || (game.state == STATE_LOBBY);
	bool seenEnemies = false;
	char text[1024];
	Boy_t* player = GetBoy(client.playerBoyId);
	if(client.playerBoyId == -1) return;

	if(showCursor){
		if(cursorHiddenLast){
			ShowCursor();
		}
		cursorHiddenLast = false;
	}else{
		if(!cursorHiddenLast){
			HideCursor();
		}
		cursorHiddenLast = true;
	}

	if((!showCursor) && (player->toyId != -1)){
		Vector2 mouse = GetScreenToWorld2D(GetMousePosition(), client.cam);
		BlitSprite(client.assets.sprite.aimCursor, 0, mouse, Fade(WHITE, 0.8), 1);
	}

	forEachToy(toy, toyId){
		ToyClass_t* toyClass = GetToyClass(toy->type);
		Vector2 pos = GetPos2d(&toy->limbs[0].tform);
		if(!CanSeeToy(client.playerBoyId, toyId)) continue;
		if(!IsVisible(&toy->limbs[0].tform)) continue;

		if(player->toyId == toyId){
			//local player

			if(toy->walk.fallenTimer){
				//float t = (float)toy->walk.fallenTimer / (10 * TICKS_PER_SEC);
				//DrawCircleSector(Vector2Add(pos, (Vector2){25, -50}), 6, 0, 360*MAX(0, MIN(1-t, 1)), 24, Fade(LIGHTGRAY, 0.85));
				float t = ((toy->walk.fallenTimer % TICKS_PER_SEC) / (float)TICKS_PER_SEC);
				float fontSize = 25+5*(t*t);
				int textPos[] = {pos.x+25-fontSize/2, pos.y-50-fontSize/2};
				sprintf(text, "%i.", 1 + toy->walk.fallenTimer / TICKS_PER_SEC);
				DrawText(text, textPos[0]+2, textPos[1]+2, fontSize, Fade(BLACK, 0.65));
				DrawText(text, textPos[0], textPos[1], fontSize, RAYWHITE);
			}
			if(toy->item.holdingItemId != -1){
				Item_t* item = GetItem(toy->item.holdingItemId);
				ItemClass_t* itemClass = GetItemClass(item->type);
				if(itemClass->isGun){
					int textPos[] = {pos.x-30, pos.y - 150};
					Color color = item->ammo ? (Color){255,0,128,255} : LIGHTGRAY;
					sprintf(text, "%i", item->ammo);
					DrawText(text, textPos[0]+1, textPos[1]+1, 20, BLACK);
					DrawText(text, textPos[0], textPos[1], 20, color);
					BlitSprite(itemClass->sprite.ammo, 0, (Vector2){textPos[0]-10, textPos[1]+12}, WHITE, 0.3);
				}
			}
			if(toy->item.lookingAtItemId != -1){
				Item_t* item = GetItem(toy->item.lookingAtItemId);
				Vector2 pos = Project2d(GetPos(&item->tform));
				float t = 10 * sinf(3.14*client.time);
				DrawText("[E]", pos.x, pos.y - 50 + t, 20, ((game.frame>>3)&1) ? BLUE : WHITE);
			}
		}else{
			if((toy->boyId != -1) && (toy->health > 0)){
				Boy_t* boy = GetBoy(toy->boyId);
				BlitSprite(client.assets.sprite.insignia, 0, (Vector2){pos.x-10, pos.y-toyClass->height-70}, boy->appearance.color, 0.333);

				if(boy->team != player->team){
					if(player->toyId != -1){
						Toy_t* ourToy = GetToy(player->toyId);
						float dist = Vector3Distance(PeekPos(&ourToy->limbs[0].tform), PeekPos(&toy->limbs[0].tform));
						if((dist > 0) && (dist < 700)){
							client.ui.combatActionLastTime = client.time;
						}
					}
				}
			}
		}

		// health bars
		if(toy->health < toy->maxHealth){
			int elapsedSinceTakenDamage = game.frame - toy->lastTakenDamageFrame;
			bool enlarged = (elapsedSinceTakenDamage < 80);
			float t = ((float)toy->health / toy->maxHealth);
			bool hidden = (
				(enlarged && (elapsedSinceTakenDamage > 60) && ((game.frame>>1)&1))
				// || ( (!enlarged) && (toy->health <= 0) )
				|| (toy->health <= 0)
				|| (toy->walk.fallenTimer && (toy->health < 10))
				|| ( (!toy->lastTakenDamageFrame) && (t > 0.8) )
			);
			int width = enlarged ? 60 : 30;
			Color bgColor = DARKGRAY;
			Color fgColor = {(1-powf(t, 3))*254, (1-powf(1-t, 2))*254, 30, 255};

			if(!hidden){
				if(!toy->lastAnimatedHealthValue){
					toy->lastAnimatedHealthValue = toy->health;
				}else{
					int diff = toy->health - toy->lastAnimatedHealthValue;
					if(diff != 0){
						diff = (int)ceilf((float)diff / 20.0f);
						toy->lastAnimatedHealthValue += diff;
					}
				}
				DrawRectangle(pos.x-width/2, pos.y-toyClass->height-30-30, width, (enlarged ? 6 : 4), bgColor);
				DrawRectangle(pos.x-width/2, pos.y-toyClass->height-30-29, t*(width+6), (enlarged ? 4 : 2), fgColor);
			}
		}

	}

}

static void DrawTimer(){
	char text[2048];
	int fontSize = 24;
	Color color = (Color){157,223,255,255};
	int ticksPerMin = 60*TICKS_PER_SEC;
	int minutes = (game.timer / ticksPerMin);
	int seconds = (game.timer - (minutes * ticksPerMin)) / TICKS_PER_SEC;
	int micro = (int)((float)(game.timer % TICKS_PER_SEC) / (float)TICKS_PER_SEC * 10.0f);
	int textpos[] = {(client.halfWidth - fontSize*2), 16};
	sprintf(text, "%02i:%02i.%i", minutes, seconds, micro);
	DrawText(text, textpos[0]+1, textpos[1]-1, fontSize, Fade(DARKGRAY, 0.7));
	DrawText(text, textpos[0], textpos[1], fontSize, color);
}

static void BattleReadyScreen(){
	float lastPlayerHeight;
	Rectangle bounds = {client.halfWidth-200, 0, 400, client.height};
	MenuBackground();
	DrawRectangleRec(bounds, Fade(BLACK, 0.2));
	Extend(&bounds, -20);
	CutTop(&bounds, client.height/4);

	forEachBoy(boy, boyId){
		Rectangle line = CutTop(&bounds, 25);
		DrawText(boy->name, line.x, line.y, 25, Fade(boy->appearance.color, boy->ready ? 1.0 : 0.5));
		CutTop(&bounds, 2);
		lastPlayerHeight = line.y + line.height;
	}

	{
		int fontSize = 20;
		Rectangle textZone = CutBottom(&bounds, 300);
		const char* text = (
			"Battle mode.\n"
			"Press R or Fire to ready up."
		);
		if(textZone.y < lastPlayerHeight){
			textZone.x = 10;
			textZone.y = 150;
			fontSize = 15;
		}
		DrawText(text, textZone.x+1, textZone.y-1, fontSize, BLACK);
		DrawText(text, textZone.x, textZone.y, fontSize, GREEN);
	}

}

void LobbyScreen(){
	switch(game.mode){
		case MODE_BATTLE:
			BattleReadyScreen();
			break;
	}
}

void DrawInterfaceScreen(){
	char text[1024];
	Boy_t* boy = GetBoy(client.playerBoyId);
	Toy_t* toy = GetToy(boy->toyId);
	bool canUseCards = (boy->toyId != -1) && (!toy->useCard.cooldown);
	Rectangle bounds = FsRect();
	Rectangle bottom = CutBottom(&bounds, 100);
	Rectangle deck = {bottom.x+bottom.width/2-250, bottom.y-15, 500, 130};
	Vector2 cardPos = {deck.x + deck.width/5, deck.y};
	int scrollCard = (
		(int)GetMouseWheelMove()
		+ (IsKeyPressed(KEY_Y) ? (IsKeyDown(KEY_LEFT_SHIFT) ? -1 : 1) : 0)
		+ (
			  IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_LEFT_TRIGGER_1)?-1:0
			+ IsGamepadButtonPressed(app.config.gamepad, GAMEPAD_BUTTON_RIGHT_TRIGGER_1)?1:0
		)
	);
	client.mouseOverUI = false;

	if(boy->toyId != -1){
		if(toy->animation.flash){
			int elapsed = game.frame - toy->animation.lastChangeFrame;
			float t = MAX(0, MIN(1, (float)elapsed / MAX(0.1, toy->animation.duration)));
			float c = fabsf(t-0.5);
			float height = MAX(140, client.height/12) + 150*t;
			Color color = Fade(WHITE, 0.5*(1-t));
			Color trans = {0};
			DrawRectangleGradientV(0, 0, client.width, height, color, trans);
			DrawRectangleGradientV(0, client.height-height, client.width, height, trans, color);
			client.camShakeZoom = c*(1-t)*0.2;
		}
	}

	if(game.mode != MODE_STORY){
		DrawTimer();
	}

	DrawRectangleGradientV(bottom.x, bottom.y, bottom.width, bottom.height, BROWN, DARKBROWN);

	if(boy->toyId != -1){
		ToyClass_t* toyClass = GetToyClass(toy->type);
		Vector2 avatarPos = {bottom.width/5, bottom.y+50};
		
		BlitSprite(client.assets.sprite.insignia, 0, (Vector2){avatarPos.x-60, avatarPos.y-45}, boy->appearance.color, 2.0);
		BlitSprite(toyClass->sprite.avatar, 0, avatarPos, WHITE, 1);

		if(toy->fatality.canPerform && (!toy->fatality.timer)){
			Toy_t* victim = GetToy(toy->fatality.toyId);
			ToyClass_t* victimClass = GetToyClass(victim->type);
			const char* label = victimClass->female ? "Finish her!" : "Finish him!";
			float t = powf(sinf(3.14*client.time), 2);
			int textWidth = MeasureText(label, 40);
			Vector2 textPos = {
				client.halfWidth-textWidth/2,
				client.halfHeight-220+t*40
			};
			DrawRectangleGradientV(textPos.x, client.halfHeight-200-60, textWidth, 60, Fade(WHITE, 0), Fade(WHITE, 0.7));
			DrawRectangleGradientV(textPos.x, client.halfHeight-200, textWidth, 60, Fade(WHITE, 0.7), Fade(WHITE, 0));
			if((game.frame>>2)&1){
				DrawText(label, textPos.x+2, textPos.y+2, 40, BLACK);
				DrawText(label, textPos.x, textPos.y, 40, RED);
			}
		}
	}

	int cardNum = 0;
	int cardSelectedId = -1;
	for(int i=0; i < BOY_MAX_CARDS; i++){
		if(!boy->cards[i].active) continue;
		if(cardNum++ == client.ui.cardSelected){
			cardSelectedId = i;
		}
	}
	if(cardNum){
		client.ui.cardSelected = mod2(client.ui.cardSelected + scrollCard, cardNum);
	}

	int cardNumMax = cardNum-1;
	cardNum = 0;
	for(int i=0; i < BOY_MAX_CARDS; i++){
		PlayCard_t* card = &boy->cards[i];
		if(!card->active) continue;
		bool selected = canUseCards && (i == client.ui.cardSelected);
		Vector2 pos = {cardPos.x, cardPos.y - (canUseCards ? (selected ? 50 : 0) : -40)};
		bool lastOne = (cardNum == cardNumMax);
		bool highlighted = canUseCards && IsMouseInRect((Rectangle){pos.x-50, pos.y-100, lastOne ? 100 : 44, selected?150:100});

		
		if(highlighted){
			client.mouseOverUI = true;
			if(client.ui.cardLastHighlightedNum != cardNum){
				client.ui.cardLastHighlightedNum = cardNum;
				client.ui.cardLastHighlightedTime = client.time;
			}else{
				float t = MIN(0.75, client.time - client.ui.cardLastHighlightedTime);
				pos.y += 10 * (t*t) * sinf(5.0*client.time);
			}
			if(IsMouseButtonPressed(MOUSE_LEFT_BUTTON)){
				boy->inputs.action.type = BOY_ACTION_USECARD;
				boy->inputs.action.useCard.num = cardNum;
				boy->inputs.buttons ^= BTN_FIRE;
				boy->inputs.buttonsPrev ^= BTN_FIRE;
			}
		}
		
		DrawCard(card, pos, canUseCards ? WHITE : LIGHTGRAY);
		sprintf(text, "%i.", cardNum+1);
		DrawText(text, pos.x - 50, pos.y - 15, highlighted ? 15 : 10, highlighted ? RED : BLACK);

		cardPos.x += 45;
		cardNum++;
	}

	if((!canUseCards) && (cardNumMax != -1)){
		if(boy->toyId != -1){
			sprintf(text, "%2i.", toy->useCard.cooldown / TICKS_PER_SEC);
			DrawText(text, deck.x+deck.width/3+2, deck.y-40+2, 25, BLACK);
			DrawText(text, deck.x+deck.width/3, deck.y-40, 25, WHITE);
		}
	}
	
	DrawRectangleGradientH(deck.x, deck.y, deck.width, deck.height, BROWN, DARKBROWN);
}

void ShadeScreen(){
	if(game.state == STATE_LOBBY) return;
	// Boy_t* boy = GetBoy(client.playerBoyId);
	// if(client.playerBoyId != -1){
	// 	Toy_t* toy = GetToy(boy->toyId);
	// 	if(boy->toyId != -1){
	// 	}
	// }
	DrawRectangleRec(FsRect(), Fade(BLACK, 1 - game.progress));
}

void DrawCutscene(){

	if(client.ui.cutsceneCount){
		Sprite_t* sprite = &client.sprites[client.ui.cutscene[0].spriteId];
		float elapsed = client.time - client.ui.cutscene[0].startTime;
		int frame = sprite->framesUsed ? (int)(elapsed / 12) % sprite->framesUsed : 0;
		char* text = client.ui.cutscene[0].text;
		int width = MeasureText(text, 40);
		int len = strlen(text);
		int symbolsShown = MIN(len, (int)(elapsed*28));
		char oldSymbol;
		Color textColor = (Color){80,190,220,255};

		DrawRectangleGradientV(0, client.height*2/3, client.width, client.height/3, (Color){0}, DARKGRAY);
		BlitSprite(client.ui.cutscene[0].spriteId, frame, (Vector2){client.halfWidth, client.height*3/4}, WHITE, 1);

		oldSymbol = text[symbolsShown];
		text[symbolsShown] = 0;
		DrawText((const char*)text, client.halfWidth-width/2, client.height*4/5, 40, textColor);
		text[symbolsShown] = oldSymbol;

		if(symbolsShown == len){
			DrawText(">>", client.width/3, client.height*4/5+80, 25, textColor);
		}

		client.chat.inputOpen = false;
		if(client.ui.cutsceneSkipped){
			client.ui.cutsceneSkipped = false;
			if(elapsed > 0.25){
				RemoveElement(&client.ui.cutscene, 0, client.ui.cutsceneCount, sizeof(client.ui.cutscene[0]));
				client.ui.cutsceneCount--;
				if(client.ui.cutsceneCount){
					client.ui.cutscene[0].startTime = client.time;
				}
			}
		}
	}

}

#undef IMP_GAMEINTERFACE_H
#endif
