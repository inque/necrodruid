#ifndef GAMEPLAY_H

void UpdateGameplay();


#define GAMEPLAY_H
#endif
#ifdef IMP_GAMEPLAY_H
//==== implementation starts here ====

#include "gameapp.h"
#include "effects.h"
#include "level_ops.h"
#include "string.h"
#include "stdio.h"

static void SpawnInPlayers(){
	forEachBoy(boy, boyId){
		bool spawnPosFound = false;
		Vector3 spawnPos = Vector3Zero();

		forEachLvEntity(ent, entId){
			if(ent->type != LVENT_PLAYERSPAWN) continue;
			if(ent->spawn.used) continue;
			spawnPos = ent->pos;
			ent->spawn.used = true;
			spawnPosFound = true;
			break;
		}
		if(!spawnPosFound){
			//cycle if all used up
			forEachLvEntity(ent, entId){
				if(ent->type != LVENT_PLAYERSPAWN) continue;
				if(!spawnPosFound){
					spawnPosFound = true;
					spawnPos = ent->pos;
				}
				ent->spawn.used = false;
			}
		}

		//boy->heroId = SpawnToy(TOY_DRUID, 0, boyId, spawnPos);
		boy->heroId = SpawnToy(TOY_DRUID, boy->isRobot, boyId, spawnPos);
		boy->toyId = boy->heroId;

		if(game.mode == MODE_STORY){
			boy->team = 14;
		}else{
			GiveCard(boyId, PLAYCARD_SUMMON_SKELETON);
			GiveCard(boyId, PLAYCARD_SUMMON_TIGRA);
			GiveCard(boyId, PLAYCARD_GIVE_SHOTGUN);
		}

	}
}

//added literally at the last minutes of the jam
static void UpdateGameplaySurvarium(){
	char text[1024];
	Vector3 centerPos = {game.level.mapSize[0]*LV_TILESIZE/2, game.level.mapSize[1]*LV_TILESIZE/2 ,0};
	static int waveCooldown;
	static int waveNumber;
	static int roboBoyId;

	switch(game.state){
		case STATE_LOBBY:
			game.state = STATE_PREPARING;
			game.timer = 0;
			break;
		case STATE_PREPARING:
			forEachBoy(boy, boyId){
				if(boy->isRobot) continue;
				boy->toyId = SpawnToy(TOY_DRUID, false, boyId, centerPos);
			}
			roboBoyId = CreateBoy(1);
			game.state = STATE_STARTING;
			game.timer = 0;
			game.progress = 0;
			break;
		case STATE_STARTING:{
			int dur = 2*TICKS_PER_SEC;
			game.progress = (float)game.timer / dur;
			if(game.timer++ > dur){
				game.state = STATE_PLAYING;
				game.timer = 0;
				waveCooldown = 0;
				waveNumber = 0;
			}
		} break;
		case STATE_PLAYING: {
			Boy_t* roboBoy = GetBoy(roboBoyId);
			bool humansAlive = false;
			bool botsAlive = false;
			
			memset(roboBoy->fog.visible, 1, sizeof(roboBoy->fog.visible));

			forEachToy(toy, toyId){
				bool toyIsPlayer = false;
				if(toy->boyId != -1){
					Boy_t* boy = GetBoy(toy->boyId);
					if(!boy->isRobot) toyIsPlayer = true;
				}
				if(toyIsPlayer){
					humansAlive = true;
				}else{
					botsAlive = true;
				}
			}

			if(!botsAlive){
				if(waveCooldown){
					waveCooldown--;
				}else{
					waveNumber++;
					sprintf(text, "Wave %i.", waveNumber);
					Shout(MESSAGE_CHAT, -1, text, 1 , 0);
					forEachBoy(boy, boyId){
						if(boy->isRobot) continue;
						if(boy->toyId == -1) continue;
						GiveCard(boyId, GetRandomValue(1, 3));
					}
					{
						int mobType = GetRandomValue(0, 2);
						int mobCount = 18*waveNumber;
						if(mobType == 0) mobCount /= 5;
						for(int i=0; i < mobCount; i++){
							float angle = GetRandomValue(0, 360)*DEG2RAD;
							Vector3 pos = {centerPos.x + GetRandomValue(1000,2000) * sinf(angle), centerPos.y + GetRandomValue(4000,8000) * cosf(angle) };
							SpawnToy(mobType, true, roboBoyId, pos);
						}
					}
					waveCooldown = 5*TICKS_PER_SEC;
				}
			}

			if(!humansAlive){
				game.state = STATE_ENDED;
				game.timer = 0;
			}
		} break;
		case STATE_ENDED:{
			int dur = 6*TICKS_PER_SEC;
			float t = (float)game.timer / dur;
			game.progress = MAX(0, 1 - t);

			if(game.timer++ > dur){
				app.shouldRestartGame = true;
			}
		}break;
	}
}



void UpdateGameplay(){
	if(game.mode == MODE_SURVARIUM){
		UpdateGameplaySurvarium();
		return;
	}
	
	switch(game.state){
		case STATE_LOBBY: {

			switch(game.mode){
				case MODE_BATTLE: {
					bool allReady = true;
					forEachBoy(boy, boyId){
						if(!boy->ready){
							allReady = false;
							break;
						}
					}
					if(allReady){
						game.state = STATE_PREPARING;
						game.timer = 0;
					}
				} break;
				default:
					game.state = STATE_PREPARING;
					game.timer = 0;
			}

		} break;
		case STATE_PREPARING: {

			SpawnInPlayers();

			forEachLvEntity(ent, entId){
				if(ent->type == LVENT_ROBOTSPAWN){
					int botId = CreateBoy(true);
					Boy_t* bot = GetBoy(botId);
					Toy_t* toy;
					float healthMult = (ent->robot.healthMult / 100.0);
					if(healthMult == 0) healthMult = 1;
					if(ent->robot.team){
						bot->team = ent->robot.team;
					}else{
						bot->team = 13;
					}
					memcpy(bot->name, ent->robot.name, sizeof(bot->name));
					bot->leaderOnly = ent->robot.leaderOnly;
					bot->toyId = SpawnToy(ent->robot.toyType, ent->robot.automated, botId, ent->pos);
					//bot->toyId = SpawnToy(ent->robot.toyType, false, botId, ent->pos);
					toy = GetToy(bot->toyId);
					bot->appearance.color = ent->robot.color;
					toy->health *= healthMult;
					toy->maxHealth *= healthMult;
					toy->immortal = ent->robot.immortal;
					toy->fatalizer = ent->robot.fatalizer;
				}
			}

			// BoyUseCard(0, 0);
			// GiveCard(1, PLAYCARD_SUMMON_TIGRA);
			// BoyUseCard(1, 0);

			game.state = STATE_STARTING;
			game.timer = 0;
			game.progress = 0;
		} break;
		case STATE_STARTING: {
			int dur = 2*TICKS_PER_SEC;
			game.progress = (float)game.timer / dur;

			if(game.timer++ > dur){
				game.state = STATE_PLAYING;
				if(game.mode == MODE_BATTLE){
					game.timer = app.config.battleDur*60*TICKS_PER_SEC;
				}else{
					game.timer = 0;
				}
			}
		} break;
		case STATE_PLAYING: {
			bool atLeastTwoDifferentTeamsPlaying = false;
			int lastTeam = -1;
			bool end = false;
			int boysInGame = 0;
			int humanPlayersInGame = 0;
			int maxTime = 60*60*TICKS_PER_SEC;
			game.progress = 1;

			forEachBoy(boy, boyId){
				bool boyConfirmed = false;
				if(boy->toyId != -1){
					Toy_t* toy = GetToy(boy->toyId);
					if(toy->health > 0){
						boyConfirmed = true;
					}
				}
				if(!boyConfirmed){
					if(!boy->leaderOnly){
						forEachToy(toy, toyId){
							if(toy->boyId != boyId) continue;
							if(toy->health > 0){
								boyConfirmed = true;
								break;
							}
						}
					}
				}
				if(boyConfirmed){
					boysInGame++;
					if(!boy->isRobot) humanPlayersInGame++;
					if(lastTeam == -1){
						lastTeam = boy->team;
					}else{
						if(lastTeam != boy->team) atLeastTwoDifferentTeamsPlaying = true;
					}
				}
			}

			switch(game.mode){
				case MODE_STORY:
					if(game.timer++ > maxTime) end = true;
					if(humanPlayersInGame < 1) end = true;
					break;
				case MODE_BATTLE:
					if(!--game.timer) end = true;
					break;
			}
			if(!atLeastTwoDifferentTeamsPlaying){
				if(humanPlayersInGame > 0){
					LvSetVar("win", 1);
					ApplyEffect((EffectParams_t){.type = EFFECT_VICTORY}, -1);
				}
				end = true;
			}

			if(end){
				game.state = STATE_ENDED;
				game.timer = 0;
			}else{
				// if(boysInGame <= 1){
				// 	game.state = STATE_ENDED;
				// 	game.timer = 0;
				// 	if(boysInGame == 1){
				// 		ApplyEffect((EffectParams_t){.type = EFFECT_VICTORY}, -1);
				// 	}
				// }
			}
		} break;
		case STATE_ENDED: {
			int dur = (game.mode == MODE_BATTLE) ? 20*TICKS_PER_SEC : 4*TICKS_PER_SEC;
			float t = (float)game.timer / (4*TICKS_PER_SEC);
			game.progress = MAX(0, 1 - t);

			if(game.timer++ > dur){
				app.shouldRestartGame = true;
			}
		} break;
	}
}

#undef IMP_GAMEPLAY_H
#endif
