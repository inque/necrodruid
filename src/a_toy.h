#pragma once
#include "physics.h"
#include "transforms.h"
#include "a_boy.h"
#include "a_toy_class.h"
#include "raymath.h"

#define MAX_TOY_LIMBS 1

typedef struct{
	int active;
	Body_t* body;
	Shape_t* shape;
	Joint_t* joint;
	Joint_t* motor;
	ShapeData_t shapeData;
	Transform_t tform;
	Vector2 lastPos;
	Vector2 lastVelocity;
} Limb_t;

typedef enum{
	ACTION_WHATEVER,
	ACTION_IDLE,
	ACTION_SCOUT,
	ACTION_WALK,
	ACTION_ATTACK_TOY,
	ACTION_RETREAT,
	ACTION_USECARD,
	ACTION_MATERIALIZE
} ToyAction_e;

typedef struct{
	ToyAction_e type;
	union{
		struct{
			int timer;
		} idle;
		struct{
			int targetToyId;
			int recheckTimer;
		} attackToy;
		struct{
			Vector3 target;
			float radius;
			bool away;
			int timer;
			int checkEnemiesNearbyTimer;
			int attackEnemyScoreThreshold;
		} walk;
	};
	int panicTimer;
	int useCardAccum;
	int useCardRecheckTimer;
} ToyAction_t;

typedef enum{
	ANIM_IDLE,
	ANIM_RUN,
	ANIM_ATTACK,
	ANIM_ATTACK_REL,
	ANIM_DASH,
	ANIM_DEATH,
	ANIM_USECARD,
	ANIM_FALL,
	ANIM_FATALITY,
	ANIM_SPAWN
} ToyAnimState_e;

typedef struct{
	int active;
	int boyId;
	ToyType_e type;
	int health;
	int maxHealth;
	bool immortal;
	bool fatalizer;
	int regenerateTimer;
	int lastTakenDamageFrame;
	int lastAnimatedHealthValue;
	int materializeTimer;
	Position_t materializeAt;
	int dematerializeTimer;
	int despawnTimer;
	BoyInputs_t inputs;
	Limb_t limbs[MAX_TOY_LIMBS];
	int sector[2];
	struct{
		ToyAnimState_e state;
		int direction8;
		int lastChangeFrame;
		int duration;
		int variant;
		bool flash;
		bool aiming;
	} animation;
	struct{
		float runAccum;
	} viz;
	struct{
		int holdingItemId;
		int lookingAtItemId;
		int lookingAtItemTimer;
		int preparation;
		int lastUsedFrame;
	} item;
	struct{
		Vector2 direction;
		Vector2 dampedDir;
		int fallenTimer;
	} walk;
	struct{
		Vector3 point[2];
		int phase;
		int lastChangeFrame;
		int lastChangeDelta;
	} loco;
	struct{
		int timer;
		int cooldown;
		Vector2 direction;
	} dash;
	struct{
		int variant;
		int series;
		int preparation;
		int cooldown;
		int seriesCooldown;
		Vector2 direction;
		int lastPerformedFrame;
		int selfKnockbackLastAppliedFrame;
		bool crit;
	} melee;
	struct{
		int timer;
		int cooldown;
		int cardSlot;
	} useCard;
	struct{
		bool canPerform;
		int toyId;
		int timer;
	} fatality;
	struct{
		bool enabled;
		ToyAction_t action;
		struct{
			int active;
			Vector3 target;
		} navigation;
		int dbg[8];
	} automation;
} Toy_t;

int SpawnToy(ToyType_e type, bool automated, int boyId, Position_t pos);
void RemoveToy(int id);
void SyncToy(int toyId);
void UpdateToys();
void DrawToys();

static inline bool ToyButtonDown(Toy_t* toy, int button){
	return (toy->inputs.buttons & button);
}

static inline bool ToyButtonPressed(Toy_t* toy, int button){
	return (toy->inputs.buttons & button) && (!(toy->inputs.buttonsPrev & button));
}

cpBool CollisionPresolveToy  (cpArbiter* arb, cpSpace* space, void *data);
void   CollisionPostsolveToy (cpArbiter* arb, cpSpace* space, void *data);

bool TakeDamage(int toyId, int dmg);
bool IsToyFriendly(int a_toyId, int b_toyId);
