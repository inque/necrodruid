#include "gameapp.h"
#include "utils.h"
#include "geometry.h"
#include "sprite_batching.h"
#include "rlgl.h"
#include "raymath.h"
#include "math.h"
#include "stdio.h"
#include "stdint.h"
#include "string.h"

static inline uint32_t* GetPixelPtr(Color* pixels, int width, int x, int y){
	return (uint32_t*)(&pixels[y*width + x]);
}

static Texture2D LoadMaskedTexture(const char* path, bool expanded){
	const uint32_t ALPHA = (0xFF << 24);
	const uint32_t COLOR = (0x01 << 24) - 1;
	const Color expMaskColor = (Color){255, 0, 255, 255};
	  const int expMaskThreshold = 60;
	const float expMaskAmplify = 2.0f;

	Image img = LoadImage(path);
	if(!img.data){
		//error loading file
		return (Texture2D){0};
	}
	uint32_t bgColor = ((uint32_t*)(img.data))[0] | ALPHA;
	Color* pixels;
	Color* pixelsOriginal;
	int originalWidth = img.width;
	if(expanded){
		pixelsOriginal = GetImageData(img);
		img.width *= 2;
		pixels = RL_CALLOC(sizeof(Color), img.width * img.height);
	}else{
		pixels = GetImageData(img);
	}
	for(int iy=0; iy < img.height; iy++)
	for(int ix=0; ix < originalWidth; ix++){
		uint32_t* dst = GetPixelPtr(pixels, img.width, ix, iy);
		if(expanded){
			uint32_t* src = GetPixelPtr(pixelsOriginal, originalWidth, ix, iy);
			*dst = *src;
		}
		if(*dst == bgColor){
			*dst &= COLOR;
		}else if(expanded){
			Color dstCol = *(Color*)dst;
			int diff[] = { expMaskColor.r - dstCol.r,
						   expMaskColor.g - dstCol.g,
						   expMaskColor.b - dstCol.b };
			int diffTotal = abs(diff[0]) + abs(diff[1]) + abs(diff[2]);
			if(diffTotal < expMaskThreshold){
				Color* mirrorDstColor = (Color*)GetPixelPtr(pixels, img.width, ix + originalWidth, iy);
				mirrorDstColor->r = 255 - expMaskThreshold + diff[0];
				mirrorDstColor->g = 255 - expMaskThreshold + diff[1];
				mirrorDstColor->b = 255 - expMaskThreshold + diff[2];
				mirrorDstColor->a = 255;
				*dst &= COLOR;
			}
		}
	}
	RL_FREE(img.data);
	img.data = pixels;
	img.format = UNCOMPRESSED_R8G8B8A8;
	img.mipmaps = 1;
	Texture2D tex = LoadTextureFromImage(img);
	
	UnloadImage(img);
	return tex;
}

void ReloadSheet(int sheetId){
	SpriteSheet_t* sheet = &client.sheets[sheetId];
	
	char path[128];
	sprintf(path, "data/gfx/%s", sheet->texname);
	if(sheet->tex.id != 0){
		UnloadTexture(sheet->tex);
	}
	sheet->tex = LoadMaskedTexture(path, false);
}

void RemoveSheet(int sheetId){
	SpriteSheet_t* sheet = &client.sheets[sheetId];
	sheet->active = 0;
	if(sheet->tex.id != 0){
		UnloadTexture(sheet->tex);
	}
	for(int spriteId=0; spriteId < MAX_SPRITES; spriteId++){
		Sprite_t* sprite = &client.sprites[spriteId];
		if(sprite->sheetId != sheetId) continue;
		sprite->active = 0;
		for(int i=0; i < sprite->framesUsed; i++){
			SpriteShape_t* shape = &client.shapes[sprite->shapeId[i]];
		}
	}
}

int LoadSpriteSheet(const char* path, SpriteLookup_t* lookupTable){
	FILE* file = fopen(path, "rb");
	if(!file) return -1;

	uint32_t data[4];
	uint32_t version;
	uint32_t blockLength;
	long blockStart;

	//check if it's already loaded
	for(int sheetId=0; sheetId < MAX_SHEETS; sheetId++){
		SpriteSheet_t* sheet = &client.sheets[sheetId];
		if(!sheet->active) continue;
		if(!strncmp(sheet->spr_path, path, sizeof(sheet->spr_path))){
			return sheetId;
		}
	}

	fread(data, 4, 2, file);
	if(data[0] != CHR4('n','S','P','R')){
		fclose(file);
		return -1;
	}
	version = data[1];

	//find a free sheet slot
	for(int sheetId=0; sheetId < MAX_SHEETS; sheetId++){
		SpriteSheet_t* sheet = &client.sheets[sheetId];
		if(sheet->active) continue;

		//resume reading
		blockStart = ftell(file);
		fread(&blockLength, 4, 1, file);

		fread(&data, 4, 1, file);
		unsigned int spriteCount = data[0];
		char texname[24], texpath[64];
		fread(&texname, 1, sizeof(texname), file);
		sprintf(texpath, "data/gfx/%s", texname);
		sheet->active = 1;
		strcpy_s(sheet->spr_path, sizeof(sheet->spr_path), path);
		strcpy_s(sheet->texname, sizeof(sheet->texname), texname);
		ReloadSheet(sheetId);

		fseek(file, blockStart + blockLength, SEEK_SET);
		
		//printf("loaded texture at %s.\n", texpath);
		for(unsigned int i=0; i < spriteCount; i++){
			for(int spriteId=0; spriteId < MAX_SPRITES; spriteId++){
				Sprite_t* spr = &client.sprites[spriteId];
				if(spr->active) continue;

				spr->active = 1;
				spr->sheetId = sheetId;
				blockStart = ftell(file);
				fread(&blockLength, 4, 1, file);
				fread(&spr->name, sizeof(spr->name), 1, file);
				//fread(&spr->name, 24, 1, file);
				fread(&spr->framesUsed, 4, 1, file);
				//printf("read sprite %s. frames = %i\n", spr->name, spr->framesUsed);
				fseek(file, blockStart + blockLength, SEEK_SET);
				for(int i2=0; i2 < spr->framesUsed; i2++){
					for(int shapeId=0; shapeId < MAX_SHAPES; shapeId++){
						SpriteShape_t* shape = &client.shapes[shapeId];
						if(shape->active) continue;

						shape->active = 1;
						blockStart = ftell(file);
						fread(&blockLength, 4, 1, file);
						fread(&shape->uv, 4, 2, file);
						fread(&shape->vertCount, 4, 1, file);
						fread(&shape->verts, 4, 2*shape->vertCount, file);
						fseek(file, blockStart + blockLength, SEEK_SET);
						if(shape->vertCount > MAX_SHAPE_VERTS) shape->vertCount = 0;
						//printf("read shape. %i.\n", shape->vertCount);
						//printf("read shape. %s %i: %i.\n", spr->name, i2, shape->vertCount);
						spr->shapeId[i2] = shapeId;
						break;
					}
				}

				if(lookupTable){
					SpriteLookup_t* entry = &lookupTable[0];
					while(entry->outSpriteId){
						if(!strncmp(entry->name, spr->name, sizeof(spr->name))){
							*entry->outSpriteId = spriteId;
							break;
						}
						entry++;
					}
				}
				break;
			}
		}
		fclose(file);
		return sheetId;
	}
	fclose(file);
	return -1;
}

int FindSpriteByName(const char* name){
	for(int spriteId=0; spriteId < MAX_SPRITES; spriteId++){
		Sprite_t* sprite = &client.sprites[spriteId];
		if(!sprite->active) continue;
		
		if(!strncmp((char*)&sprite->name, name, sizeof(sprite->name))){
			return spriteId;
		}
	}
	//return -1;
	return 0;
}

static void TriangulateShape(SpriteShape_t* shape){
	Triangulation2dOutput_t out;
	Triangulate2d(shape->vertCount, (Vector2*)&shape->verts, &out);
	shape->indicesUsed = 3 * out.triCount;
	for(int i=0; i < out.triCount; i++){
		shape->indices[i*3+0] = out.indices[i][0];
		shape->indices[i*3+1] = out.indices[i][1];
		shape->indices[i*3+2] = out.indices[i][2];
	}
	//printf("triangulated. got %i polys.\n", shape->indicesUsed/3);
}

void DrawSprite(int spriteId, int frame, Vector3 pos, Color tint, float size){
	Sprite_t* sprite = &client.sprites[spriteId];
	SpriteSheet_t* sheet = &client.sheets[sprite->sheetId];
	SpriteShape_t* shape = &client.shapes[sprite->shapeId[frame]];
	if((!shape->indicesUsed) && (shape->vertCount >= 3)){
		TriangulateShape(shape);
	}
	
	SpriteQueuePush((SpriteQueueEntry_t){
		.shape = shape,
		.tex = &sheet->tex,
		.pos = pos,
		.scale = size,
		.tint = tint
	});
}

void BlitSprite(int spriteId, int frame, Vector2 pos, Color tint, float size){
	Sprite_t* sprite = &client.sprites[spriteId];
	SpriteSheet_t* sheet = &client.sheets[sprite->sheetId];
	SpriteShape_t* shape = &client.shapes[sprite->shapeId[frame]];
	if((!shape->indicesUsed) && (shape->vertCount >= 3)){
		TriangulateShape(shape);
	}

	SpriteQueueDrawShape(&(SpriteQueueEntry_t){
		.shape = shape,
		.tex = &sheet->tex,
		.pos = (Vector3){pos.x, pos.y, 0},
		.scale = size,
		.tint = tint
	});
}

void BeginDrawingSprites(){
	SpriteQueueClear(endQueue);
}

void EndDrawingSprites(){
	SpriteQueueRender(startQueue);
}

void BeginSpriteCutoutHack(){
	rlDrawRenderBatchActive();
	rlEnableDepthTest();
}

void EndSpriteCutoutHack(){
	rlDrawRenderBatchActive();
	rlDisableDepthTest();
}