#pragma once
//#include "masterserver.h"
#include "stdint.h"

typedef enum{
	MASTER_REQUEST_SERVERLIST = 0x20,
	MASTER_REQUEST_HOST = 0x30
} MasterRequestMessage_e;

typedef enum{
	MASTER_RESPONSE_SERVERLIST = 0x20
} MasterResponseMessage_e;

typedef enum {
	INFO_REQUEST_STATUS = 0x20
} InfoRequestMessage_e;

typedef enum {
	INFO_RESPONSE_STATUS = 0x20
} InfoResponseMessage_e;

#pragma pack(push, 1)

typedef struct{
	uint32_t magic;
	char code;
	union{
		char data[8192 - 5];
		struct MasterRequestServerList_st{
			uint32_t version;
            uint32_t offset;
		} serverList;
		struct MasterRequestHost_st{
			uint32_t gameId;
			uint32_t unused;
		} host;
	};
} MasterRequestMessage_t;

typedef struct{
	uint32_t magic;
	char code;
	union{
		char data[8192 - 5];
		struct MasterResponseServerList_st{
			uint16_t serverCount;
			uint16_t pageSize;
			uint16_t serverSize;
			uint32_t reserved[4];
			struct MasterResponseServerListEntry_st{
				uint32_t ip[4];
				uint16_t port;
			} servers[SERVERS_PER_PAGE];
		} serverList;
	};
} MasterResponseMessage_t;

typedef struct {
	uint32_t magic;
	uint8_t code;
	union {
		struct InfoRequestStatus_st{
			uint32_t version;
			uint32_t clientTimestamp;
		} status;
	};
} InfoRequestMessage_t;

typedef struct {
	uint32_t magic;
	uint8_t code;
	union {
		struct InfoResponseStatus_st{
			uint32_t clientTimestamp;
			uint32_t version;
			char isCompatible;
			char playersOn;
			char playersMax;
			char gameType[16];
			char title[48];
		} status;
	};
} InfoResponseMessage_t;

#pragma pack(pop)

