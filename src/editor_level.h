#ifndef EDITOR_LEVEL_H

void LevelEditor();

#define EDITOR_LEVEL_H
#endif
#ifdef IMP_EDITOR_LEVEL_H
//==== implementation starts here ====
#include "gameapp.h"
#include "graphics.h"
#include "geometry.h"
#include "utils.h"
#include "level_ops.h"
#include "raygui.h"


typedef enum{
	LVED_SEL_NONE,
	LVED_SEL_SPRITE,
	LVED_SEL_BRUSH,
	LVED_SEL_LAYER,
	LVED_SEL_TERRAIN,
	LVED_SEL_ENTITY,
	LVED_SEL_SCRIPTS
} LvEditorSelection_e;

typedef struct{
	int initialized;
	Vector2 camWayPoint;
	Vector2 mouseLast;
	int sheetIndex;
	bool sheetToAddEditing;
	char sheetToAddPath[64];
	LvEditorSelection_e selection;
	int spritePageOffset;
	int spriteSelected;
	int brushSelected;
	int layerSelected;
	int entitySelected;
	int scriptSelected;
	bool layerNameEditing;
	bool layerOrderEditing;
	bool entityTypeSelecting;
	bool entityRobotNameEditing;
	bool entityRobotHealthEditing;
	bool entityRobotTeamEditing;
	int terrainColor;
	int terrainBrushSize;
	bool levelPathEditing;
	bool scriptCommentEditing;
	bool scriptConditionTypeSelecting;
	bool scriptConditionVariableEditing;
	bool scriptConditionValueEditing;
	bool scriptActionTypeSelecting;
	bool scriptActionChatEditing;
	bool levelWidthEditing;
	bool levelHeightEditing;
	int newMapSize[2];
} LevelEditorState_t;

LevelEditorState_t lv_editor = {
	.sheetIndex = -1,
	.spriteSelected = -1,
	.brushSelected = -1,
	.layerSelected = 1,
	.entitySelected = -1,
	.scriptSelected = -1,
	.terrainBrushSize = 2
};

static void ColorTerrain(unsigned char* colorPacked, int change){
	Level_t* level = &game.level;
	int newColor = lv_editor.terrainColor;
	int valueRange = LV_TERRAINTEXROWS*LV_TERRAINTEXROWS;
	int colorIndex = (*colorPacked / valueRange);
	int colorPower = (*colorPacked % valueRange);

	if(change != 0){
		//printf("index: %i. power: %i.\n", colorIndex, colorPower);
		int newPower = (colorIndex == lv_editor.terrainColor) ? (colorPower + change) : change;
		*colorPacked = newColor*valueRange + MAX(0, MIN(newPower, valueRange-1));
	}
}

static const char* entityTypes[] = {
	"<unused>", "Player spawn", "Robot spawn", NULL
};

//special format required by raygui :/
static const char* entityTypesJoined = "<unused>;Player spawn;Robot spawn";

void LevelEditor(){
	char text[2048];
	char bottomText[1024];
	Level_t* level = &game.level;
	Rectangle bounds = FsRect();
	Rectangle rightPanel = CutRight(&bounds, 250);
	Rectangle topPanel = CutTop(&bounds, 180);
	Rectangle bnd;
	bool noedit = !(
		lv_editor.sheetToAddEditing
		|| lv_editor.layerNameEditing
		|| lv_editor.levelPathEditing
		|| lv_editor.layerOrderEditing
		|| lv_editor.scriptCommentEditing
		|| lv_editor.scriptConditionVariableEditing
		|| lv_editor.scriptConditionValueEditing
		|| lv_editor.scriptActionChatEditing
		|| lv_editor.levelWidthEditing
		|| lv_editor.levelHeightEditing
		|| lv_editor.entityRobotNameEditing
		|| lv_editor.entityRobotHealthEditing
		|| lv_editor.entityRobotTeamEditing
	);
	bool focused = IsMouseInRect(bounds);
	Vector2 mouse = GetMousePosition();
	Vector2 mouseWorldUnprojected = GetScreenToWorld2D(mouse, client.cam);
	Vector2 mouseWorld = GetScreenToWorld2D(mouse, client.cam);
	bool lmb = IsMouseButtonPressed(MOUSE_BUTTON_LEFT);
	bool rmb = IsMouseButtonPressed(MOUSE_BUTTON_RIGHT);
	int mouseWheel = GetMouseWheelMove();
	Vector2 mouseDelta = Vector2Subtract(mouse, lv_editor.mouseLast);
	lv_editor.mouseLast = mouse;
	mouseWorld.y /= app.config.persp_y;

	sprintf(bottomText, "%.02f x %.02f\n", mouseWorld.x, mouseWorld.y);

	if(!lv_editor.initialized){
		lv_editor.initialized = 1;
		//level->filePath[0] = 0;
		//LoadLevel(NULL);
		ResetGame();
		lv_editor.camWayPoint = (Vector2){level->mapSize[0]/2*LV_TILESIZE, level->mapSize[1]/2*app.config.persp_y*LV_TILESIZE};

		lv_editor.newMapSize[0] = level->mapSize[0];
		lv_editor.newMapSize[1] = level->mapSize[1];
	}

	client.cam.target = Vector2Lerp(client.cam.target, lv_editor.camWayPoint, SmoothOver(0.9, 0.8, client.deltaTime));
	if(noedit){
		float speed = 4.0 * client.deltaFrame;
		if(IsKeyDown(KEY_LEFT_SHIFT)) speed *= 2;
		if(IsKeyDown(KEY_A)) lv_editor.camWayPoint.x -= speed;
		if(IsKeyDown(KEY_D)) lv_editor.camWayPoint.x += speed;
		if(IsKeyDown(KEY_W)) lv_editor.camWayPoint.y -= speed;
		if(IsKeyDown(KEY_S)) lv_editor.camWayPoint.y += speed;
	}

	ClearBackground(game.level.bgColor);
	BeginMode2D(client.cam);
		DrawLevelTerrain();
		BeginDrawingSprites();
			DrawLevel(-9999, -1);
		EndDrawingSprites();
		BeginDrawingSprites();
			DrawLevel(0, 9999);
		EndDrawingSprites();
		if(focused){
			switch(lv_editor.selection){
				case LVED_SEL_LAYER:
					if(lmb){
						lv_editor.selection = LVED_SEL_NONE;
					}
					break;
				case LVED_SEL_SPRITE: {
					BlitSprite(lv_editor.spriteSelected, 0, mouseWorldUnprojected, Fade(WHITE, 0.25), 1);
					if(lmb){
						LvAddBrush(lv_editor.layerSelected, (LvBrush_t){
							.pos = {mouseWorld.x, mouseWorld.y, 0},
							.size = 1,
							.spriteId = lv_editor.spriteSelected,
							.spriteFrame = 0,
							.tint = WHITE,
							.flags = 0,
							.z1 = 0,
							.z2 = 100
						});
						lv_editor.spriteSelected = -1;
						lv_editor.selection = LVED_SEL_NONE;
					}
				} break;
				case LVED_SEL_BRUSH: {
					LvBrush_t* brush = &level->brushes[lv_editor.brushSelected];
					Sprite_t* sprite = &client.sprites[brush->spriteId];
					DrawRectangleLines(brush->bbox[0], brush->bbox[1], brush->bbox[2]-brush->bbox[0], brush->bbox[3]-brush->bbox[1], Fade(RED, 0.3));
					if(noedit && IsKeyDown(KEY_G)){
						brush->pos.x += mouseDelta.x;
						brush->pos.y += mouseDelta.y;
					}
					if(noedit && IsKeyDown(KEY_H)){
						brush->size += 0.01 * mouseDelta.x;
					}
					if(IsKeyReleased(KEY_G) || IsKeyReleased(KEY_H)){
						InitBrush(brush);
					}
					if(noedit &&IsKeyPressed(KEY_DELETE)){
						// RemoveElement(&level->brushes, lv_editor.brushSelected, level->brushCount, sizeof(LvBrush_t));
						// level->brushCount--;
						LvRemoveBrush(lv_editor.brushSelected);
						lv_editor.selection = LVED_SEL_NONE;
						break;
					}
					if(lmb){
						lv_editor.selection = LVED_SEL_NONE;
					}
					sprintf(bottomText, "frame %i/%i", brush->spriteFrame, sprite->framesUsed);
					
					if(noedit && IsKeyPressed(KEY_COMMA)) brush->spriteFrame = mod2(brush->spriteFrame-1, sprite->framesUsed);
					if(noedit && IsKeyPressed(KEY_PERIOD)) brush->spriteFrame = (brush->spriteFrame+1)%sprite->framesUsed;
				} break;
				case LVED_SEL_ENTITY: {
					LvEntity_t* ent = &level->entities[lv_editor.entitySelected];
					Vector2 pos = Project2d(ent->pos);

					if(IsKeyDown(KEY_G)){
						ent->pos.x += mouseDelta.x;
						ent->pos.y += mouseDelta.y;
					}
					if(IsKeyPressed(KEY_DELETE)){
						RemoveElement(&level->entities, lv_editor.entitySelected, level->entityCount, sizeof(LvEntity_t));
						level->entityCount--;
						lv_editor.selection = LVED_SEL_NONE;
						break;
					}
					if(lmb){
						lv_editor.selection = LVED_SEL_NONE;
					}

					DrawRectangleLines(pos.x-50, pos.y-50, 100, 100, ORANGE);
				} break;
				case LVED_SEL_SCRIPTS: {
					LvScriptLine_t* exp = &level->script[lv_editor.scriptSelected];
					switch(exp->condition.type){
						case LVCOND_TRIGGER:
							Vector2 pos = Project2d(exp->condition.point.pos);
							DrawCircle(pos.x, pos.y, exp->condition.point.radius, Fade(ORANGE, 0.3));
							DrawCircle(pos.x, pos.y, 20, Fade(RED, 0.7));
							if(noedit && IsKeyDown(KEY_G)){
								if(Vector2Distance(Vec2(exp->condition.point.pos), mouseWorld) > 100){
									exp->condition.point.pos.x = mouseWorld.x;
									exp->condition.point.pos.y = mouseWorld.y;
								}else{
									exp->condition.point.pos.x += mouseDelta.x;
									exp->condition.point.pos.y += mouseDelta.y;
								}
							}
							if(noedit && IsKeyDown(KEY_H)){
								exp->condition.point.radius += mouseDelta.x;
							}
							if(noedit && IsKeyDown(KEY_J)){
								exp->condition.point.pos.z += mouseDelta.y;
							}							

							break;
					}

				} break;
				case LVED_SEL_NONE: {
					
					for(int brushId=0; brushId < level->brushCount; brushId++){
						LvBrush_t* brush = &level->brushes[brushId];
						Sprite_t* sprite = &client.sprites[brush->spriteId];
						Vector2 textPos = Project2d(brush->pos);
						DrawText(sprite->name, textPos.x, textPos.y-40, 15, Fade(LIGHTGRAY, 0.5));
						bool hovering = (
							(mouseWorldUnprojected.x >= brush->bbox[0])
							&& (mouseWorldUnprojected.y >= brush->bbox[1])
							&& (mouseWorldUnprojected.x <= brush->bbox[2])
							&& (mouseWorldUnprojected.y <= brush->bbox[3])
						);
						if(hovering){
							Vector2 pos = Project2d(brush->pos);
							DrawRectangleLines(brush->bbox[0], brush->bbox[1], brush->bbox[2]-brush->bbox[0], brush->bbox[3]-brush->bbox[1], Fade(WHITE, 0.5));
							DrawLine(pos.x-10, pos.y, pos.x+10, pos.y, LIGHTGRAY);
							DrawLine(pos.x, pos.y-10, pos.x, pos.y+10, LIGHTGRAY);

							if(lmb){
								if(Vector2Distance(pos, mouseWorldUnprojected) < 30){
									lv_editor.selection = LVED_SEL_BRUSH;
									lv_editor.brushSelected = brushId;
									break;
								}
							}
						}
					}

					forEachLvEntity(ent, entId){
						Vector2 pos = Project2d(ent->pos);
						//Rectangle rect = {pos.x-25, pos.y-25, 50, 50};
						Rectangle rectWorld = {ent->pos.x-25, ent->pos.y-25, 50, 50};
						Rectangle rect = {pos.x-25, pos.y-25, 50, 50};
						bool hovering = (mouseWorld.x>rectWorld.x && mouseWorld.y>rectWorld.y && mouseWorld.x<(rectWorld.x+rectWorld.width) && mouseWorld.y<(rectWorld.y+rectWorld.height));
						DrawRectangleLinesEx(rect, 2, hovering ? WHITE : RED);
						DrawText(entityTypes[(ent->type < _LVENT_COUNT) ? ent->type : 0], pos.x-50, pos.y-40, 20, RED);
						if(hovering && lmb){
							lv_editor.selection = LVED_SEL_ENTITY;
							lv_editor.entitySelected = entId;
							break;
						}
					}

					if(noedit && IsKeyPressed(KEY_T) && (!client.chat.inputOpen)){
						level->entities[level->entityCount++] = (LvEntity_t){
							.type = LVENT_PLAYERSPAWN,
							.pos = (Vector3){mouseWorld.x, mouseWorld.y, 0}
						};
					}

				} break;
				case LVED_SEL_TERRAIN: {
					int tile[] = {
						(int)(mouseWorld.x / LV_TILESIZE),
						(int)(mouseWorld.y / LV_TILESIZE)
					};
					if((tile[0] >= 0) && (tile[1] >= 0) && (tile[0] < level->mapSize[0]) && (tile[1] < level->mapSize[1])){
						unsigned char* height = &level->terrainHeight[tile[0]][tile[1]];
						int brushSize = (lv_editor.terrainBrushSize*2+1);
						if(IsKeyPressed(KEY_E)){
							if(*height < 255) *height = MIN(255, *height + 5);
						}
						if(IsKeyPressed(KEY_Q)){
							if(*height > 0) *height = MAX(0, *height - 5);
						}
						if(mouseWheel != 0) lv_editor.terrainBrushSize = MAX(0, MIN(mouseWheel, 10));
						if(lmb ^ rmb){
							float sign = lmb ? 1 : -1;
							for(int iy=0; iy < brushSize; iy++)
							for(int ix=0; ix < brushSize; ix++){
								float power = lv_editor.terrainBrushSize - Vector2Distance((Vector2){ix, iy}, (Vector2){brushSize/2.0, brushSize/2.0});
								if(power > 0){
									unsigned char* colorPacked = &level->terrainColor[tile[0]-brushSize/2+ix][tile[1]-brushSize/2+iy];
									ColorTerrain(colorPacked, (int)(power*sign*8));
								}
							}
						}
						DrawRectangleLines(mouseWorld.x-brushSize/2*LV_TILESIZE, mouseWorld.y-brushSize/2*LV_TILESIZE, brushSize*LV_TILESIZE, brushSize*LV_TILESIZE*app.config.persp_y, LIGHTGRAY);
					}
				} break;
			}
		}
	EndMode2D();
	BeginMode2D(client.uiCam);

	GuiWindowBox(topPanel, "");
	{
		int spritesOnPage = 0;
		int spriteLastFound = -1;
		Extend(&topPanel, -6);
		CutTop(&topPanel, 20);
		{
			bnd = CutTop(&topPanel, 26);
			if(GuiToggle(CutLeft(&bnd, 80), "brush", lv_editor.selection == LVED_SEL_NONE)){
				lv_editor.selection = LVED_SEL_NONE;
			}
			if(GuiToggle(CutLeft(&bnd, 80), "terrain", lv_editor.selection == LVED_SEL_TERRAIN)){
				lv_editor.selection = LVED_SEL_TERRAIN;
			}
			if(GuiToggle(CutLeft(&bnd, 80), "scripts", lv_editor.selection == LVED_SEL_SCRIPTS)){
				lv_editor.selection = LVED_SEL_SCRIPTS;
			}
		}
		switch(lv_editor.selection){
			case LVED_SEL_TERRAIN: {
				GuiLabel(CutTop(&topPanel, 20), "Terrain type");
				bnd = (Rectangle){topPanel.x+8, topPanel.y, 60, 60};
				for(int colorIndex=0; colorIndex < LV_TERRAINTEXROWS*LV_TERRAINTEXROWS; colorIndex++){
					float quadrantSize = client.assets.textures.terrain.height/4;
					Rectangle sourceRect = {(colorIndex % LV_TERRAINTEXROWS)*quadrantSize, (colorIndex / LV_TERRAINTEXROWS)*quadrantSize, quadrantSize, quadrantSize};
					DrawTexturePro(client.assets.textures.terrain, sourceRect, bnd, Vector2Zero(), 0, WHITE);
					if(lv_editor.terrainColor == colorIndex){
						DrawRectangleLinesEx(bnd, 2, RED);
					}else{
						if(lmb && IsMouseInRect(bnd)){
							lv_editor.terrainColor = colorIndex;
						}
					}
					bnd.x += bnd.width + 2;
					if((bnd.x + bnd.width) > (topPanel.x + topPanel.width)){
						bnd.x = topPanel.x;
						bnd.y += bnd.height + 2;
					}
				}
			} break;
			case LVED_SEL_SCRIPTS: {
				Rectangle wnd = {client.halfWidth-300, client.halfHeight-300, 800, 600};

				if(GuiWindowBox(wnd, "Scenario editor")) lv_editor.selection = LVED_SEL_NONE;
				CutTop(&wnd, 20);
				Extend(&wnd, -10);
				{
					Rectangle leftPanel = CutLeft(&wnd, 180);
					static Vector2 scroll;
					Rectangle scrollZone = CutTop(&leftPanel, 400);
					Rectangle content = {scrollZone.x, scrollZone.y, 160, 26*level->scriptCount};
					GuiScrollPanel(scrollZone, content, &scroll);
					BeginScissorMode(scrollZone.x, scrollZone.y, scrollZone.width, scrollZone.height);
					scrollZone.y += scroll.y;
					bnd = CutLeft(&scrollZone, 160);
					for(int i=0; i < level->scriptCount; i++){
						LvScriptLine_t* exp = &level->script[i];
						bool selected = (lv_editor.scriptSelected == i);
						Rectangle line = CutTop(&bnd, 26);
						sprintf(text, "%i. %s", i, exp->comment);
						if(GuiToggle(line, text, selected)){
							lv_editor.scriptSelected = i;
						}
						if(selected && lv_editor.scriptCommentEditing){
							if(GuiTextBox(line, (char*)&exp->comment, sizeof(exp->comment), true)){
								lv_editor.scriptCommentEditing = false;
							}
						}
					}
					EndScissorMode();
					bnd = CutTop(&leftPanel, 26);
					if(level->scriptCount < LV_SCRIPTS){
						if(GuiButton(CutLeft(&bnd, 32), "+")){
							level->script[level->scriptCount++] = (LvScriptLine_t){
								.condition.type = LVCOND_NONE,
								.action = {.type = LVEXP_CHAT, .chat = ""}
							};
						}
					}
					if((level->scriptCount > 0) && (lv_editor.scriptSelected != -1)){
						if(GuiButton(CutLeft(&bnd, 32), "-")){
							RemoveElement(&level->script, lv_editor.scriptSelected, level->scriptCount, sizeof(LvScriptLine_t));
							level->scriptCount--;
						}
					}
					if(lv_editor.scriptSelected != -1){
						if(GuiButton(CutLeft(&bnd, 32), "R")){
							lv_editor.scriptCommentEditing = true;
						}
					}
				}
				if(lv_editor.scriptSelected != -1){
					const char* conditionNames = "None;Var equals;Trigger";
					const char* actionNames = "Chat command;Cutscene";
					LvScriptLine_t* exp = &level->script[lv_editor.scriptSelected];
					Rectangle editZone = CutRight(&wnd, 360);
					CutRight(&wnd, 20);
					Rectangle centerColumn = CutRight(&wnd, 200);
					bnd = CutTop(&editZone, 26);
					if(GuiCheckBox(CutLeft(&bnd, 40), "Multi-use?", exp->multiUse)){
						exp->multiUse = true;
					}else{
						exp->multiUse = false;
					}
					GuiLabel(CutTop(&centerColumn, 26), "Condition:");
					if(GuiDropdownBox(CutTop(&centerColumn, 26), conditionNames, (int*)&exp->condition.type, lv_editor.scriptConditionTypeSelecting)){
						lv_editor.scriptConditionTypeSelecting = !lv_editor.scriptConditionTypeSelecting;
					}
					GuiLabel(CutTop(&editZone, 26), "Condition:");
					switch(exp->condition.type){
						case LVCOND_NONE:
							GuiLabel(CutTop(&editZone, 26), "(execute on start)");
							break;
						case LVCOND_VAR_EQ:
							bnd = CutTop(&editZone, 26);
							if(GuiTextBox(CutLeft(&bnd, 80), (char*)&exp->condition.var.name, sizeof(exp->condition.var.name), lv_editor.scriptConditionVariableEditing)){
								lv_editor.scriptConditionVariableEditing = !lv_editor.scriptConditionVariableEditing;
							}
							if(GuiValueBox(CutRight(&bnd, 40), "Value:", &exp->condition.var.value, INT32_MIN, INT32_MAX, lv_editor.scriptConditionValueEditing)){
								lv_editor.scriptConditionValueEditing = !lv_editor.scriptConditionValueEditing;
							}
							break;
						case LVCOND_TRIGGER:
							sprintf(text, "POSITION: %.02f %.02f %.02f", exp->condition.point.pos.x, exp->condition.point.pos.y, exp->condition.point.pos.z);
							GuiLabel(CutTop(&editZone, 26), text);
							exp->condition.point.radius = GuiSlider(CutTop(&editZone, 26), "", "", exp->condition.point.radius, 0.1, 1000.0);
							if(exp->condition.point.radius == 0) exp->condition.point.radius = 100;
							sprintf(text, "RADIUS: %.02f", exp->condition.point.radius);
							GuiLabel(CutTop(&editZone, 26), text);
							break;
					}
					CutTop(&centerColumn, 150);
					GuiLabel(CutTop(&centerColumn, 26), "Action:");
					if(GuiDropdownBox(CutTop(&centerColumn, 26), actionNames, (int*)&exp->action.type, lv_editor.scriptActionTypeSelecting)){
						lv_editor.scriptActionTypeSelecting = !lv_editor.scriptActionTypeSelecting;
					}
					GuiLabel(CutTop(&editZone, 26), "Action:");
					switch(exp->action.type){
						case LVEXP_CHAT:
							if(GuiTextBox(CutTop(&editZone, 26), exp->action.chat, sizeof(exp->action.chat), lv_editor.scriptActionChatEditing)){
								lv_editor.scriptActionChatEditing = !lv_editor.scriptActionChatEditing;
							}
							break;
						case LVEXP_CUTSCENE:
							bnd = CutTop(&editZone, 26);
							if(GuiCheckBox(CutLeft(&bnd, 40), "Global?", exp->action.cutscene.global)){
								exp->action.cutscene.global = true;
							}else{
								exp->action.cutscene.global = false;
							}
							// if(GuiTextBox(CutTop(&editZone, 26), exp->action.cutscene.text, sizeof(exp->action.cutscene.text), lv_editor.scriptActionChatEditing)){
							// 	lv_editor.scriptActionChatEditing = !lv_editor.scriptActionChatEditing;
							// }
							if(GuiTextBoxMulti(CutTop(&editZone, 120), exp->action.cutscene.text, sizeof(exp->action.cutscene.text), lv_editor.scriptActionChatEditing)){
								lv_editor.scriptActionChatEditing = !lv_editor.scriptActionChatEditing;
							}

							break;
					}
				}
			} break;
			default: {
				GuiLabel(CutTop(&topPanel, 20), "Palette");
				bnd = (Rectangle){topPanel.x+8, topPanel.y, 120, 26};

				if(GuiToggle(bnd, "(none)", lv_editor.spriteSelected == -1)){
					if(lv_editor.spriteSelected != -1){
						lv_editor.spriteSelected = -1;
						lv_editor.selection = LVED_SEL_NONE;
					}
				}
				bnd.x += bnd.width + 2;

				for(int spriteId = lv_editor.spritePageOffset; spriteId < MAX_SPRITES; spriteId++){
					Sprite_t* sprite = &client.sprites[spriteId];
					bool relatedToLevel = false;
					if(!sprite->active) continue;
					for(int i=0; i < level->sheetCount; i++){
						if(sprite->sheetId == level->sheets[i]){
							relatedToLevel = true;
							break;
						}
					}
					if(!relatedToLevel) continue;

					if(GuiToggle(bnd, sprite->name, spriteId == lv_editor.spriteSelected)){
						if(spriteId != lv_editor.spriteSelected){
							lv_editor.spriteSelected = spriteId;
							lv_editor.selection = LVED_SEL_SPRITE;
						}
					}
					spriteLastFound = spriteId;
					spritesOnPage++;

					bnd.x += bnd.width + 2;
					if((bnd.x + bnd.width) > (topPanel.x + topPanel.width)){
						bnd.x = topPanel.x;
						bnd.y += bnd.height + 2;
					}
					if((bnd.y + bnd.height*2) > (topPanel.y + topPanel.height)) break;
				}
				//CutTop(&topPanel, bnd.y+bnd.height-topPanel.y );

				bnd = CutBottom(&topPanel, 26);
				if(GuiButton(CutRight(&bnd, 32), ">")){
					lv_editor.spritePageOffset = spriteLastFound+1;
				}
				if(GuiButton(CutRight(&bnd, 32), "<") || ((!spritesOnPage) && lv_editor.spritePageOffset)){
					lv_editor.spritePageOffset = spritesOnPage ? MAX(0, lv_editor.spritePageOffset - spritesOnPage) : 0;
				}
			} break;
		}
	}

	GuiWindowBox(rightPanel, "Map editor");
	{
		Extend(&rightPanel, -6);
		CutTop(&rightPanel, 20);
		GuiLabel(CutTop(&rightPanel, 20), "Map");
		if(GuiTextBox(CutTop(&rightPanel, 26), level->filePath, sizeof(level->filePath), lv_editor.levelPathEditing)){
			lv_editor.levelPathEditing = !lv_editor.levelPathEditing;
		}
		bnd = CutTop(&rightPanel, 26);
		if(GuiButton(CutLeft(&bnd, 40), "Save")){
			SaveLevel();
		}
		if(GuiButton(CutLeft(&bnd, 40), "Open")){
			LoadLevel(NULL);
		}
		GuiLabel(CutTop(&rightPanel, 20), "Spritesheets");
		for(int i=0; i < level->sheetCount; i++){
			SpriteSheet_t* sheet = &client.sheets[level->sheets[i]];
			bnd = CutTop(&rightPanel, 26);
			if(GuiToggle(bnd, sheet->spr_path, (lv_editor.sheetIndex == i))){
				if(lv_editor.sheetIndex != i){
					lv_editor.sheetIndex = i;
				}
			}
			CutTop(&rightPanel, 2);
		}
		bnd = CutTop(&rightPanel, 26);
		if(level->sheetCount < LV_SPRITESHEETS){
			if(GuiButton(CutLeft(&bnd, 40), "+")){
				lv_editor.sheetToAddEditing = true;
				lv_editor.sheetToAddPath[0] = 0;
			}
		}
		if(level->sheetCount && (lv_editor.sheetIndex != -1)){
			if(GuiButton(CutLeft(&bnd, 40), "-")){
				RemoveSheet(level->sheets[lv_editor.sheetIndex]);
				RemoveElement((void*)&level->sheets, lv_editor.sheetIndex, level->sheetCount, sizeof(int));
				level->sheetCount--;
			}
		}
		if(lv_editor.sheetToAddEditing){
			if(GuiTextBox(bnd, lv_editor.sheetToAddPath, sizeof(lv_editor.sheetToAddPath), true)){
				int newSheet = LoadSpriteSheet(lv_editor.sheetToAddPath, NULL);
				if(newSheet != -1){
					lv_editor.sheetIndex = level->sheetCount;
					level->sheets[level->sheetCount++] = newSheet;
				}
				lv_editor.sheetToAddEditing = false;
			}
		}

		GuiLabel(CutTop(&rightPanel, 20), "Layers");
		forEachLvLayer(layer, layerId){
			Rectangle zone = CutTop(&rightPanel, 26);
			if((lv_editor.layerSelected == layerId) && lv_editor.layerNameEditing){
				if(GuiTextBox(zone, layer->name, sizeof(layer->name), true)){
					lv_editor.layerNameEditing = false;
				}
			}else{
				if(GuiButton(CutRight(&zone, 24), layer->visible ? "v" : "x")){
					layer->visible = !layer->visible;
				}
				if(GuiToggle(zone, layer->name, lv_editor.layerSelected == layerId)){
					if(lv_editor.layerSelected != layerId){
						lv_editor.layerSelected = layerId;
						lv_editor.selection = LVED_SEL_LAYER;
					}
				}
			}
		}
		bnd = CutTop(&rightPanel, 26);
		if(level->layerCount < LV_MAX_LAYERS){
			if(GuiButton(CutLeft(&bnd, 40), "+")){
				lv_editor.layerSelected = LvAddLayer();
				lv_editor.layerNameEditing = true;
			}
		}
		if(level->layerCount > 1){
			if(GuiButton(CutLeft(&bnd, 40), "-")){
				LvRemoveLayer(lv_editor.layerSelected);
				if(lv_editor.layerSelected == level->layerCount){
					lv_editor.layerSelected--;
				}
			}
		}
		if(GuiButton(CutLeft(&bnd, 40), "R")){
			lv_editor.layerNameEditing = true;
		}


		CutTop(&rightPanel, 10);
		switch(lv_editor.selection){
			case LVED_SEL_BRUSH:
				LvBrush_t* brush = &level->brushes[lv_editor.brushSelected];
				sprintf(text, "%.2f %.2f %.2f", brush->pos.x, brush->pos.y, brush->pos.z);
				GuiLabel(CutTop(&rightPanel, 26), text);
				bnd = CutTop(&rightPanel, 26);
				bool isPhysical = (brush->flags & BRUSH_PHYSICAL);
				bool isPhysicalNew = GuiCheckBox(CutLeft(&bnd, 32), "Physical?", isPhysical);
				if(isPhysical != isPhysicalNew){
					brush->flags ^= BRUSH_PHYSICAL;
				}

				bnd = CutTop(&rightPanel, 26);
				float alpha = GuiSlider(CutRight(&bnd, 100), "Opacity", "", brush->tint.a/255.0, 0, 1);
				brush->tint.a = (char)(alpha * 255);

				CutTop(&rightPanel, 10);
				bnd = CutTop(&rightPanel, 120);
				brush->tint = GuiColorPicker(CutLeft(&bnd, 120), brush->tint);

				break;
			case LVED_SEL_ENTITY: {
				LvEntity_t* ent = &level->entities[lv_editor.entitySelected];
				
				sprintf(text, "%.2f %.2f %.2f", ent->pos.x, ent->pos.y, ent->pos.z);
				GuiLabel(CutTop(&rightPanel, 26), text);
				switch(ent->type){
					case LVENT_ROBOTSPAWN:
						GuiLabel(CutTop(&rightPanel, 26), "Robot name:");
						if(GuiTextBox(CutTop(&rightPanel, 26), ent->robot.name, sizeof(ent->robot.name), lv_editor.entityRobotNameEditing)){
							lv_editor.entityRobotNameEditing = !lv_editor.entityRobotNameEditing;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiButton(CutLeft(&bnd, 32), "<")){
							ent->robot.toyType = (ent->robot.toyType+1) % _TOY_TYPE_COUNT;
						}
						GuiLabel(bnd, toyClasses[ent->robot.toyType].name );

						bnd = CutTop(&rightPanel, 26);
						if(GuiCheckBox(CutLeft(&bnd, 32), "Immortal?", ent->robot.immortal)){
							ent->robot.immortal = true;
						}else{
							ent->robot.immortal = false;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiCheckBox(CutLeft(&bnd, 32), "Fatalizer?", ent->robot.fatalizer)){
							ent->robot.fatalizer = true;
						}else{
							ent->robot.fatalizer = false;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiCheckBox(CutLeft(&bnd, 32), "Automated?", ent->robot.automated)){
							ent->robot.automated = true;
						}else{
							ent->robot.automated = false;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiCheckBox(CutLeft(&bnd, 32), "Leader-only?", ent->robot.leaderOnly)){
							ent->robot.leaderOnly = true;
						}else{
							ent->robot.leaderOnly = false;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiValueBox(CutRight(&bnd, 100), "Health mult:", &ent->robot.healthMult, 0, 10000, lv_editor.entityRobotHealthEditing)){
							lv_editor.entityRobotHealthEditing = !lv_editor.entityRobotHealthEditing;
						}
						bnd = CutTop(&rightPanel, 26);
						if(GuiValueBox(CutRight(&bnd, 100), "Team:", &ent->robot.team, -1, 100, lv_editor.entityRobotTeamEditing)){
							lv_editor.entityRobotTeamEditing = !lv_editor.entityRobotTeamEditing;
						}
						CutTop(&rightPanel, 10);
						bnd = CutTop(&rightPanel, 150);
						bnd.x -= 50;
						ent->robot.color = GuiColorPicker(CutRight(&bnd, 120), ent->robot.color);
						break;
				}
				if(GuiDropdownBox(CutTop(&rightPanel, 26), entityTypesJoined, (int*)&ent->type, lv_editor.entityTypeSelecting)){
					lv_editor.entityTypeSelecting = !lv_editor.entityTypeSelecting;
				}

			} break;
			case LVED_SEL_LAYER: {
				LvLayer_t* layer = &level->layers[lv_editor.layerSelected];

				bnd = CutTop(&rightPanel, 26);
				if(GuiValueBox(CutRight(&bnd, 100), "Order:", &layer->order, -10000, 10000, lv_editor.layerOrderEditing)){
					lv_editor.layerOrderEditing = !lv_editor.layerOrderEditing;
				}
				bnd = CutTop(&rightPanel, 26);
				if(GuiCheckBox(CutLeft(&bnd, 32), "Additive blend", layer->blendMode == 1)){
					layer->blendMode = 1;
				}else{
					layer->blendMode = 0;
				}
			} break;
			default: {

				bnd = CutTop(&rightPanel, 26);
				if(GuiValueBox(CutRight(&bnd, 100), "Width:", &lv_editor.newMapSize[0], 16, LV_MAX_TILECOUNT, lv_editor.levelWidthEditing)){
					lv_editor.levelWidthEditing = !lv_editor.levelWidthEditing;
				}
				bnd = CutTop(&rightPanel, 26);
				if(GuiValueBox(CutRight(&bnd, 100), "Height:", &lv_editor.newMapSize[1], 16, LV_MAX_TILECOUNT, lv_editor.levelHeightEditing)){
					lv_editor.levelHeightEditing = !lv_editor.levelHeightEditing;
				}
				if(lv_editor.newMapSize[0]) level->mapSize[0] = lv_editor.newMapSize[0];
				if(lv_editor.newMapSize[1]) level->mapSize[1] = lv_editor.newMapSize[1];

				if(GuiButton(CutTop(&bnd, 26), "Terrain")){
					LvGenRandomTerrain();
				}

			} break;
		}
	}

	DrawText(bottomText, 20, client.height - 150, 20, GRAY);

	if(noedit){
		DrawChatBox();
	}

	EndMode2D();
}

#undef IMP_EDITOR_LEVEL_H
#endif