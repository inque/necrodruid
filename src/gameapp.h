#pragma once

#define GAME_TITLE "Necrodruid Showdown"
#define TICKS_PER_SEC 30
#define GET_GAME_TICKRATE() (int)(app.config.tickrate * app.speed)

//multipliers that affect unit movement and abilities
// #define MUL_FRAMES 1
// #define MUL_FORCE 1.5
#define MUL_FRAMES app.config.tmpMulFrames
#define MUL_FORCE app.config.tmpMulForce

//stiffness of the physics
#define SPACE_DRAG 0.4

#define USECARD_COOLDOWN 30*TICKS_PER_SEC

//location approximation for fast search
#define SECTOR_SIZE 512
#define APPROX_NEAR(a, b, max) ((abs(a[0]-b[0]) + abs(a[1]-b[1])) <= max)

#define MAX_BOYS 32
#define MAX_TOYS 1024
#define MAX_ITEMS 128

#include "settings.h"
#include "a_boy.h"
#include "a_toy.h"
#include "an_item.h"
#include "physics.h"
#include "gameclient.h"
#include "level.h"
#include "raylib.h"
#include "stdlib.h"

typedef enum{
	ROLE_LOCAL,
	ROLE_HOST,
	ROLE_REMOTE
} AppRole_e;

typedef enum{
	STATE_LOBBY,
	STATE_PREPARING,
	STATE_STARTING,
	STATE_PLAYING,
	STATE_ENDED
} GameState_e;

typedef enum{
	MODE_STORY,
	MODE_BATTLE,
	MODE_SURVARIUM
} GameMode_e;

typedef struct{
	bool initialized;
	int frame;
	int timer;
	float progress;
	GameState_e state;
	GameMode_e mode;
	Boy_t boys[MAX_BOYS];
	Toy_t toys[MAX_TOYS];
	Item_t items[MAX_ITEMS];
	Level_t level;
	Space_t space;
} GameState_t;

typedef struct{
	char configPath[64];
	AppConfig_t config;
	bool headless;
	bool shouldRestartGame;
	float speed;
	AppRole_e role;
	int playerCount;
} AppState_t;

extern AppState_t app;
extern GameState_t game;

#include "macros.h"

void GameTick();
void DrawGraphics();
void StartGame(AppRole_e role, const char* map);
void ResetGame();
void CloseGame(bool hard);

#ifdef _MSC_VER
	//disable float conversion warnings
	#pragma warning(disable:4244 4305)
#endif
