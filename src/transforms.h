#ifndef TRANSFORMS_H
#include "stdbool.h"
#include "raymath.h"
#include "physics.h"

// dimensions:    2.5d
// interpolation: linear

typedef Vector3 Position_t;
typedef float   Orientation_t;

typedef struct{
	Vector3 pos[2];
	float angle[2];
	Vector3 size;
} Transform_t;

// get screen coordinates in our custom perspective
Vector2 Project2d(Vector3 vec);
Vector3 Project3d(Vector3 vec);
Vector3 Unproject2d(Vector2 vec, float z);

void PinTransform(Transform_t* tform, Vector3 pos, float angle, Vector3 size);
void PetrifyTransform(Transform_t* tform); // prevents flicker when it stops updating
void UpdateTransform(Transform_t* tform, cpBody* body);
void UpdateTransformPos(Transform_t* tform, Vector3 pos);
Vector3 PeekPos(Transform_t* tform);   // no interpolation
Vector3 GetPos(Transform_t* tform);    // interpolated
Vector2 GetPos2d(Transform_t* tform);  // interpolated, projection
float PeekAngle(Transform_t* tform);   // no interpolation, radians
float GetAngle(Transform_t* tform);    // interpolated, radians
float GetAngleDeg(Transform_t* tform); // interpolated, degrees

Rectangle GetBounds(Transform_t* tform);
bool DoesIntersectRect(Transform_t* tform, Rectangle rec);
bool IsVisible(Transform_t* tform);
void PinTransformZ(Transform_t* tform, float z);

#define TRANSFORMS_H
#endif
#ifdef IMP_TRANSFORMS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "math.h"

#define BOILERPLATE() int a = game.frame & 1; int b = 1-a;

void PinTransform(Transform_t* tform, Vector3 pos, float angle, Vector3 size){
	for(int i=0; i < 2; i++){
		tform->pos[i] = pos;
		tform->angle[i] = angle;
	}
	tform->size = size;
}

void PetrifyTransform(Transform_t* tform){
	BOILERPLATE();
	tform->pos[b] = tform->pos[a];
	tform->angle[b] = tform->angle[a];
}

void UpdateTransform(Transform_t* tform, cpBody* body){
	BOILERPLATE();
	cpVect pos = cpBodyGetPosition(body);
	tform->pos[a] = (Vector3){pos.x, pos.y, tform->pos[a].z};
	tform->angle[a] = cpBodyGetAngle(body);
}

void UpdateTransformPos(Transform_t* tform, Vector3 pos){
	BOILERPLATE();
	tform->pos[a] = pos;
}

Vector3 PeekPos(Transform_t* tform){
	BOILERPLATE();
	return tform->pos[a];
}

Vector3 GetPos(Transform_t* tform){
	BOILERPLATE();
	if(app.config.interpolation){
		return Vector3Lerp(tform->pos[a], tform->pos[b], client.deltaFrame);
	}else{
		return tform->pos[a];
	}
}

Vector2 Project2d(Vector3 vec){
	return (Vector2){
		vec.x,
		(vec.y * app.config.persp_y) - (vec.z * app.config.persp_z)
	};
}

Vector3 Project3d(Vector3 vec){
	Vector2 vec2 = Project2d(vec);
	return (Vector3){
		vec2.x, vec2.y,
		(vec.y * (1-app.config.persp_y)) - (vec.z * (1-app.config.persp_z))
	};
}

Vector3 Unproject2d(Vector2 vec, float z){
	//fixme: z???
	return (Vector3){
		vec.x,
		(vec.y / app.config.persp_y),
		(z / app.config.persp_z)
	};
}

Vector2 GetPos2d(Transform_t* tform){
	Vector3 pos = GetPos(tform);
	return Project2d(pos);
}

float PeekAngle(Transform_t* tform){
	BOILERPLATE();
	return tform->angle[a];
}

float GetAngle(Transform_t* tform){
	BOILERPLATE();
	if(app.config.interpolation){
		return tform->angle[b] + client.deltaFrame * (tform->angle[b] - tform->angle[a]);
	}else{
		return tform->angle[a];
	}
}

float GetAngleDeg(Transform_t* tform){
	return GetAngle(tform) * RAD2DEG;
}

Rectangle GetBounds(Transform_t* tform){
	BOILERPLATE();
	return (Rectangle){
		tform->pos[a].x - tform->size.x / 2,
		tform->pos[a].y*app.config.persp_y - tform->size.y*app.config.persp_y / 2,
		tform->size.x,
		tform->size.y*app.config.persp_y
	};
	//todo: angle
}

bool DoesIntersectRect(Transform_t* tform, Rectangle rec){
	BOILERPLATE();
	return CheckCollisionRecs(GetBounds(tform), rec);
}

bool IsVisible(Transform_t* tform){
	return DoesIntersectRect(tform, client.viewBounds);
}

void PinTransformZ(Transform_t* tform, float z){
	tform->pos[0].z = z;
	tform->pos[1].z = z;
}

#undef IMP_TRANSFORMS_H
#endif