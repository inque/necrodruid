#ifndef ASSETS_H
#include "raylib.h"
#include "audio.h"

typedef struct{
	struct {
		Shader basic;
	} shaders;
	struct {
		Texture2D terrain;
	} textures;
	struct {
		int card;
		int lightning;
		int insignia;
		int aimCursor;
		int gunfire;
		int gunsmoke;
		int shotgunPellet;
		int shadowMedium;
		int shadowSmall;
		int menuBg;
	} sprite;
	struct{
		Sound_t fatality;
		Sound_t victory;
		Sound_t click[2];
		Sound_t success;
		Sound_t shotgunCock;
		Sound_t shotgunShot;
		Sound_t whoosh;
		Sound_t whoosh2;
		Sound_t bodyImpact;
	} sounds;
} AppAssets_t;

void LoadAssets();

#define ASSETS_H
#endif
#ifdef IMP_ASSETS_H
//==== implementation starts here ====

#include "gameapp.h"
#include "shadersrc.h"
#include "an_item.h"
#include "stdarg.h"

static inline void AddToLookup(SpriteLookup_t** cur, int* out, const char* format, ...){
	va_list(args);
	va_start(args, format);
	vsprintf((*cur)->name, format, args);
	va_end(args);
	(*cur)->outSpriteId = out;
	*cur += 1;
}

static void LoadCharacters(){
	SpriteLookup_t toyLookup[_TOY_TYPE_COUNT * TOYCLASS_SPRITES_PER_SHEET + 1];
	SpriteLookup_t* toyLookupCur = &toyLookup[0];

	for(int toyType=0; toyType < _TOY_TYPE_COUNT; toyType++){
		ToyClass_t* toyClass = GetToyClass(toyType);

		AddToLookup(&toyLookupCur, &toyClass->sprite.avatar, "unit_%i_avatar", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.icon, "unit_%i_icon", toyType);

		AddToLookup(&toyLookupCur, &toyClass->sprite.idle, "unit_%i_idle", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.fall, "unit_%i_fall", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.death, "unit_%i_death", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.fatality, "unit_%i_fatality", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.spawn, "unit_%i_spawn", toyType);
		AddToLookup(&toyLookupCur, &toyClass->sprite.usecard, "unit_%i_card", toyType);

		for(int i=0; i < 4; i++){
			AddToLookup(&toyLookupCur, &toyClass->sprite.run[i], "unit_%i_run:%i", toyType, i);
			AddToLookup(&toyLookupCur, &toyClass->sprite.dash[i], "unit_%i_dash:%i", toyType, i);
			AddToLookup(&toyLookupCur, &toyClass->sprite.attack[0][i], "unit_%i_attack:%i", toyType, i);
			AddToLookup(&toyLookupCur, &toyClass->sprite.attack[1][i], "unit_%i_attack2:%i", toyType, i);
			AddToLookup(&toyLookupCur, &toyClass->sprite.attackRel[0][i], "unit_%i_attack_rel:%i", toyType, i);
			AddToLookup(&toyLookupCur, &toyClass->sprite.attackRel[1][i], "unit_%i_attack2_rel:%i", toyType, i);
		}
	}
	*toyLookupCur = (SpriteLookup_t){0}; //null-terminate

	LoadSpriteSheet("data/gfx/chartest.spr", (SpriteLookup_t*)&toyLookup);
}

static void LoadItems(){
	SpriteLookup_t lookup[_ITEM_TYPE_COUNT * ITEMCLASS_SPRITES_PER_SHEET + 1];
	SpriteLookup_t* lookupCur = &lookup[0];

	for(int itemType=0; itemType < _ITEM_TYPE_COUNT; itemType++){
		ItemClass_t* itemClass = &itemClasses[itemType];

		AddToLookup(&lookupCur, &itemClass->sprite.icon, "item_%i_icon", itemType);
		AddToLookup(&lookupCur, &itemClass->sprite.ammo, "item_%i_ammo", itemType);
		AddToLookup(&lookupCur, &itemClass->sprite.hold, "item_%i_hold", itemType);
		AddToLookup(&lookupCur, &itemClass->sprite.aimed, "item_%i_aimed", itemType);
	}
	*lookupCur = (SpriteLookup_t){0};

	LoadSpriteSheet("data/gfx/items.spr", (SpriteLookup_t*)&lookup);
}

static void LoadSounds(){
	MyLoadSound(&client.assets.sounds.fatality, "data/sfx/fatality1.ogg");
	MyLoadSound(&client.assets.sounds.victory, "data/sfx/victory.ogg");
	MyLoadSound(&client.assets.sounds.click[0], "data/sfx/click1.ogg");
	MyLoadSound(&client.assets.sounds.click[1], "data/sfx/click2.ogg");
	MyLoadSound(&client.assets.sounds.success, "data/sfx/success.ogg");
	MyLoadSound(&client.assets.sounds.shotgunCock, "data/sfx/cock.ogg");
	MyLoadSound(&client.assets.sounds.shotgunShot, "data/sfx/shotgun.ogg");
	MyLoadSound(&client.assets.sounds.whoosh, "data/sfx/whoosh.ogg");
	MyLoadSound(&client.assets.sounds.whoosh2, "data/sfx/whoosh2.ogg");
	MyLoadSound(&client.assets.sounds.bodyImpact, "data/sfx/bodyimpact.ogg");

	MyLoadMusic(&client.audio.music[0].music, "data/sfx/music_ambience.ogg");
	MyLoadMusic(&client.audio.music[1].music, "data/sfx/music_combat.ogg");
}

void LoadAssets(){
	if(!app.headless){
		client.assets.textures.terrain = LoadTexture("data/gfx/terrain.png");
		client.assets.shaders.basic = LoadShaderFromMemory(VS_BASIC, FS_BASIC);
		{
			SpriteLookup_t uiLookup[] = {
				{.name = "card",       .outSpriteId = &client.assets.sprite.card},
				{.name = "lightning",  .outSpriteId = &client.assets.sprite.lightning},
				{.name = "insignia",   .outSpriteId = &client.assets.sprite.insignia},
				{.name = "aim_cursor", .outSpriteId = &client.assets.sprite.aimCursor},
				{.name = "gunfire",    .outSpriteId = &client.assets.sprite.gunfire},
				{.name = "gunsmoke",   .outSpriteId = &client.assets.sprite.gunsmoke},
				{.name = "shotgun_pellet", .outSpriteId = &client.assets.sprite.shotgunPellet},
				{.name = "menu_bg",    .outSpriteId = &client.assets.sprite.menuBg},
				{0}
			};
			LoadSpriteSheet("data/gfx/ui.spr", (SpriteLookup_t*)&uiLookup);
		}
		{
			SpriteLookup_t transLookup[] = {
				{.name = "shadow_med",   .outSpriteId = &client.assets.sprite.shadowMedium},
				{.name = "shadow_small", .outSpriteId = &client.assets.sprite.shadowSmall},
				{0}
			};
			LoadSpriteSheet("data/gfx/trans.spr", (SpriteLookup_t*)&transLookup);
		}
		LoadSpriteSheet("data/gfx/cutscenes.spr", NULL);
		LoadCharacters();
		LoadItems();
		LoadSounds();
	}
}


#undef IMP_ASSETS_H
#endif
