#ifndef PARTICLES_H
#include "raymath.h"

#define MAX_PARTICLES 1024

typedef enum{
	PARTICLE_NONE,
	PARTICLE_BONUS,
	PARTICLE_BLOOD,
	PARTICLE_CARD,
	PARTICLE_LIGHTNING,
	PARTICLE_SPARKY,
	PARTICLE_GUNFIRE,
	PARTICLE_GUNSMOKE,
	PARTICLE_PELLET_SHOTGUN
} ParticleType_e;

typedef struct{
	int type;
	float life;
	Vector3 pos;
	Vector3 velocity;
} Particle_t;

typedef struct{
	int particleCount, nextFreeParticle;
	Particle_t particles[MAX_PARTICLES];
} ParticleSystem_t;

Particle_t* AddParticle(ParticleSystem_t* fx, ParticleType_e type, Vector3 pos, float life, float randomPosRadius, Vector3 minVelocity, Vector3 maxVelocity);
void DrawParticles(ParticleSystem_t* fx);

#define PARTICLES_H
#endif
#ifdef IMP_PARTICLES_H
//==== implementation starts here ====
#include "gameapp.h"
#include "graphics.h"
#include "utils.h"

Particle_t* AddParticle(ParticleSystem_t* fx, ParticleType_e type, Vector3 pos, float life, float randomPosRadius, Vector3 minVelocity, Vector3 maxVelocity){
	for(int partId = fx->nextFreeParticle; partId < (MAX_PARTICLES-1); partId++){
		Particle_t* part = &fx->particles[partId];
		if(part->type != PARTICLE_NONE) continue;

		part->type = type;
		part->pos = pos;
		{
			float a = 2*3.14*randf();
			float d = randomPosRadius*randf();
			part->pos.x += d*sinf(a);
			part->pos.y += d*cosf(a);
		}
		part->velocity.x = minVelocity.x + (maxVelocity.x - minVelocity.x) * randf();
		part->velocity.y = minVelocity.y + (maxVelocity.y - minVelocity.y) * randf();
		part->velocity.z = minVelocity.z + (maxVelocity.z - minVelocity.z) * randf();
		part->life = life;

		fx->nextFreeParticle = partId+1;
		if(fx->nextFreeParticle >= MAX_PARTICLES){
			fx->nextFreeParticle = 0;
		}
		if(partId >= fx->particleCount){
			fx->particleCount = partId+1;
		}
		return part;
	}
	//return NULL;
	return &fx->particles[0];
}

static void RemoveParticle(ParticleSystem_t* fx, int id){
	fx->particles[id].type = PARTICLE_NONE;
	if(id < fx->nextFreeParticle){
		fx->nextFreeParticle = id;
	}
	if(id == (fx->particleCount-1)){
		fx->particleCount--;
	}
}

void DrawParticles(ParticleSystem_t* fx){

	for(int partId=0; partId < fx->particleCount; partId++){
		Particle_t* part = &fx->particles[partId];
		if(part->type == PARTICLE_NONE) continue;
		//Vector3 pos = Vector3Add(part->pos, Vector3Scale(part->velocity, client.deltaFrame));
		Vector3 pos = part->pos;
		Vector3 pos2 = Vector3Add(pos, Vector3Scale(part->velocity, 40));
		//part->pos = Vector3Add(part->pos, part->velocity);
		part->pos = Vector3Lerp(pos, pos2, SmoothOver(0.9, 0.5, client.deltaTime));

		switch(part->type){
			case PARTICLE_BONUS: {
				float opacity = part->life/100.0;
				float size = 10+(6+2*(partId&3))*0.3*(100-part->life);
				Color color = {255,230,130,255};
				DrawRectangle(pos.x-size/2, pos.y-size/2, size, size, Fade(color, opacity*opacity));
			} break;
			case PARTICLE_BLOOD: {
				Vector3 endPoint = Vector3Subtract(pos, Vector3Scale(part->velocity, 10.0f));
				DrawLineEx(Project2d(pos), Project2d(endPoint), 4.0f, (Color){247, 40, 43, 190});
				//part->velocity.x *= 0.97f;
				part->velocity.z -= 0.005f * client.deltaFrame;
			} break;
			case PARTICLE_CARD: {
				float period = 0.4+0.3*(partId&3);
				float period2 = period*1.2;
				float t = fmodf(5*partId+2.5*client.time, period) / period;
				float t2 = fmodf(3*partId+2.5*client.time, period2) / period2;
				float a = MIN(part->life/70, 1);

				BeginTransformEx(Vec2(pos), t2, fmodf(t*2, 1), 1);
				BlitSprite(client.assets.sprite.card, (t>0.5)?1:0, Vector2Zero(), Fade(WHITE, a), 0.3);
				EndTransform();
				part->velocity.y += 0.008f * client.deltaFrame;
			} break;
			case PARTICLE_LIGHTNING: {
				int frame = (partId + (int)(part->life/(40+20*(partId%3))) ) % 17;
				float a = MIN(part->life/70, 1);
				if(part->life > 100){
					if(((game.frame + 2*frame)&15) == 0){
						Vector3 newPos = {part->pos.x+80*sinf(21*partId), part->pos.y, part->pos.z+30+80*fabsf(sinf(13*partId))};
						AddParticle(fx, PARTICLE_LIGHTNING, newPos, part->life/3, 8, Vector3Zero(), Vector3Zero());
					}
				}
				BlitSprite(client.assets.sprite.lightning, frame, Project2d(pos), Fade(WHITE, a), 1);
			} break;
			case PARTICLE_SPARKY: {
				bool quirky = !!(partId & 5);
				const Color colors[] = {ORANGE, YELLOW, BLUE, RAYWHITE};
				float a = 0.6 * MIN(part->life/70, 1);
				Color color = Fade(colors[partId % 4], a);
				Vector2 pos2 = Project2d(pos);
				float size = 4;
				Vector2 points1[] = {
					{pos2.x, pos2.y-size}, {pos2.x, pos2.y+size},
					{pos2.x-size, pos2.y}, {pos2.x+size, pos2.y}
				};
				Vector2 points2[] = {
					{pos2.x-size, pos2.y-size}, {pos2.x+size, pos2.y+size},
					{pos2.x+size, pos2.y-size}, {pos2.x-size, pos2.y+size}
				};

				DrawLineEx(quirky ? points2[0] : points1[0], quirky ? points2[1] : points1[1], 4, color);
				DrawLineEx(quirky ? points2[2] : points1[2], quirky ? points2[3] : points1[3], 4, color);
				part->velocity.z -= 0.015f * client.deltaFrame;
			} break;
			case PARTICLE_GUNFIRE: {
				float a = 0.7 * MIN(part->life/60, 1);
				int dir4 = MatchDirection(Vec2(part->velocity), 4);
				Vector3 posScreen = Project3d(pos);
				DrawSprite(client.assets.sprite.gunfire, dir4, posScreen, Fade(WHITE, 1-powf(1-a,2) ), 1);
			} break;
			case PARTICLE_GUNSMOKE: {
				float a = 0.6 * MIN(part->life/120, 1);
				int frame = MIN(6, (1 - part->life / 200) * 6);
				Vector3 posScreen = Project3d(pos);
				DrawSprite(client.assets.sprite.gunsmoke, frame, posScreen, Fade(WHITE, a), 1);
			} break;
			case PARTICLE_PELLET_SHOTGUN: {
				float a = MIN(part->life/50, 1);
				int frame = (int)(part->life / 4) % 5;
				float terrainHeight = TerrainHeightAt(Vec2(pos));
				Vector3 posScreen = Project3d(pos);
				DrawSprite(client.assets.sprite.shotgunPellet, frame, posScreen, Fade(WHITE, a), 1);
				part->velocity.z -= 0.3175f * client.deltaFrame;
				if(pos.z < terrainHeight){
					part->velocity.z *= -1;
					pos.z = terrainHeight + 0.01;
				}
			}
		}
		
		part->life -= 2 * client.deltaFrame;
		if(part->life <= 0){
			RemoveParticle(fx, partId);
		}
	}
}

#undef IMP_PARTICLES_H
#endif
