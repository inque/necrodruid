#pragma once
#include "stdbool.h"
#include "raylib.h"

#define MAX_SHEETS 32
#define MAX_SHAPES 2048
#define MAX_SPRITES 1024

#define MAX_SHAPE_VERTS 16
#define MAX_SHAPE_INDICES 24*3
#define MAX_SPRITE_FRAMES 24

typedef struct{
	int active;
    char spr_path[64];
	char texname[24];
    Texture2D tex;
} SpriteSheet_t;

typedef struct{
	int active;
	Vector2 uv;
	int vertCount;
	Vector2 verts[MAX_SHAPE_VERTS];
	int indicesUsed;
	char indices[MAX_SHAPE_INDICES];
} SpriteShape_t;

typedef struct{
	int active;
    char name[24];
	int sheetId;
	int framesUsed;
	int shapeId[MAX_SPRITE_FRAMES];
} Sprite_t;

typedef struct{
	char name[24];
	int* outSpriteId;
} SpriteLookup_t;

int LoadSpriteSheet(const char* path, SpriteLookup_t* lookupTable);
void ReloadSheet(int sheetId);
void RemoveSheet(int sheetId);
int FindSpriteByName(const char* name);
void DrawSprite(int spriteId, int frame, Vector3 pos, Color tint, float size);
void BlitSprite(int spriteId, int frame, Vector2 pos, Color tint, float size);

void BeginDrawingSprites();
void EndDrawingSprites();

void BeginSpriteCutoutHack();
void EndSpriteCutoutHack();
