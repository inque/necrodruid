#include "gameapp.h"
#include "gameplay.h"
#include "menus.h"
#include "devtools.h"
#include "gameinterface.h"
#include "fogofwar.h"
#include "masterserver.h"
#include "level_ops.h"
#include "raymath.h"
#include "stdbool.h"
#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#ifdef __EMSCRIPTEN__
	#include <emscripten.h>
#endif


AppState_t app = {
	.configPath = "options.txt",
	.speed = 1
};
GameState_t game;

void GameTick(){

	if(app.shouldRestartGame){
		ResetGame();
		app.shouldRestartGame = false;
	}

	UpdateNetworking();

	if(!app.headless){
		client.lastTickTime = GetTime();
		if(app.role == ROLE_LOCAL){
			if(client.paused) return;
			if(client.ui.cutsceneCount){
				PlayerControls();
				return;
			}
		}
		if(client.screen != SCREEN_GAME) return;
	}
	
	if(app.role == ROLE_REMOTE) return;
	if(app.role == ROLE_HOST){
		MasterUpdate(false, true);
	}

	UpdatePhysics();
	if(!app.headless) PlayerControls();
	UpdateBoys();
	UpdateToys();
	UpdateItems();
	UpdateLevel();
	UpdateGameplay();

	game.frame++;
}

void DrawGraphics(){
	if(app.headless) return;
	if((client.screen == SCREEN_GAME) && (LvGetVar("finalcredits") == 1) && (!client.ui.cutsceneCount)){
		client.screen = SCREEN_MAINMENU;
		client.submenu = SUBMENU_CREDITS;
	}
	UpdateAudio();
	UpdateClient();
	BeginDrawing();
	switch(client.screen){
		case SCREEN_MAINMENU: {
			const bool skipMenu = false;
			if(skipMenu){
				client.screen = SCREEN_GAME;
				StartGame(ROLE_LOCAL, "spc_1.nmp");
				game.mode = MODE_STORY;
			}else{
				MenuBackground();
				MainMenu();
			}
		} break;
		case SCREEN_GAME: {
			ClearBackground(game.level.bgColor);
			BeginMode2D(client.cam);
				DrawLevelTerrain();
					BeginDrawingSprites();
						DrawLevel(-9999, -1);
					EndDrawingSprites();
					BeginDrawingSprites();
						DrawLevel(0, 0);
						DrawToys();
						DrawItems();
						DrawLevel(1, 99);
						DrawParticles(&client.fxWorldOrdered);
					EndDrawingSprites();
					BeginDrawingSprites();
						DrawParticles(&client.fxWorld);
					EndDrawingSprites();
					DrawLevel(100, 9999);
					DrawFog();
					DrawInterfaceWorld();
				DebugWorld();
			EndMode2D();
			BeginMode2D(client.uiCam);
				DrawParticles(&client.fxScreen);
				if(IsKeyPressed(KEY_ESCAPE)) client.paused = !client.paused;
				if(game.state == STATE_LOBBY){
					LobbyScreen();
				}else{
					if(client.paused){
						ShadeScreen();
						PauseMenu();
					}else{
						if(client.ui.cutsceneCount){
							DrawCutscene();
						}else{
							DrawInterfaceScreen();
							ShadeScreen();
						}
					}
				}
				DebugOverlay();
				DrawChatBox();
			EndMode2D();
		} break;
		case SCREEN_EDITOR: {
			ClearBackground(game.level.bgColor);
			EditorScreen();
		}
	}
	EndDrawing();
}

void StartGame(AppRole_e role, const char* map){
	memset(&game, 0, sizeof(game));
	app.role = role;
	if(map) strncpy(game.level.filePath, map, sizeof(game.level.filePath));

	if(role != ROLE_REMOTE){
		if(!app.headless){
			Boy_t* boy;
			client.playerBoyId = CreateBoy(false);
			boy = GetBoy(client.playerBoyId);
			memcpy(boy->name, app.config.playerName, sizeof(app.config.playerName));
			boy->isOperator = 1;
		}
	}
	ResetGame();
}

void ResetGame(){
	game.state = STATE_LOBBY;
	game.frame = 0;
	game.timer = 0;

	if(!game.initialized){
		game.initialized = true;
		InitializePhysics();
	}else{
		CloseGame(false);
		#ifdef __EMSCRIPTEN__
			//web: save settings on every reset
			SaveSettings();
		#endif
	}
	//LoadLevel(NULL);
	//LoadLevel("test.nmp");
	//LoadLevel("spc_2.nmp");
	//LoadLevel("spc_3.nmp");
	//LoadLevel("spc_4.nmp");
	LoadLevel(NULL);

	if(!app.headless){
		ResetClient();
	}
}

void CloseGame(bool hard){
	forEachToy(toy, toyId){
		RemoveToy(toyId);
	}
	forEachBoy(boy, boyId){
		if(hard){
			RemoveBoy(boyId);
		}else{
			if(boy->isRobot){
				RemoveBoy(boyId);
			}else{
				ResetBoyStats(boyId);
			}
		}
	}
	forEachItem(item, itemId){
		RemoveItem(itemId);
	}
	ClearLevel();
}
