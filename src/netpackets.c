#include "gameapp.h"
#include "netpackets.h"
#include "stdio.h"
#define _USE_MATH_DEFINES
#include "math.h"

#ifdef _MSC_VER
#define isnanf isnan
#endif

void PackTform(PackedTransform_t* dst, Transform_t* src){
	Vector3 pos = GetPos(src);
	dst->pos[0] = pos.x;
	dst->pos[1] = pos.y;
	dst->pos[2] = pos.z;
	//double srcAngle = fmod(GetAngle(src) / (2*M_PI), 1);
	//dst->angle = (uint16_t)(UINT16_MAX * srcAngle);
}

void UnpackTform(Transform_t* dst, PackedTransform_t* src){
	Vector3 pos = (Vector3){src->pos[0], src->pos[1], src->pos[2]};
	UpdateTransformPos(dst, pos);
	//SetTransformData(dst, pos, src->angle);
	//double srcAngle = ((double)src->angle / UINT16_MAX) * (2*M_PI);
	//SetTransformData(dst, pos, src->angle);
}

