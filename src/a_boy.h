#pragma once
#include "networking.h"
#include "raylib.h"
#include "raymath.h"
#include "a_card.h"
#include "level.h"

#define BOY_MAX_CARDS 32

typedef enum{
	BTN_FIRE    = 1,
	BTN_USE     = 2,
	BTN_JUMP    = 4,
	BTN_ALTFIRE = 8
} BoyInputButton_e;

typedef enum{
	BOY_ACTION_NONE,
	BOY_ACTION_USECARD,
	BOY_ACTION_READY
} BoyInputAction_e;

typedef struct{
	float axes[6]; // 0x1 - move. 2x3 - look dir.
	int buttons;
	int buttonsPrev;
	struct{
		BoyInputAction_e type;
		union{
			struct{
				int num; ///not slot id!
			} useCard;
		};
	} action;
} BoyInputs_t;

typedef struct{
	int active;
	char name[48];
	int toyId;
	int heroId;
	int team;
	int isOperator;
	int isRobot;
	bool leaderOnly;
	bool ready;
	PlayCard_t cards[BOY_MAX_CARDS];
	BoyInputs_t inputs;
	struct{
		Color color;
	} appearance;
	struct{
		char visible[LV_MAX_TILECOUNT][LV_MAX_TILECOUNT];
		char visited[LV_MAX_TILECOUNT][LV_MAX_TILECOUNT];
	} fog;
	PlayerSession_t session; ///net connection
} Boy_t;

int CreateBoy(int isRobot);
void RemoveBoy(int id);
void SyncBoy(int boyId);
void ResetBoyStats(int boyId);
void UpdateBoys();
