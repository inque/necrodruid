#include "gameapp.h"
#include "geometry.h"
#include "graphics.h"
#include "level_ops.h"
#include "string.h"
#include "stdio.h"


bool LoadLevel(const char* path){
	char actualPath[96];
	bool success = false;
	Level_t* level = &game.level;
	FILE* file = NULL;

	level->bgColor = BLACK;
	level->mapSize[0] = 64;
	level->mapSize[1] = 32;
	memset(level->terrainHeight, 128, sizeof(level->terrainHeight));
	//memset(level->terrainColor, 0, sizeof(level->terrainColor));
	level->sheetCount = 0;
	level->brushCount = 0;
	level->layerCount = 0;
	level->entityCount = 0;
	level->varCount = 0;
	level->scriptCount = 0;
	memset(level->vars, 0, sizeof(level->vars));

	if(path) strncpy(level->filePath, path, sizeof(level->filePath));
	if(level->filePath[0]){
		snprintf(actualPath, sizeof(actualPath), "maps/%s", level->filePath);
		file = fopen(actualPath, "rb");
	}

	if(file){
		int version;
		long blockStart;
		size_t read;
		uint32_t data[2];
		MapFile_header_t header;
		MapFile_sheetData_t spriteSheetData;
		MapFile_brushData_t brushData;
		MapFile_layerData_t layerData;
		MapFile_entityData_t entityData;
		MapFile_scriptData_t scriptData;

		fread(&data, 4, 2, file);
		if(data[0] != CHR4('n','M','A','P')) return true;
		version = data[1];

		// header
		blockStart = ftell(file);
		read = fread(&header, sizeof(header), 1, file);
		if(header.blockLength < sizeof(header)){
			MapFile_header_t* header2 = &header;
			AddPtr(&header2, header.blockLength);
			memset(header2, 0, (sizeof(header) - header.blockLength));
		}
		fseek(file, blockStart + header.blockLength, SEEK_SET);
		if(read){
			if(header.width > LV_MAX_TILECOUNT)      goto loadLevelFinished;
			if(header.height > LV_MAX_TILECOUNT)     goto loadLevelFinished;
			if(header.sheetCount > LV_SPRITESHEETS)  goto loadLevelFinished;
			if(header.brushCount > LV_MAX_BRUSHES)   goto loadLevelFinished;
			if(header.layerCount > LV_MAX_LAYERS)    goto loadLevelFinished;
			if(header.entityCount > LV_MAX_ENTITIES) goto loadLevelFinished;
			if(header.scriptCount > LV_SCRIPTS)      goto loadLevelFinished;
			level->mapSize[0] = header.width;
			level->mapSize[1] = header.height;
			level->sheetCount = header.sheetCount;
			level->brushCount = header.brushCount;
			level->layerCount = header.layerCount;
			level->entityCount = header.entityCount;
			level->scriptCount = header.scriptCount;
		} else goto loadLevelFinished;

		// sprite sheets
		for(int i=0; i < level->sheetCount; i++){
			blockStart = ftell(file);
			read = fread(&spriteSheetData, sizeof(spriteSheetData), 1, file);
			fseek(file, blockStart + spriteSheetData.blockLength, SEEK_SET);
			if(read){
				//todo: sanitize path
				level->sheets[i] = LoadSpriteSheet(spriteSheetData.sprPath, NULL);
			} else goto loadLevelFinished;
		}

		// brushes
		for(int i=0; i < level->brushCount; i++){
			blockStart = ftell(file);
			read = fread(&brushData, sizeof(brushData), 1, file);
			fseek(file, blockStart + brushData.blockLength, SEEK_SET);
			if(read){
				LvBrush_t* brush = &level->brushes[i];
				brush->spriteId = FindSpriteByName(brushData.spriteName);
				brush->spriteFrame = brushData.spriteFrame;
				brush->tint = *(Color*)&brushData.tint;
				brush->pos = *(Vector3*)&brushData.pos;
				brush->size = brushData.size;
				brush->z1 = brushData.z1;
				brush->z2 = brushData.z2;
				brush->flags = brushData.flags;
			} else goto loadLevelFinished;
		}

		// layers
		for(int i=0; i < level->layerCount; i++){
			blockStart = ftell(file);
			read = fread(&layerData, sizeof(layerData), 1, file);
			fseek(file, blockStart + layerData.blockLength, SEEK_SET);
			if(read){
				LvLayer_t* layer = &level->layers[i];
				strncpy((char*)&layer->name, layerData.name, sizeof(layerData.name));
				layer->visible = layerData.flags & 1;
				layer->blendMode = (layerData.flags & 2) ? 1 : 0;
				layer->order = layerData.order;
				layer->brushStartId = layerData.brushStartId;
				layer->brushCount = layerData.brushCount;
			} else goto loadLevelFinished;
		}

		//entities
		for(int i=0; i < level->entityCount; i++){
			blockStart = ftell(file);
			read = fread(&entityData, sizeof(entityData), 1, file);
			fseek(file, blockStart + entityData.blockLength, SEEK_SET);
			if(read){
				LvEntity_t* ent = &level->entities[i];
				memset(ent, 0, sizeof(LvEntity_t));
				ent->type = entityData.type;
				ent->pos.x = entityData.pos[0];
				ent->pos.y = entityData.pos[1];
				ent->pos.z = entityData.pos[2];
				switch(ent->type){
					case LVENT_ROBOTSPAWN:
						memcpy(ent->robot.name, entityData.robot.name, sizeof(ent->robot.name));
						ent->robot.toyType = entityData.robot.toyType;
						ent->robot.color = *(Color*)&entityData.robot.color;
						ent->robot.color.a = 255;
						ent->robot.healthMult = entityData.robot.healthMult;
						ent->robot.team = entityData.robot.team;
						ent->robot.immortal = !!(entityData.robot.flags & 1);
						ent->robot.fatalizer = !!(entityData.robot.flags & 2);
						ent->robot.automated = !!(entityData.robot.flags & 4);
						ent->robot.leaderOnly = !!(entityData.robot.flags & 8);
						break;
				}
			} else goto loadLevelFinished;
		}

		// terrain
		for(int iy=0; iy < level->mapSize[1]; iy++)
		for(int ix=0; ix < level->mapSize[0]; ix++){
			if(!fread(&level->terrainHeight[ix][iy], 1, 1, file)) goto loadLevelFinished;
			if(!fread(&level->terrainColor[ix][iy],  1, 1, file)) goto loadLevelFinished;
		}

		//script data
		for(int i=0; i < level->scriptCount; i++){
			LvScriptLine_t* exp = &level->script[i];
			memset(exp, 0, sizeof(LvScriptLine_t));
			//header
			blockStart = ftell(file);
			read = fread(&scriptData.header, sizeof(scriptData.header), 1, file);
			fseek(file, blockStart + scriptData.header.blockLength, SEEK_SET);
			if(read){
				exp->multiUse = (scriptData.header.flags & 1);
				memcpy(&exp->comment, &scriptData.header.comment, sizeof(exp->comment));
			} else goto loadLevelFinished;
			//condition
			blockStart = ftell(file);
			read = fread(&scriptData.condition, sizeof(scriptData.condition), 1, file);
			fseek(file, blockStart + scriptData.condition.blockLength, SEEK_SET);
			if(read){
				exp->condition.type = scriptData.condition.type;
				switch(scriptData.condition.type){
					case LVCOND_VAR_EQ:
						memcpy(&exp->condition.var.name, &scriptData.condition.var.name, sizeof(exp->condition.var.name));
						exp->condition.var.value = scriptData.condition.var.value;
						break;
					case LVCOND_TRIGGER:
						exp->condition.point.pos = (Vector3){
							scriptData.condition.point.pos[0],
							scriptData.condition.point.pos[1],
							scriptData.condition.point.pos[2]
						};
						exp->condition.point.radius = scriptData.condition.point.radius;
						break;
				}
			} else goto loadLevelFinished;
			//action
			blockStart = ftell(file);
			read = fread(&scriptData.action, sizeof(scriptData.action), 1, file);
			fseek(file, blockStart + scriptData.action.blockLength, SEEK_SET);
			if(read){
				exp->action.type = scriptData.action.type;
				switch(scriptData.action.type){
					case LVEXP_CHAT:
						memcpy(&exp->action.chat, &scriptData.action.chat, sizeof(exp->action.chat));
						break;
					case LVEXP_CUTSCENE:
						exp->action.cutscene.global = (scriptData.action.cutscene.flags & 1);
						memcpy(&exp->action.cutscene.text, &scriptData.action.cutscene.text, sizeof(exp->action.cutscene.text));
						break;
				}
			} else goto loadLevelFinished;
		}

		success = true;
		loadLevelFinished:
		fclose(file);
	}else{
		level->layers[level->layerCount++] = (LvLayer_t){
			.name = "bg",
			.order = -100,
			.visible = true
		};
		level->layers[level->layerCount++] = (LvLayer_t){
			.name = "ground",
			.order = 0,
			.visible = true
		};
		level->layers[level->layerCount++] = (LvLayer_t){
			.name = "over",
			.order = 10,
			.visible = true
		};
		level->layers[level->layerCount++] = (LvLayer_t){
			.name = "foreground",
			.order = 100,
			.visible = true
		};
		level->entities[level->entityCount++] = (LvEntity_t){
			.type = LVENT_PLAYERSPAWN,
			.pos = {0.25*(level->mapSize[0]*LV_TILESIZE), 0.5*(level->mapSize[1]*LV_TILESIZE), 0}
		};
		level->entities[level->entityCount++] = (LvEntity_t){
			.type = LVENT_PLAYERSPAWN,
			//.pos = {0.75*(level->mapSize[0]*LV_TILESIZE), 0.5*(level->mapSize[1]*LV_TILESIZE), 0}
			.pos = {0.25*(level->mapSize[0]*LV_TILESIZE)+800, 0.5*(level->mapSize[1]*LV_TILESIZE), 0}
		};
		LvGenRandomTerrain();
		success = true;
	}

	forEachLvBrush(brush, brushId){
		InitBrush(brush);
	}
	LvUpdateBounds();
	LvFixLayerGaps();

	return !success;
}

void ClearLevel(){
	Level_t* level = &game.level;
	Space_t* space = &game.space;

	for(int i=0; i < 4; i++){
		if(level->bounds[i]){
			cpSpaceRemoveShape(space, level->bounds[i]);
			level->bounds[i] = NULL;
		}
	}

	for(int i=0; i < level->sheetCount; i++){
		RemoveSheet(level->sheets[i]);
	}
	level->sheetCount = 0;

	forEachLvBrush(brush, brushId){
		//if(brush->body) cpSpaceRemoveBody(space, brush->body);
		if(brush->shape){
			cpSpaceRemoveShape(space, brush->shape);
			brush->shape = NULL;
		}
	}
	level->brushCount = 0;
	level->layerCount = 0;
	level->entityCount = 0;
	level->varCount = 0;
	level->scriptCount = 0;
}

static bool IsConditionTrue(LvScriptLine_t* exp, int* outBoyId){
	*outBoyId = -1;
	switch(exp->condition.type){
		case LVCOND_NONE:
			return true;
		case LVCOND_VAR_EQ:
			return (LvGetVar(exp->condition.var.name) == exp->condition.var.value);
		case LVCOND_TRIGGER: {
			forEachBoy(boy, boyId){
				Toy_t* toy = GetToy(boy->toyId);
				if(boy->isRobot) continue; //!
				if((boy->toyId != -1) && toy->limbs[0].body){
					if(Vector3Distance(PeekPos(&toy->limbs[0].tform), exp->condition.point.pos) < exp->condition.point.radius){
						*outBoyId = boyId;
						return true;
					}
				}
			}
			return false;
		} break;
		default:
			return false;
	}
}

static void ExecuteExpression(LvScriptLine_t* exp, int boyId){
	switch(exp->action.type){
		case LVEXP_CHAT:
			Shout(MESSAGE_CHAT, -1, exp->action.chat, 0, 0);
			break;
		case LVEXP_CUTSCENE:
			Shout(MESSAGE_CUTSCENE, exp->action.cutscene.global ? -1 : boyId, exp->action.cutscene.text, 0, 0);
			break;
	}
}

void UpdateLevel(){
	Level_t* level = &game.level;
	int boyId = -1;
	
	if( (game.state == STATE_PLAYING) || (game.state == STATE_ENDED) ){
		//scripts
		for(int expId=0; expId < level->scriptCount; expId++){
			LvScriptLine_t* exp = &level->script[expId];
			if(exp->isUsed && (!exp->multiUse)) continue;

			if(IsConditionTrue(exp, &boyId)){
				ExecuteExpression(exp, boyId);
				exp->isUsed = true;
			}
		}
	}

}

float TerrainHeightAt(Vector2 point){
	Level_t* level = &game.level;
	unsigned char value;
	int tile[] = {
		(int)(point.x / LV_TILESIZE),
		(int)(point.y / LV_TILESIZE)
	};
	if(CoordOutOfBounds(tile, level)) return 0;
	value = level->terrainHeight[tile[0]][tile[1]];
	return (value / 255.0 - 0.5) * LV_TERRAINHEIGHT;
}

void DrawLevelTerrain(){
	Level_t* level = &game.level;
	//bool useShader = (client.assets.shaders.terrain.id != 0);
	bool useShader = false;
	Texture2D texture = client.assets.textures.terrain;
	float quadrantSize = texture.height/4;
	Rectangle defaultGround = {0, 0, quadrantSize, quadrantSize};
	int valueRange = LV_TERRAINTEXROWS*LV_TERRAINTEXROWS;


	
	for(int iy=client.tileViewBox[1]; iy < client.tileViewBox[3]; iy++)
	for(int ix=client.tileViewBox[0]; ix < client.tileViewBox[2]; ix++){
		float height = (level->terrainHeight[ix][iy] / 255.0 - 0.5) * LV_TERRAINHEIGHT;
		Vector3 pos = Project3d((Vector3){ix*LV_TILESIZE, iy*LV_TILESIZE, height});
		char colorPacked = level->terrainColor[ix][iy];
		int colorIndex = (colorPacked / valueRange);
		float colorAlpha = (colorPacked % valueRange) / (255.0/valueRange);
		float texMargin = 0.0;
		Rectangle sourceRect = {(colorIndex % LV_TERRAINTEXROWS)*quadrantSize+texMargin, (colorIndex / 4)*quadrantSize+texMargin, quadrantSize-texMargin*2, quadrantSize-texMargin*2};
		Rectangle destRect = {pos.x, pos.y, LV_TILESIZE, LV_TILESIZE};
		
		//DrawRectangleLines(ix*LV_TILESIZE, iy*LV_TILESIZE, LV_TILESIZE, LV_TILESIZE, LIGHTGRAY);
		DrawTexturePro(texture, defaultGround, destRect, Vector2Zero(), 0, WHITE);
		if(colorIndex != 0){
			DrawTexturePro(texture, sourceRect, destRect, Vector2Zero(), 0, Fade(WHITE, colorAlpha));
		}
	}
}

void DrawLevel(int minOrder, int maxOrder){
	Level_t* level = &game.level;

	forEachLvLayer(layer, layerId){
		bool fgLayer = (layer->order >= 100);
		if(!layer->visible) continue;
		if((layer->order < minOrder) || (layer->order > maxOrder)) continue;
		if(layer->blendMode) BeginBlendMode(BLEND_ADDITIVE);

		for(int brushNum=0; brushNum < layer->brushCount; brushNum++){
			LvBrush_t* brush = &level->brushes[brushNum + layer->brushStartId];
			if(!fgLayer){
				if(!APPROX_NEAR(brush->sector, client.sector, client.sectorsInView)) continue;
			}
			
			Vector3 pos = Project3d(brush->pos);
			if(!fgLayer){
				DrawSprite(brush->spriteId, brush->spriteFrame, pos, brush->tint, brush->size);
			}else{
				BlitSprite(brush->spriteId, brush->spriteFrame, Vec2(pos), brush->tint, brush->size);
			}
		}

		if(layer->blendMode) EndBlendMode();
	}

	// forEachLvBrush(brush, brushId){
	// 	if(!APPROX_NEAR(brush->sector, client.sector, client.sectorsInView)) continue;
	// 	Vector3 pos = Project3d(brush->pos);
	// 	DrawSprite(brush->spriteId, brush->spriteFrame, Vec2(pos), pos.z, brush->tint);
		
	// }

}
