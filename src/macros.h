#pragma once
#include "gameapp.h"

static inline Boy_t*  GetBoy  (int id){ return &game.boys[id];  }
static inline Toy_t*  GetToy  (int id){ return &game.toys[id];  }
static inline Item_t* GetItem (int id){ return &game.items[id]; }
static inline ToyClass_t*  GetToyClass  (int id){ return &toyClasses[id];  }
static inline ItemClass_t* GetItemClass (int id){ return &itemClasses[id]; }

#define forEach(T, Get, Max, Handle, Id) \
			int Id = 0; \
			for(T* Handle = Get(0); Id < Max; Handle = Get(++Id)) \
			    if(Handle->active)

#define innerForEach(T, Prop, Max, Handle, Id) \
			int Id = 0; \
			for(T* Handle = Prop[0]; Id < Max; Handle = Prop[++Id]) \
			    if(Handle->active)

#define monoForEach(T, Prop, Max, Handle, Id) \
			int Id = 0; \
			for(T* Handle = Prop[0]; Id < Max; Handle = Prop[++Id])

#define forEachBoy(Handle, Id) \
			forEach(Boy_t, GetBoy, MAX_BOYS, Handle, Id)

#define forEachToy(Handle, Id) \
			forEach(Toy_t, GetToy, MAX_TOYS, Handle, Id)

#define forEachItem(Handle, Id) \
			forEach(Item_t, GetItem, MAX_ITEMS, Handle, Id)

#define forEachLimb(toy, Handle, Id) \
			innerForEach(Limb_t, &toy->limbs, MAX_TOY_LIMBS, Handle, Id)

#define forEachLvBrush(Handle, Id) \
			monoForEach(LvBrush_t, &game.level.brushes, game.level.brushCount, Handle, Id)

#define forEachLvLayer(Handle, Id) \
			monoForEach(LvLayer_t, &game.level.layers, game.level.layerCount, Handle, Id)

#define forEachLvEntity(Handle, Id) \
			monoForEach(LvEntity_t, &game.level.entities, game.level.entityCount, Handle, Id)
